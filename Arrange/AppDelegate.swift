//
//  AppDelegate.swift
//  Arrange
//
//  Created by Miral Kamani on 07/02/20.
//  Copyright © 2020 Miral Kamani. All rights reserved.
//

import UIKit
import FBSDKCoreKit
import FBSDKLoginKit
import Toaster
import UserNotifications
import Firebase
import FirebaseMessaging
@UIApplicationMain

class AppDelegate: UIResponder, UIApplicationDelegate {
    
    var window: UIWindow?
    let gcmMessageIDKey = "gcm.message_id"
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        
        if Defaults[.UserIsLogin] == true{
            
            let mainStoryboard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
            let myVC = mainStoryboard.instantiateViewController(withIdentifier: "MaincardNavigation")
            window?.rootViewController = myVC
        }
        else{
            let mainStoryboard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
            let myVC = mainStoryboard.instantiateViewController(withIdentifier: "IntroNavigation")
            window?.rootViewController = myVC
        }
        FirebaseApp.configure()
        Messaging.messaging().delegate = self
        
        if #available(iOS 10.0, *) {
            // For iOS 10 display notification (sent via APNS)
            UNUserNotificationCenter.current().delegate = self
            
            let authOptions: UNAuthorizationOptions = [.alert, .badge, .sound]
            UNUserNotificationCenter.current().requestAuthorization(
                options: authOptions,
                completionHandler: {_, _ in })
        } else {
            let settings: UIUserNotificationSettings =
                UIUserNotificationSettings(types: [.alert, .badge, .sound], categories: nil)
            application.registerUserNotificationSettings(settings)
        }
        
        application.registerForRemoteNotifications()
        //       GMSServices.provideAPIKey("AIzaSyBZR53LeDpCYf2YjpG9fPkuyMmuJl6vsh4")
        //       GMSPlacesClient.provideAPIKey("AIzaSyBZR53LeDpCYf2YjpG9fPkuyMmuJl6vsh4")
        // Override point for customization after application launch.
        return true
    }
    
    func configureAppearance() {
        let appearance = ToastView.appearance()
        appearance.backgroundColor = UIColor.init(red: 0/255, green: 0/255, blue: 0/255, alpha: 0.85)
        appearance.textColor = .white
        appearance.font = .boldSystemFont(ofSize: 16)
        appearance.textInsets = UIEdgeInsets(top: 15, left: 20, bottom: 15, right: 20)
        appearance.bottomOffsetPortrait = 100
        appearance.cornerRadius = 20
    }
    
    // MARK: UISceneSession Lifecycle
    
    //    func application(_ application: UIApplication, configurationForConnecting connectingSceneSession: UISceneSession, options: UIScene.ConnectionOptions) -> UISceneConfiguration {
    //        // Called when a new scene session is being created.
    //        // Use this method to select a configuration to create the new scene with.
    //        return UISceneConfiguration(name: "Default Configuration", sessionRole: connectingSceneSession.role)
    //    }
    //
    //    func application(_ application: UIApplication, didDiscardSceneSessions sceneSessions: Set<UISceneSession>) {
    //        // Called when the user discards a scene session.
    //        // If any sessions were discarded while the application was not running, this will be called shortly after application:didFinishLaunchingWithOptions.
    //        // Use this method to release any resources that were specific to the discarded scenes, as they will not return.
    //    }
    func application(_ application: UIApplication, open url: URL, sourceApplication: String?, annotation: Any) -> Bool {
        
        
        let FBSDSignIn = ApplicationDelegate.shared.application(application, open: url, sourceApplication: sourceApplication, annotation: annotation)
        return FBSDSignIn
    }
    // Facebook End
    func application(_ app: UIApplication, open url: URL, options: [UIApplication.OpenURLOptionsKey : Any] = [:]) -> Bool
    {
        let FBSDSignIn = ApplicationDelegate.shared.application(app, open: url, sourceApplication:  options[UIApplication.OpenURLOptionsKey.sourceApplication] as? String, annotation:  options[UIApplication.OpenURLOptionsKey.annotation])
        return FBSDSignIn
    }
    
    // MARK:- Push Notification Code
    
    // [START receive_message]
    func application(_ application: UIApplication, didReceiveRemoteNotification userInfo: [AnyHashable: Any]) {
        
        if let messageID = userInfo[gcmMessageIDKey] {
            print("Message ID: \(messageID)")
        }
        print(userInfo)
    }
    
    func application(_ application: UIApplication, didReceiveRemoteNotification userInfo: [AnyHashable: Any],
                     fetchCompletionHandler completionHandler: @escaping (UIBackgroundFetchResult) -> Void) {
        if let messageID = userInfo[gcmMessageIDKey] {
            print("Message ID: \(messageID)")
        }
        
        print(userInfo)
        
        completionHandler(UIBackgroundFetchResult.newData)
    }
  
    
    func application(_ application: UIApplication, didFailToRegisterForRemoteNotificationsWithError error: Error) {
        print("Unable to register for remote notifications: \(error.localizedDescription)")
    }
    
    
    func application(_ application: UIApplication, didRegisterForRemoteNotificationsWithDeviceToken deviceToken: Data) {
        print("APNs token retrieved: \(deviceToken)")
        
       Messaging.messaging().apnsToken = deviceToken
    }
}
@available(iOS 10, *)
extension AppDelegate : UNUserNotificationCenterDelegate {
    
    // Receive displayed notifications for iOS 10 devices.
    func userNotificationCenter(_ center: UNUserNotificationCenter,
                                willPresent notification: UNNotification,
                                withCompletionHandler completionHandler: @escaping (UNNotificationPresentationOptions) -> Void) {
        let userInfo = notification.request.content.userInfo
        
        
        if let messageID = userInfo[gcmMessageIDKey] {
            print("Message ID: \(messageID)")
        }
        print(userInfo)
        completionHandler([])
    }
    
    func userNotificationCenter(_ center: UNUserNotificationCenter,didReceive response: UNNotificationResponse,withCompletionHandler completionHandler: @escaping () -> Void)
    {
        let userInfo = response.notification.request.content.userInfo
        // Print message ID.
        if let messageID = userInfo[gcmMessageIDKey] {
            print("Message ID: \(messageID)")
        }
        
        if UIApplication.shared.applicationState == .background {
            print("didReceiveRemoteNotification BACKGROUND")
            
        }
        else  if UIApplication.shared.applicationState == .inactive {
            print("didReceiveRemoteNotification INACTIVE")
        }
        
        print(userInfo)
        
        //  AnyHashable("chatroom_id"): 5, AnyHashable("gcm.message_id"): 1579766343252768, AnyHashable("type"): Campaign_Chat_Massage, AnyHashable("last_chat_message"): hi, AnyHashable("admin_name"): Admin]
        
        //        if let C_id = userInfo[AnyHashable("chatroom_id")] as? String
        //        {
        //            chatroomId = Int(C_id)!
        //        }
        //
        //        if let a_name = userInfo[AnyHashable("admin_name")] as? String
        //        {
        //            adminName = a_name
        //        }
        //
        //        AppDelegate.isChatVC = true
        
        //   let vc = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "navigationStart")
        //  window?.rootViewController = vc
        
        completionHandler()
    }
    
}


extension AppDelegate : MessagingDelegate {
    // [START refresh_token]
    func messaging(_ messaging: Messaging, didReceiveRegistrationToken fcmToken: String) {
        print("Firebase registration token: \(fcmToken)")
        
        Defaults[.FCMToken] = fcmToken
        
    }
    func messaging(_ messaging: Messaging, didReceive remoteMessage: MessagingRemoteMessage) {
        print("Received data message: \(remoteMessage.appData)")
    }
    // [END ios_10_data_message]
}
