//
//  ApiUtility.swift
//  TestApp
//
//  Created by MAC on 10/31/17.
//  Copyright © 2017 MAC. All rights reserved.
//

import Foundation
import UIKit
import AVFoundation
import Photos
import Alamofire

private let _sharedInstance = ApiUtillity()



class DCustomButton: UIButton {
    var indexpath: IndexPath?
}

let groupByKey:String = "key"
let groupByArray:String = "subArray"

class ApiUtillity: NSObject {
    
    class var sharedInstance: ApiUtillity {
        return _sharedInstance
    }
    
    // MARK:- GetCurrentDateTime
    
    func currentDateTime() -> String {
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyy-MM-dd hh:mm:ss"
        let resultDateString = formatter.string(from: Date())
        print(resultDateString)
        return resultDateString
    }
    
    // MARK:- Get Device
    
    func is_iPhoneX() -> Bool {
        if UIDevice.current.userInterfaceIdiom == .phone {
            
            switch Int(UIScreen.main.nativeBounds.size.height) {
                //
                //            case 1136:
                //                printf("iPhone 5 or 5S or 5C");
                //                return false;
                //                break;
                //            case 1334:
                //                printf("iPhone 6/6S/7/8");
                //                return false;
                //                break;
                //            case 1920:
                //                printf("iPhone 6+/6S+/7+/8+");
                //                return false;
                //                break;
                //            case 2208:
                //                printf("iPhone 6+/6S+/7+/8+");
                //                return false;
            //                break;
            case 2436:
                print("iPhone X")
                return true
            default:
                print("unknown")
                return false
            }
        }
        return false
    }

    
    func isValidEmail(testStr:String) -> Bool {
        let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,64}"
        
        let emailTest = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
        return emailTest.evaluate(with: testStr)
    }
    
    func isPasswordValid(_ password : String) -> Bool{
        let passwordTest = NSPredicate(format: "SELF MATCHES %@", "^(?=.*[A-Z])(?=.*[!@#$&*])(?=.*[0-9])(?=.*[a-z]).{6,}$")
        return passwordTest.evaluate(with: password)
    }
    
    func notPrettyString(from object: Any) -> String? {
        if let objectData = try? JSONSerialization.data(withJSONObject: object, options: JSONSerialization.WritingOptions(rawValue: 0)) {
            let objectString = String(data: objectData, encoding: .utf8)
            return objectString
        }
        return nil
    }
    
    func prettyPrint(with json: [String:Any]) -> String{
        let data = try! JSONSerialization.data(withJSONObject: json, options: .prettyPrinted)
        let string = NSString(data: data, encoding: String.Encoding.utf8.rawValue)
        return string! as String
    }
    
    func json(from object:Any) -> String? {
        guard let data = try? JSONSerialization.data(withJSONObject: object, options: []) else {
            return nil
        }
        return String(data: data, encoding: String.Encoding.utf8)
    }
    
    // For Get Color Using HexCode
    func getColorIntoHex(Hex:String) -> UIColor {
        if Hex.isEmpty {
            return UIColor.clear
        }
        let scanner = Scanner(string: Hex)
        scanner.scanLocation = 0
        var rgbValue: UInt64 = 0
        scanner.scanHexInt64(&rgbValue)
        let r = (rgbValue & 0xff0000) >> 16
        let g = (rgbValue & 0xff00) >> 8
        let b = rgbValue & 0xff
        return UIColor.init(red: CGFloat(r) / 0xff, green: CGFloat(g) / 0xff, blue: CGFloat(b) / 0xff, alpha: 1)
    }
    
    //For Check Internate Availability
    func isReachable() -> Bool
    {
        let reachabilityManager = Alamofire.NetworkReachabilityManager(host: "www.google.com")
        return (reachabilityManager?.isReachable)!
    }
    
    //Image Color Change
    func setImageColor(obj:Any, ImageName imagename:String,Color color:UIColor) {
        if obj is UIButton {
            let tempView:UIButton = obj as! UIButton
            let image :UIImage = UIImage(named: imagename)!
            tempView.setImage(image.withRenderingMode(.alwaysTemplate), for: .normal)
            tempView.tintColor = color
        }
        if obj is UIImageView {
            let tempView:UIImageView = obj as! UIImageView
            let image :UIImage = UIImage(named: imagename)!
            tempView.image = image.withRenderingMode(.alwaysTemplate)
            tempView.tintColor = color
        }
    }
    
    // Popupview
    func AddSubViewtoParentView(parentview: UIView! , subview: UIView!)
    {
        subview.translatesAutoresizingMaskIntoConstraints = false
        parentview.addSubview(subview);
        parentview.addConstraint(NSLayoutConstraint(item: subview, attribute: NSLayoutConstraint.Attribute.top, relatedBy: NSLayoutConstraint.Relation.equal, toItem: parentview, attribute: NSLayoutConstraint.Attribute.top, multiplier: 1.0, constant: 0.0))
        parentview.addConstraint(NSLayoutConstraint(item: subview, attribute: NSLayoutConstraint.Attribute.leading, relatedBy: NSLayoutConstraint.Relation.equal, toItem: parentview, attribute: NSLayoutConstraint.Attribute.leading, multiplier: 1.0, constant: 0.0))
        parentview.addConstraint(NSLayoutConstraint(item: subview, attribute: NSLayoutConstraint.Attribute.bottom, relatedBy: NSLayoutConstraint.Relation.equal, toItem: parentview, attribute: NSLayoutConstraint.Attribute.bottom, multiplier: 1.0, constant: 0.0))
        parentview.addConstraint(NSLayoutConstraint(item: subview, attribute: NSLayoutConstraint.Attribute.trailing, relatedBy: NSLayoutConstraint.Relation.equal, toItem: parentview, attribute: NSLayoutConstraint.Attribute.trailing, multiplier: 1.0, constant: 0.0))
        parentview.layoutIfNeeded()
        
    }
    
    func getRowsforList(_ tableCollectionView: Any, forArray arrayCount: Int, withPlaceHolder strPlaceHolder: String) -> Int {
        if (tableCollectionView is UITableView) {
            let tableView = tableCollectionView as? UITableView
            if tableView?.tag != 0 && arrayCount == 0 {
                let noDataMessage = UILabel(frame: CGRect(x: 0, y: 0, width: (tableView?.bounds.size.width)!, height: (tableView?.bounds.size.height)!))
                noDataMessage.text = strPlaceHolder
                noDataMessage.textColor = UIColor.darkGray
                noDataMessage.textAlignment = .center
                tableView?.backgroundView = noDataMessage
            }
            else {
                tableView?.backgroundView = nil
            }
        }
        if (tableCollectionView is UICollectionView) {
            let collectionView = tableCollectionView as? UICollectionView
            if collectionView?.tag != 0 && arrayCount == 0 {
                let noDataMessage = UILabel(frame: CGRect(x: 0, y: 0, width: (collectionView?.bounds.size.width)!, height: (collectionView?.bounds.size.height)!))
                noDataMessage.text = strPlaceHolder
                noDataMessage.textColor = UIColor.darkGray
                noDataMessage.textAlignment = .center
                collectionView?.backgroundView = noDataMessage
            }
            else {
                collectionView?.backgroundView = nil
            }
        }
        return arrayCount
    }
    
    // Set Language Data
    func setLanguageData(data:NSMutableDictionary) {
        var Dic:NSMutableDictionary = NSMutableDictionary()
        Dic = data.mutableCopy() as! NSMutableDictionary
        
        UserDefaults.standard.setValue(Dic, forKey: "LANGUAGE_DATA")
    }
    
    // For Set Shadow To All Control
    func setShadow(obj:Any, cornurRadius:CGFloat, ClipToBound:Bool, masksToBounds:Bool, shadowColor:String, shadowOpacity:Float, shadowOffset:CGSize, shadowRadius:CGFloat, shouldRasterize:Bool, shadowPath:CGRect) {
        if obj is UIView {
            let tempView:UIView = obj as! UIView
            tempView.clipsToBounds = ClipToBound
            tempView.layer.cornerRadius = cornurRadius
            tempView.layer.shadowOffset = shadowOffset
            tempView.layer.shadowOpacity = shadowOpacity
            tempView.layer.shadowRadius = shadowRadius
            tempView.layer.shadowColor = self.getColorIntoHex(Hex: shadowColor).cgColor
            tempView.layer.masksToBounds =  masksToBounds
            tempView.layer.shouldRasterize = shouldRasterize
            tempView.layer.shadowPath = UIBezierPath(roundedRect: tempView.bounds, cornerRadius: cornurRadius).cgPath
        }
    }
    
    // For Set CornurRadius To All Control
    func setCornurRadius(obj:Any, cornurRadius:CGFloat, isClipToBound:Bool, borderColor:String, borderWidth:CGFloat) {
        if obj is UILabel {
            let tempLabel:UILabel = obj as! UILabel
            tempLabel.layer.cornerRadius = cornurRadius
            tempLabel.clipsToBounds = isClipToBound
            tempLabel.layer.borderColor = self.getColorIntoHex(Hex: borderColor).cgColor
            tempLabel.layer.borderWidth = borderWidth
        }
        if obj is UITextField {
            let tempTextField:UITextField = obj as! UITextField
            tempTextField.layer.cornerRadius = cornurRadius
            tempTextField.clipsToBounds = isClipToBound
            tempTextField.layer.borderColor = self.getColorIntoHex(Hex: borderColor).cgColor
            tempTextField.layer.borderWidth = borderWidth
        }
        if obj is UIButton {
            let tempButton:UIButton = obj as! UIButton
            tempButton.layer.cornerRadius = cornurRadius
            tempButton.clipsToBounds = isClipToBound
            tempButton.layer.borderColor = self.getColorIntoHex(Hex: borderColor).cgColor
            tempButton.layer.borderWidth = borderWidth
        }
        if obj is UITextView {
            let tempTextView:UITextView = obj as! UITextView
            tempTextView.layer.cornerRadius = cornurRadius
            tempTextView.clipsToBounds = isClipToBound
            tempTextView.layer.borderColor = self.getColorIntoHex(Hex: borderColor).cgColor
            tempTextView.layer.borderWidth = borderWidth
        }
        if obj is UIView {
            let tempView:UIView = obj as! UIView
            tempView.layer.cornerRadius = cornurRadius
            tempView.clipsToBounds = isClipToBound
            tempView.layer.borderColor = self.getColorIntoHex(Hex: borderColor).cgColor
            tempView.layer.borderWidth = borderWidth
        }
    }
    
    // For Set Same CornurRadius To All Control
    func setCornurRadiusToAllControls(allObj:[Any], cornurRadius:CGFloat, isClipToBound:Bool, borderColor:String, borderWidth:CGFloat) {
        for item in allObj {
            self.setCornurRadius(obj: item, cornurRadius: cornurRadius, isClipToBound: isClipToBound, borderColor: borderColor, borderWidth: borderWidth)
        }
    }
    
    // For Set PlaceHolderColor To TextField
    func setPlaceHolderColorToTextField(obj:UITextField,color:String) {
        obj.attributedPlaceholder = NSAttributedString(string:obj.placeholder!, attributes: [NSAttributedString.Key.foregroundColor:self.getColorIntoHex(Hex:color)])
    }
    // For Set PlaceHolderColorS To All TextFields
    func setPlaceHolderColorsToAllTextFields(allObj:[UITextField],color:String) {
        for item in allObj {
            self.setPlaceHolderColorToTextField(obj: item, color: color)
        }
    }
    
    // For Set Capital First Latter
    func setCapitalFirstLatter(word:String) -> String {
        if !word.isEmpty {
            let temp_Word:NSString = word as NSString
            let First_Char:NSString = (temp_Word.substring(to: 1) as NSString).uppercased as NSString
            let New_Upper_String:String = (First_Char as String) + ((temp_Word.substring(from: 1) as NSString) as String)
            return New_Upper_String
        }
        else {
            return ""
        }
    }
    
    
    
   
    
    
    
    // MARK:- Pretty Print Function
    
    func DictToJSON(with json: [String:Any]) -> String {
        let data = try! JSONSerialization.data(withJSONObject: json, options: .prettyPrinted)
        let string = NSString(data: data, encoding: String.Encoding.utf8.rawValue)
        return string! as String
    }
    
    func JSONToDict(text: String) -> [String: Any]? {
        if let data = text.data(using: .utf8) {
            do {
                return try JSONSerialization.jsonObject(with: data, options: []) as? [String: Any]
            } catch {
                print(error.localizedDescription)
            }
        }
        return nil
    }
    
    // MARK:- Convert Date To String
    
    func convertDateToString(forApp date: String) -> Date {
        let format = DateFormatter()
        
        if date.count==10 {
            format.dateFormat = "dd MMM yyyy"
        }
        else
        {
            format.dateFormat = "yyyy-MM-dd hh:mm:ss"
        }
        
        let dt: Date = format.date(from: date)!
        return dt
    }
    
    func convertDateToMonthString(forApp strdate: String) -> String {
        
        let date = self.convertDateToString(forApp: strdate)
        let format = DateFormatter()
        format.dateFormat = "dd MMM yyyy"
        let dt: String = format.string(from: date)
        return dt
    }
}
