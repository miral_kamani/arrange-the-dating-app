//
//  UserDefaults.swift
//  Arrange
//
//  Created by Miral Kamani on 04/03/20.
//  Copyright © 2020 Miral Kamani. All rights reserved.
//

import Foundation
import UIKit


//Default Declared Keys
extension DefaultsKeys {
    
    
    static let FacebookResponse = DefaultsKey<[String: Any]>("FacebookResponse")
    static let UserIsLogin = DefaultsKey<Bool>("UserIsLogin")
    static let FCMToken = DefaultsKey<String>("FCMToken")
    static let loginType = DefaultsKey<String>("loginType")
    static let ages = DefaultsKey<String>("ages")
    static let religion = DefaultsKey<String>("religion")
    static let community = DefaultsKey<String>("community")
    static let career = DefaultsKey<String>("career")
    static let raisedIn = DefaultsKey<String>("raisedIn")
    static let education = DefaultsKey<String>("education")
    static let traits = DefaultsKey<[Int]>("traits")
    static let Gender = DefaultsKey<String>("Gender")
    static let Height = DefaultsKey<String>("Height")
    static let Location = DefaultsKey<String>("Location")
    static let UpdateResponse = DefaultsKey<[String: Any]>("UpdateResponse")
    static func defaultFCMKeyMaker(key:String) -> DefaultsKey<Int>{
        return DefaultsKey<Int>(key)
    }
}
