import UIKit
import NVActivityIndicatorView
import Alamofire

class UtilityClass: NSObject {
    
    //---------------------------------------------------------------
    //MARK: - AlertView
    //---------------------------------------------------------------
    
    typealias CompletionHandler = (_ success:Bool) -> Void
    
    class func showAlert(_ title: String, message: String, vc: UIViewController) -> Void
    {
        let alert = UIAlertController(title: title,
                                      message: message,
                                      preferredStyle: UIAlertController.Style.alert)
        
        let cancelAction = UIAlertAction(title: "OK", style: .cancel, handler: nil)
        
        alert.addAction(cancelAction)
        
        let alertWindow = UIWindow(frame: UIScreen.main.bounds)
        alertWindow.rootViewController = UIViewController()
        alertWindow.windowLevel = UIWindow.Level.alert + 1;
        alertWindow.makeKeyAndVisible()
        alertWindow.rootViewController?.present(alert, animated: true, completion: nil)
    }
    
    
    class func showAlertWithCompletion(_ title: String, message: String, vc: UIViewController,completionHandler: @escaping CompletionHandler) -> Void
    {
        let alert = UIAlertController(title: title,
                                      message: message,
                                      preferredStyle: UIAlertController.Style.alert)
        
        alert.addAction(UIAlertAction(title: "OK", style: .default, handler: { (action) in
            completionHandler(true)
        }))
        vc.present(alert, animated: true, completion: nil)
    }
    //---------------------------------------------------------------
    // set Gredient color to Login Button
    //---------------------------------------------------------------
    
    class func giveGradientColorToButton(setView: UIView, startColor: UIColor, endColor: UIColor)
    {
        let colorStart =  startColor.cgColor
        let colorEnd = endColor.cgColor
        let gradientLayer = CAGradientLayer()
        
        gradientLayer.startPoint = CGPoint(x: 0.0, y: 0.5)
        gradientLayer.endPoint = CGPoint(x: 1.0, y: 0.5)
        gradientLayer.colors = [ colorStart, colorEnd ]
        gradientLayer.frame = setView.bounds
        if((setView.layer.sublayers?.count)! == 3)
        {
            setView.layer.sublayers?.remove(at: 0)
        }
        setView.layer.insertSublayer(gradientLayer, at: 0)

    }
    
    class func giveBorderAndCornerRadius(view : UIView, cornerRadius : CGFloat)
    {
        view.layer.borderWidth = 1.0
        view.layer.borderColor = UIColor.lightGray.cgColor
        view.layer.cornerRadius = cornerRadius

        view.clipsToBounds = true
    }
   
    
    class func changeColorOfSearchBarPlaceHolder(searchbar: UISearchBar,string: String)
    {
        let searchTextField: UITextField? = searchbar.value(forKey: "searchField") as? UITextField
        if searchTextField!.responds(to: #selector(getter: UITextField.attributedPlaceholder)) {
            let string_to_color2 = string
            let range2 = (string as NSString).range(of: string_to_color2)
            let attributedString1 = NSMutableAttributedString(string:string)
            
            attributedString1.addAttribute(NSAttributedString.Key.foregroundColor, value: UIColor.white, range: range2)
            
            searchTextField!.attributedPlaceholder = attributedString1
        }
    }
    //circleStrokeSpin
    class func showhud()
    {
        let activityData = ActivityData()
        NVActivityIndicatorView.DEFAULT_TYPE = .circleStrokeSpin//.ballZigZagDeflect
        NVActivityIndicatorView.DEFAULT_BLOCKER_MINIMUM_DISPLAY_TIME = 55
        NVActivityIndicatorView.DEFAULT_BLOCKER_DISPLAY_TIME_THRESHOLD = 55
        NVActivityIndicatorView.DEFAULT_COLOR = UIColor.white //spinnerColor
        NVActivityIndicatorPresenter.sharedInstance.startAnimating(activityData)
    }
    class func hidehud() {
        NVActivityIndicatorPresenter.sharedInstance.stopAnimating()
    }
   
    class func delay(_ delay:Double, closure:@escaping ()->())
    {
        DispatchQueue.main.asyncAfter(
            deadline: DispatchTime.now() + Double(Int64(delay * Double(NSEC_PER_SEC))) / Double(NSEC_PER_SEC), execute: closure)
    }
    
    class func removeSpace(string : String) -> Bool
    {
        if !string.trimmingCharacters(in: .whitespaces).isEmpty {
            return false
        }
        return true
    }
}



//-------------------------------------------------------------
// MARK: -  For Dismiss Keyboard
//-------------------------------------------------------------

extension String {
    func isEmptyOrWhitespace() -> Bool {
        
        if(self.isEmpty) {
            return true
        }
        return (self.trimmingCharacters(in: .whitespaces ) == "")

    }
}



extension UIViewController {
    
    // Ends editing view when touches to view
    open override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        super.touchesBegan(touches, with: event)
        self.view.endEditing(true)
    }
}

//
//extension UIFont {
//    class func extraBold(ofSize size: CGFloat) -> UIFont {
//        return UIFont(name: "ProximaNova-Extrabld", size: size)!
//    }
//    class func light(ofSize size: CGFloat) -> UIFont {
//        return UIFont(name: "ProximaNova-Light", size: size)!
//    }
//    class func Black(ofSize size: CGFloat) -> UIFont {
//        return UIFont(name: "ProximaNova-Black", size: size)!
//    }
//    class func Semibold(ofSize size: CGFloat) -> UIFont {
//        return UIFont(name:"ProximaNova-Semibold", size: size)!
//    }
//    class func Bold(ofSize size: CGFloat) -> UIFont {
//        return UIFont(name: "ProximaNova-Bold", size: size)!
//    }
//    class func Regular(ofSize size: CGFloat) -> UIFont {
//        return UIFont(name: "ProximaNova-Regular", size: size)!
//    }
//}


class CustomNavigationController: UINavigationController {
    
    convenience init() {
        self.init(navigationBarClass: CustomNavigationBar.self, toolbarClass: nil)
    }
}

class CustomNavigationBar: UINavigationBar {
    
    override func layoutSubviews() {
        backItem?.title = ""
        super.layoutSubviews()
    }
}

//// internetchack on/off
class Connectivity {
    class func isConnectedToInternet() ->Bool {
        return NetworkReachabilityManager()!.isReachable
    }
}


extension Array where Element:Equatable {
    func removeDuplicates() -> [Element] {
        var result = [Element]()
        
        for value in self {
            if result.contains(value) == false {
                result.append(value)
            }
        }
        
        return result
    }
}

