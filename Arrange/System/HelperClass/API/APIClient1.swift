//
//  APIClient1.swift
//  Chance4Chance
//
//  Created by Nishant on 06/09/19.
//  Copyright © 2019 Abhishek. All rights reserved.
//

//
//  SignupManager.swift
//  Mensa
//
//  Created by social in on 18/10/16.
//  Copyright © 2016 Dataready. All rights reserved.
//

import Foundation
import UIKit
import Alamofire
import ObjectMapper
//import Crashlytics

typealias apiResponseSuccess = (_ success: Any) -> Void
typealias apiResponseFailed = (_ failed: Error) -> Void
typealias apiResponseStatus = (_ Status: Bool) -> Void

private let _sharedInstance = APIClient1()

class APIClient1: NSObject {
    
    let storyboard:UIStoryboard = UIStoryboard.init(name: "Main", bundle: nil)
    
    class var sharedInstance: APIClient1 {
        return _sharedInstance
    }
    
    func authHeader(ISAuth:Bool) -> [String : String] {
        
        var header = [String : String]()
        
       let model = ApiUtillity.sharedInstance.getLoginModel()
        
        if let api = model?.facebookResult?.userApi,api.count != 0
        {
            header = ["X-Authorization": api,
                      "Content-Type": "application/x-www-form-urlencoded"] as [String : String]
        }
        else
        {
            header = ["Content-Type": "application/x-www-form-urlencoded"] as [String : String]
        }
        print(header)
        
        return header
    }
    
    //MARK:- JSON converter
    func prettyPrint(with json: [String:Any]) -> String {
        let data = try! JSONSerialization.data(withJSONObject: json, options: .prettyPrinted)
        let string = NSString(data: data, encoding: String.Encoding.utf8.rawValue)
        return string! as String
    }
    
    
    //MARK:- API_GET
    func API_GET(Url:String,Params:[String : AnyObject], Authentication:Bool, mapObject:Any?, SuperVC:UIViewController, completionSuccess: @escaping apiResponseSuccess, completionFailed: @escaping apiResponseFailed) {
        
        //        print( prettyPrint(with: Params))
        //        print(Url)
        
        if ApiUtillity.sharedInstance.isReachable() {
            let authHeader = self.authHeader(ISAuth: Authentication)
            
            
            Alamofire.request(Url, method: .get, parameters: Params, encoding: URLEncoding.default, headers: authHeader).responseJSON
                { response in
                    //            debugPrint(response)
                    guard
                        let responseJSON = response.result.value
                        
                        else {
                            print(response.error!)
                            completionFailed(response.error!)
                            return
                    }
                    //                    print(responseJSON)
                    
                    if response.result.value != nil
                    {
                        completionSuccess(responseJSON)
                        return
                    }
                    else
                    {
                        print(response.error!)
                    }
                }.responseString { response in
                    print("-----------------------")
                    debugPrint(response)
                    print("-----------------------")
                    print(response)
            }
        }
        else
        {
            return
        }
    }
    
    // MARK:- API_POST
    
    func API_POST(Url:String,Params:[String : AnyObject],Authentication:Bool,mapObject:Any?,SuperVC:UIViewController,completionSuccess: @escaping apiResponseSuccess,completionFailed: @escaping apiResponseFailed) {
        
        print( prettyPrint(with: Params))
        print(Url)
        
        
        
        if ApiUtillity.sharedInstance.isReachable() {
            let authHeader = self.authHeader(ISAuth: Authentication)
            Alamofire.request(Url, method: .post, parameters: Params, encoding: URLEncoding.default, headers: authHeader).responseJSON
                { response in
                    //                    debugPrint(response)
                    //                    print(response)
                    guard
                        let responseJSON = response.result.value
                        
                        else {
                            
                            completionFailed(response.error!)
                            return
                    }
                    if response.result.value != nil
                    {
                        completionSuccess(responseJSON)
                        return
                    }
                    else
                    {
                        print(response.error!)
                    }
                }.responseString { response in
                    print("-----------------------")
                    debugPrint(response)
                    print("-----------------------")
                    print(response)
                    
            }
            
        }
        else {
            
        }
    }
    
    //MARK:- API_DELETE
    func API_DELETE(Url:String,Params:[String : AnyObject],Authentication:Bool,mapObject:Any?,SuperVC:UIViewController,completionSuccess: @escaping apiResponseSuccess,completionFailed: @escaping apiResponseFailed) {
        
        print( prettyPrint(with: Params))
        print(Url)
        
        if ApiUtillity.sharedInstance.isReachable() {
            let authHeader = self.authHeader(ISAuth: Authentication)
            Alamofire.request(Url, method: .delete, parameters: Params, encoding: URLEncoding.default, headers: authHeader).responseJSON
                { response in
                    
                    guard
                        let responseJSON = response.result.value
                        
                        else {
                            
                            completionFailed(response.error!)
                            return
                    }
                    if response.response != nil
                    {
                        completionSuccess(responseJSON)
                        return
                    }
            }
        }
        else {
            
        }
    }
    
    //MARK:- API_PUT
    func API_PUT(Url:String,Params:[String : AnyObject],Authentication:Bool,mapObject:Any?,SuperVC:UIViewController,completionSuccess: @escaping apiResponseSuccess,completionFailed: @escaping apiResponseFailed) {
        
        print( prettyPrint(with: Params))
        print(Url)
        
        if ApiUtillity.sharedInstance.isReachable() {
            let authHeader = self.authHeader(ISAuth: Authentication)
            Alamofire.request(Url, method: .put, parameters: Params, encoding: URLEncoding.default, headers: authHeader).responseJSON
                { response in
                    //            debugPrint(response)
                    guard
                        let responseJSON = response.result.value
                        
                        else {
                            
                            completionFailed(response.error!)
                            return
                    }
                    if response.response != nil
                    {
                        completionSuccess(responseJSON)
                        return
                    }
            }
        }
        else {
            
        }
    }
    
    //MARK:- API_UPLOAD
    func API_UPLOAD(Url:String,Params:[String : AnyObject],fileName:String,fileData:Data,mimeType:String,Authentication:Bool,mapObject:Any?,SuperVC:UIViewController,completionSuccess: @escaping apiResponseSuccess,completionFailed: @escaping apiResponseFailed) {
        print(Url)
        if ApiUtillity.sharedInstance.isReachable() {
            let authHeader = self.authHeader(ISAuth: Authentication)
            let URL = try! URLRequest(url: Url, method: .post, headers: authHeader)
            Alamofire.upload(multipartFormData: { (multipartFormData) in
                multipartFormData.append(fileData, withName: "upload", fileName: fileName, mimeType: mimeType)
            }, with: URL) { (result) in
                switch result {
                case .success(let upload, _, _):
                    upload.uploadProgress(closure: { (progress) in
                        print("Upload Progress: \(progress.fractionCompleted)")
                    })
                    
                    upload.responseJSON { response in
                        guard
                            let responseJSON = response.result.value
                            
                            else {
                                print(response.error!)
                                completionFailed(response.error!)
                                return
                        }
                        if response.response != nil
                        {
                            print(responseJSON)
                            completionSuccess(responseJSON)
                            return
                        }
                    }
                case .failure(let encodingError):
                    print(encodingError)
                }
            }
        }
        else {
            let errorTemp = NSError(domain:"Please check Internet Connection", code:403, userInfo:nil)
            completionFailed(errorTemp)
            return
        }
    }
    
}

