//
//  ApiUtility.swift
//  TestApp
//
//  Created by MAC on 10/31/17.
//  Copyright © 2017 MAC. All rights reserved.
//

import Foundation
import UIKit
import Alamofire
import ObjectMapper
import NVActivityIndicatorView
private let _sharedInstance = ApiUtillity()

class ApiUtillity: NSObject, NVActivityIndicatorViewable {
    
//    let frame = CGRect(x: (UIScreen.main.bounds.size.width / 2) - 35, y: (UIScreen.main.bounds.size.height / 2) - 35, width: 70, height: 70)
    let activityIndicatorView = NVActivityIndicatorView(frame: CGRect(x: (UIScreen.main.bounds.size.width / 2) - 35, y: (UIScreen.main.bounds.size.height / 2) - 35, width: 100, height: 100), type: NVActivityIndicatorType.circleStrokeSpin, color: UIColor.black)
    
     let activityIndicatorViewUP = NVActivityIndicatorView(frame: CGRect(x: (UIScreen.main.bounds.size.width / 2) - 35, y: (UIScreen.main.bounds.size.height / 2) - 120, width: 100, height: 100), type: NVActivityIndicatorType.circleStrokeSpin, color: UIColor.black)
    
    class var sharedInstance: ApiUtillity {
        return _sharedInstance
    }
    
    func changeDateForamte(_ date: String, conVertFormate:String, currentDateFormate:String) -> String
    {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = currentDateFormate
        let date = dateFormatter.date(from: date)
        dateFormatter.dateFormat = conVertFormate
        return  dateFormatter.string(from: date!)

    }
    
    
    func isValidEmail(testStr:String) -> Bool {
        let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,64}"
        
        let emailTest = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
        return emailTest.evaluate(with: testStr)
    }
    
    func isPasswordValid(_ password : String) -> Bool{
        let passwordTest = NSPredicate(format: "SELF MATCHES %@", "^(?=.*[A-Z])(?=.*[!@#$&*])(?=.*[0-9])(?=.*[a-z]).{6,}$")
        return passwordTest.evaluate(with: password)
    }
    
    func notPrettyString(from object: Any) -> String? {
        if let objectData = try? JSONSerialization.data(withJSONObject: object, options: JSONSerialization.WritingOptions(rawValue: 0)) {
            let objectString = String(data: objectData, encoding: .utf8)
            return objectString
        }
        return nil
    }
    
    func StartProgressUP(view: UIView) {
        
        activityIndicatorViewUP.padding = 20
        view.addSubview(activityIndicatorViewUP)
        activityIndicatorViewUP.startAnimating()
    }
    func StopProgressUp(view: UIView) {
        activityIndicatorViewUP.stopAnimating()
    }
    
    
    func StartProgress(view: UIView)
    {
        UIApplication.shared.beginIgnoringInteractionEvents()
        activityIndicatorView.padding = 20
        view.addSubview(activityIndicatorView)
        activityIndicatorView.startAnimating()
    }
    
    func StopProgress(view: UIView)
    {
        UIApplication.shared.endIgnoringInteractionEvents()
        activityIndicatorView.stopAnimating()
    }

    //MARK:- getDocumentsDirectory
    func getDocumentsDirectory() -> URL {
        let paths = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask)
        return paths[0]
    }
    
    //MARK:- Login Model Get/Set
//    func setLoginModel(modelResponse: ModelSocialLoginBase)
//    {
//        Defaults[.UserIsLogin] = true
//        Defaults[.FacebookResponse] = modelResponse.dictionaryRepresentation()
//    }
    func setLoginModel(modelResponse: ModelSocialLoginBase)
    {
        Defaults[.UserIsLogin] = true
        Defaults[.FacebookResponse] = modelResponse.dictionaryRepresentation()
    }
    func getLoginModel() -> ModelSocialLoginBase?
    {
        if let resultModel = Mapper<ModelSocialLoginBase>().map(JSONObject: Defaults[.FacebookResponse])
        {
            return resultModel
        }
        return nil
    }
   
    func json(from object:Any) -> String? {
        guard let data = try? JSONSerialization.data(withJSONObject: object, options: []) else {
            return nil
        }
        return String(data: data, encoding: String.Encoding.utf8)
    }
    
    //For Get Filename
    func getFilename() -> String {
        let today = Date()
        let dateFormat = DateFormatter()
        dateFormat.dateFormat = "yyyyMMdd-HHmmss"
        let str_filename = "\(dateFormat.string(from: today))"
        return str_filename
    }
    
    // For Get Color Using HexCode
    func getColorIntoHex(Hex:String) -> UIColor {
        if Hex.isEmpty {
            return UIColor.clear
        }
        let scanner = Scanner(string: Hex)
        scanner.scanLocation = 0
        var rgbValue: UInt64 = 0
        scanner.scanHexInt64(&rgbValue)
        let r = (rgbValue & 0xff0000) >> 16
        let g = (rgbValue & 0xff00) >> 8
        let b = rgbValue & 0xff
        return UIColor(red: CGFloat(r) / 0xff, green: CGFloat(g) / 0xff, blue: CGFloat(b) / 0xff, alpha: 1)
    }
    
    func ImageTintColor(Name: UIImageView, Color: String) {
        Name.image = Name.image!.withRenderingMode(.alwaysTemplate)
        Name.tintColor = getColorIntoHex(Hex: Color)
    }
    
    // Popupview
    func AddSubViewtoParentView(parentview: UIView! , subview: UIView!) {
        subview.translatesAutoresizingMaskIntoConstraints = false
        parentview.addSubview(subview);
        parentview.addConstraint(NSLayoutConstraint(item: subview!, attribute: NSLayoutConstraint.Attribute.top, relatedBy: NSLayoutConstraint.Relation.equal, toItem: parentview, attribute: NSLayoutConstraint.Attribute.top, multiplier: 1.0, constant: 0.0))
        parentview.addConstraint(NSLayoutConstraint(item: subview!, attribute: NSLayoutConstraint.Attribute.leading, relatedBy: NSLayoutConstraint.Relation.equal, toItem: parentview, attribute: NSLayoutConstraint.Attribute.leading, multiplier: 1.0, constant: 0.0))
        parentview.addConstraint(NSLayoutConstraint(item: subview!, attribute: NSLayoutConstraint.Attribute.bottom, relatedBy: NSLayoutConstraint.Relation.equal, toItem: parentview, attribute: NSLayoutConstraint.Attribute.bottom, multiplier: 1.0, constant: 0.0))
        parentview.addConstraint(NSLayoutConstraint(item: subview!, attribute: NSLayoutConstraint.Attribute.trailing, relatedBy: NSLayoutConstraint.Relation.equal, toItem: parentview, attribute: NSLayoutConstraint.Attribute.trailing, multiplier: 1.0, constant: 0.0))
        parentview.layoutIfNeeded()
        
    }
    // For Set CornurRadius To All Control
    func setCornurRadius(obj:Any, cornurRadius:CGFloat, isClipToBound:Bool, borderColor:String, borderWidth:CGFloat) {
        if obj is UILabel {
            let tempLabel:UILabel = obj as! UILabel
            tempLabel.layer.cornerRadius = cornurRadius
            tempLabel.clipsToBounds = isClipToBound
            tempLabel.layer.borderColor = self.getColorIntoHex(Hex: borderColor).cgColor
            tempLabel.layer.borderWidth = borderWidth
        }
        if obj is UITextField {
            let tempTextField:UITextField = obj as! UITextField
            tempTextField.layer.cornerRadius = cornurRadius
            tempTextField.clipsToBounds = isClipToBound
            tempTextField.layer.borderColor = self.getColorIntoHex(Hex: borderColor).cgColor
            tempTextField.layer.borderWidth = borderWidth
        }
        if obj is UIButton {
            let tempButton:UIButton = obj as! UIButton
            tempButton.layer.cornerRadius = cornurRadius
            tempButton.clipsToBounds = isClipToBound
            tempButton.layer.borderColor = self.getColorIntoHex(Hex: borderColor).cgColor
            tempButton.layer.borderWidth = borderWidth
        }
        if obj is UITextView {
            let tempTextView:UITextView = obj as! UITextView
            tempTextView.layer.cornerRadius = cornurRadius
            tempTextView.clipsToBounds = isClipToBound
            tempTextView.layer.borderColor = self.getColorIntoHex(Hex: borderColor).cgColor
            tempTextView.layer.borderWidth = borderWidth
        }
        if obj is UIView {
            let tempView:UIView = obj as! UIView
            tempView.layer.cornerRadius = cornurRadius
            tempView.clipsToBounds = isClipToBound
            tempView.layer.borderColor = self.getColorIntoHex(Hex: borderColor).cgColor
            tempView.layer.borderWidth = borderWidth
        }
    }
    
    // MARK:- Document Directory functions
    func getURLFromDocumentDirectory(name:String) -> URL? {
        let fileManager = FileManager.default
        do {
            let documentDirectory = try fileManager.url(for: .documentDirectory, in: .userDomainMask, appropriateFor:nil, create:false)
            let pathComponent = documentDirectory.appendingPathComponent(name)
            return pathComponent
        } catch {
            print(error)
        }
        return nil
    }
    
    func getFileFromDocumentDirectory(name:String) -> URL? {
        if let pathComponent = self.getURLFromDocumentDirectory(name: name)
        {
            let filePath = pathComponent.path
            let fileManager = FileManager.default
            if fileManager.fileExists(atPath: filePath) {
                print("FILE AVAILABLE")
                return pathComponent
            } else {
                print("FILE NOT AVAILABLE")
            }
        }
        return nil
    }
    
    
    func downloadFileToDocumentDirectory(url: URL, to localUrl: URL, completion: @escaping () -> ()) {
        let sessionConfig = URLSessionConfiguration.default
        let session = URLSession(configuration: sessionConfig)
        let request = try! URLRequest(url: url, method: .get)
        print("File Downloading");
        let task = session.downloadTask(with: request) { (tempLocalUrl, response, error) in
            if let tempLocalUrl = tempLocalUrl, error == nil {
                // Success
                if let statusCode = (response as? HTTPURLResponse)?.statusCode {
                    print("Success: \(statusCode)")
                }
                
                do {
                    try FileManager.default.copyItem(at: tempLocalUrl, to: localUrl)
                    completion()
                } catch (let writeError) {
                    print("error writing file \(localUrl) : \(writeError)")
                }
                
            } else {
                print("Failure: %@", error?.localizedDescription ?? "");
            }
        }
        task.resume()
    }
    
    //For Check Internate Availability
    func isReachable() -> Bool
    {
        let reachabilityManager = Alamofire.NetworkReachabilityManager(host: "www.google.com")
        return (reachabilityManager?.isReachable)!
    }
      // MARK:- String To Convert Date
    
    func ConvertStingTodate(forApp date: String) -> Date  {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss" //Your date format
        //according to date format your date string
        guard let date = dateFormatter.date(from: "2019-01-08 12:08:07") else {
            fatalError()
        }
        print(date) //Convert String to Date
        return date
    }
    
    
    // MARK:- Convert Date To String
    
    func convertDateToString(forApp date: String,  dateFormate: String) -> Date {
        let format = DateFormatter()
        format.dateFormat = dateFormate
        let dt: Date = format.date(from: date)!
        return dt
    }
    
//    func convertDateToMonthString(forApp strdate: String) -> String {
//
//        let date = self.convertDateToString(forApp: strdate)
//        let format = DateFormatter()
//        format.dateFormat = "dd MMM yyyy"
//        let dt: String = format.string(from: date)
//        return dt
//    }
    
    //    MARK:- UTC to Local
    
    func UTCToLocal(date:String) -> String {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
        dateFormatter.timeZone = TimeZone(abbreviation: "UTC")
        let dt = dateFormatter.date(from: date)
        dateFormatter.timeZone = TimeZone.current
        dateFormatter.dateFormat = "yyyy-MM-dd hh:mm:ss a"
        
        return dateFormatter.string(from: dt!)
    }
    
    func UserDobe(DOB: String, currentDateForamte: String, newDateFormate: String) -> String {
        let dateFormatterGet = DateFormatter()
        dateFormatterGet.dateFormat = currentDateForamte
        
        let dateFormatterPrint = DateFormatter()
        dateFormatterPrint.dateFormat = newDateFormate
        
        let date: NSDate? = dateFormatterGet.date(from: DOB)! as NSDate
        print(dateFormatterPrint.string(from: date! as Date))
        return (dateFormatterPrint.string(from: date! as Date))
    }
    
    
    func DateFormate(Notificaitiondate: String) -> String {
        let dateFormatterGet = DateFormatter()
        dateFormatterGet.dateFormat = "yyyy-MM-dd HH:mm:ss"
        
        let dateFormatterPrint = DateFormatter()
        dateFormatterPrint.dateFormat = "dd MMM yyyy"
        
        let date: NSDate? = dateFormatterGet.date(from: Notificaitiondate)! as NSDate
        print(dateFormatterPrint.string(from: date! as Date))
        return (dateFormatterPrint.string(from: date! as Date))
    }
    
    func DateFormateTimeStemp(MatchDate: String) -> String {
        let dateFormatterGet = DateFormatter()
        dateFormatterGet.dateFormat = "yyyy-MM-dd HH:mm:ss z"
        
        let dateFormatterPrint = DateFormatter()
        dateFormatterPrint.dateFormat = "dd MMM yyyy"
        
        let date: NSDate? = dateFormatterGet.date(from: MatchDate)! as NSDate
        print(dateFormatterPrint.string(from: date! as Date))
        return (dateFormatterPrint.string(from: date! as Date))
    }
    
    func TimeFormateTimeStemp(MatchTime: String) -> String {
        let dateFormatterGet = DateFormatter()
        dateFormatterGet.dateFormat = "yyyy-MM-dd HH:mm:ss z"
        
        let dateFormatterPrint = DateFormatter()
        dateFormatterPrint.dateFormat = "HH:mm a"
        
        let date: NSDate? = dateFormatterGet.date(from: MatchTime)! as NSDate
        print(dateFormatterPrint.string(from: date! as Date))
        return (dateFormatterPrint.string(from: date! as Date))
    }
    
    
    
    
    func timeAgoSinceDate(date:Date, numericDates:Bool) -> String {
        let calendar = NSCalendar.current
        let unitFlags: Set<Calendar.Component> = [.minute, .hour, .day, .weekOfYear, .month, .year, .second]
        let now = NSDate()
        let earliest = now.earlierDate(date as Date)
        let latest = (earliest == now as Date) ? date : now as Date
        let components = calendar.dateComponents(unitFlags, from: earliest as Date,  to: latest as Date)

        let month_ago = "month ago"
        let day_ago = "day ago"
        let yesterday = "yesterday"
        let week_ago = "week ago"
        let days_ago = "days ago"
        let last_week = "last week"
        let weeks_ago = "weeks ago"
        let last_month = "last month"
        let last_year = "last year"
        let years_ago = "years ago"
        let year_ago = "year ago"
        let hour_ago = "hour ago"
        let hours_ago = "hours ago"
        let just_now = "just now"
        let minutes_ago = "minutes ago"
        let minute_ago = "minute ago"
        let seconds_ago = "seconds ago"
        
        if (components.year! >= 2) {
            
            return "\(components.year!) "+"\(years_ago)"
        } else if (components.year! >= 1){
            if (numericDates){
                return "1 "+"\(year_ago)"
            } else {
                return "\(last_year)"
            }
        } else if (components.month! >= 2) {
            return "\(components.month!) "+"\(month_ago)"
        } else if (components.month! >= 1){
            if (numericDates){
                return "1 "+"\(month_ago)"
            } else {
                return "\(last_month)"
            }
        } else if (components.weekOfYear! >= 2) {
            return "\(components.weekOfYear!) "+"\(weeks_ago)"
        } else if (components.weekOfYear! >= 1){
            if (numericDates){
                return "1 "+"\(week_ago)"
            } else {
                return "\(last_week)"
            }
        } else if (components.day! >= 2) {
            return "\(components.day!) "+"\(days_ago)"
        } else if (components.day! >= 1){
            if (numericDates){
                return "1 "+"\(day_ago)"
            } else {
                return "\(yesterday)"
            }
        } else if (components.hour! >= 2) {
            return "\(components.hour!) "+"\(hours_ago)"
        } else if (components.hour! >= 1){
            if (numericDates){
                return "1 "+"\(hour_ago)"
            } else {
                return "An "+"\(hour_ago)"
            }
        } else if (components.minute! >= 2) {
            return "\(components.minute!) "+"\(minutes_ago)"
        } else if (components.minute! >= 1){
            if (numericDates){
                return "1 "+"\(minute_ago)"
            } else {
                return "A "+"\(minute_ago)"
            }
        } else if (components.second! >= 3) {
            return "\(components.second!) "+"\(seconds_ago)"
        } else {
            return "\(just_now)"
        }
    }
    
   
}
