//
//  SignupManager.swift
//  Mensa
//
//  Created by social in on 18/10/16.
//  Copyright © 2016 Dataready. All rights reserved.
//

import Foundation
import UIKit
import Alamofire
import ObjectMapper

let IS_LOG_ENABLE = true

class APIClient<M: Mappable> {
    
    typealias apiResponseSuccess = (_ success: M) -> Void
    typealias apiResponseUploading = (_ success: Double) -> Void
    typealias apiResponseSuccessJSON = (_ success: Any) -> Void
    typealias apiResponseFailed = (_ failed: Error) -> Void
    typealias apiResponseStatus = (_ Status: Bool) -> Void
    
    let storyboard:UIStoryboard = UIStoryboard.init(name: "Main", bundle: nil)
    
        func authHeader(ISAuth:Bool) -> [String : String] {
    
            var header = [String : String]()
            let model = ApiUtillity.sharedInstance.getLoginModel()
    
            if let user_Api = model?.facebookResult?.userApi, user_Api.count != 0
            {
                header = ["X-Authorization": user_Api,
                          "Content-Type": "application/x-www-form-urlencoded"] as [String : String]
            }
            else
            {
                header = ["Content-Type": "application/x-www-form-urlencoded"] as [String : String]
            }
    
            if (IS_LOG_ENABLE)
            {
                print(header)
            }
            return header
        }
    
//    func authHeader(ISAuth:Bool) -> HTTPHeaders
//    {
//        var header = HTTPHeaders()
//        let model = ApiUtillity.sharedInstance.getLoginModel()
//
//        if let user_Api = model?.facebookResult?.userApi, user_Api.count != 0
//        {
//            header = [
//                .authorization(bearerToken: user_Api),
//                .contentType("application/x-www-form-urlencoded")
//            ]
//        }
//        else
//        {
//            header = [
//                .contentType("application/x-www-form-urlencoded")
//            ]
//        }
//
//        return header
//    }
    
    
    
    //MARK:- JSON converter
    func prettyPrint(with json: [String:Any]) -> String {
        let data = try! JSONSerialization.data(withJSONObject: json, options: .prettyPrinted)
        let string = NSString(data: data, encoding: String.Encoding.utf8.rawValue)
        return string! as String
    }
    
    
    //MARK:- API_GET
    func API_GET(Url:String, Params:[String : AnyObject], Spinner: Int, Authentication:Bool,Progress:Bool,Alert:Bool,Offline:Bool, SuperVC:UIViewController, completionSuccess: @escaping apiResponseSuccess, completionFailed: @escaping apiResponseFailed) {
        
        if (IS_LOG_ENABLE)
        {
            print(Url)
            print( prettyPrint(with: Params))
        }
        
        if ApiUtillity.sharedInstance.isReachable()
        {
            if Progress
            {
                if Spinner == 1
                {
                    ApiUtillity.sharedInstance.StartProgress(view: SuperVC.view)
                }
                else {
                    UtilityClass.showhud()
                }
            }
            
            let authHeader = self.authHeader(ISAuth: Authentication)
            
            //            AF.request(Url, method: .get, parameters: Params, encoder: URLEncoding.default, headers: authHeader, interceptor: nil)
            
            
            
            Alamofire.request(Url, method: .get, parameters: Params, encoding: URLEncoding.default, headers: authHeader).responseJSON
                { response in
                    
                    if Progress
                    {
                        if Spinner == 1
                        {
                            ApiUtillity.sharedInstance.StopProgress(view: SuperVC.view)
                        }
                        else
                        {
                            UtilityClass.hidehud()
                        }
                    }
                    
                    debugPrint(response)
                    guard
                        let responseJSON = response.result.value
                        
                        else {
                            print(response.error!)
                            completionFailed(response.error!)
                            return
                    }
                    
                    if (IS_LOG_ENABLE)
                    {
                        print(responseJSON)
                    }
                    
                    if response.result.value != nil
                    {
                        if let resultModel = Mapper<M>().map(JSONObject: responseJSON)
                        {
                            completionSuccess(resultModel)
                            return
                        }
                    }
                    else
                    {
                        print(response.error ?? "")
                    }
            }
        }
        else
        {
            let errorTemp = NSError(domain:"Please check Internet Connection", code:403, userInfo:nil)
            completionFailed(errorTemp)
            // Toast(text: "No Internet Connection").show()
            
            return
        }
    }
    
    // MARK:- API_POST
    func API_POST(Url:String,Params:[String : AnyObject],Authentication:Bool,Progress:Bool,Alert:Bool,Offline:Bool,SuperVC:UIViewController,completionSuccess: @escaping apiResponseSuccess,completionFailed: @escaping apiResponseFailed) {
        
        if (IS_LOG_ENABLE)
        {
            print(Url)
            print(prettyPrint(with: Params))
        }
        
        if ApiUtillity.sharedInstance.isReachable() {
            
            if Progress
            {
                UtilityClass.showhud()
            }
            
            let authHeader = self.authHeader(ISAuth: Authentication)
            Alamofire.request(Url, method: .post, parameters: Params, encoding: URLEncoding.default, headers: authHeader).responseJSON
                { response in
                    //debugPrint(response)
                    print(response)
                    if Progress
                    {
                        UtilityClass.hidehud()
                    }
                    
                    guard let responseJSON = response.result.value else {
                        completionFailed(response.error!)
                        return
                    }
                    if (IS_LOG_ENABLE)
                    {
                        print(responseJSON)
                    }
                    if response.result.value != nil
                    {
                        if let resultModel = Mapper<M>().map(JSONObject: responseJSON)
                        {
                            completionSuccess(resultModel)
                            return
                        }
                    }
                    else
                    {
                        print(response.error ?? "")
                    }
            }.responseString
                { response in
                    debugPrint(response)
            }
        }
        else {
            let errorTemp = NSError(domain:"Please check Internet Connection", code:403, userInfo:nil)
            completionFailed(errorTemp)
            //   Toast(text: "No Internet Connection").show()
            return
        }
    }
    
    //MARK:- API_PUT
    func API_PUT(Url:String,Params:[String : AnyObject],Authentication:Bool,Progress:Bool,Alert:Bool,Offline:Bool,SuperVC:UIViewController,completionSuccess: @escaping apiResponseSuccess,completionFailed: @escaping apiResponseFailed) {
        
        if (IS_LOG_ENABLE)
        {
            print(Url)
            print( prettyPrint(with: Params))
        }
        
        if ApiUtillity.sharedInstance.isReachable() {
            
            if Progress{
                UtilityClass.showhud()
            }
            
            let authHeader = self.authHeader(ISAuth: Authentication)
            Alamofire.request(Url, method: .put, parameters: Params, encoding: URLEncoding.default, headers: authHeader).responseJSON
                { response in
                    
                    if Progress{
                        UtilityClass.hidehud()
                    }
                    
                    debugPrint(response)
                    guard
                        let responseJSON = response.result.value
                        
                        else {
                            
                            completionFailed(response.error!)
                            return
                    }
                    if (IS_LOG_ENABLE)
                    {
                        print(responseJSON)
                    }
                    if response.response != nil
                    {
                        if let resultModel = Mapper<M>().map(JSONObject: responseJSON)
                        {
                            completionSuccess(resultModel)
                            return
                        }
                    }
            } .responseString
                { response in
                    debugPrint(response)
                    
            }
        }
        else {
            let errorTemp = NSError(domain:"Please check Internet Connection", code:403, userInfo:nil)
            completionFailed(errorTemp)
            // Toast(text: "No Internet Connection").show()
            return
        }
    }
    
    //MARK:- API_DELETE
    func API_DELETE(Url:String,Params:[String : AnyObject],Authentication:Bool,Progress:Bool,Alert:Bool,Offline:Bool,SuperVC:UIViewController,completionSuccess: @escaping apiResponseSuccess,completionFailed: @escaping apiResponseFailed) {
        
        if (IS_LOG_ENABLE)
        {
            print(Url)
            print( prettyPrint(with: Params))
        }
        
        if ApiUtillity.sharedInstance.isReachable() {
            
            if Progress{
                UtilityClass.showhud()
            }
            
            let authHeader = self.authHeader(ISAuth: Authentication)
            Alamofire.request(Url, method: .delete, parameters: Params, encoding: URLEncoding.default, headers: authHeader).responseJSON
                { response in
                    
                    if Progress{
                        UtilityClass.hidehud()
                    }
                    
                    guard
                        let responseJSON = response.result.value
                        
                        else {
                            
                            completionFailed(response.error!)
                            return
                    }
                    if (IS_LOG_ENABLE)
                    {
                        print(responseJSON)
                    }
                    if response.response != nil
                    {
                        if let resultModel = Mapper<M>().map(JSONObject: responseJSON)
                        {
                            completionSuccess(resultModel)
                            return
                        }
                    }
            }
        }
        else {
            let errorTemp = NSError(domain:"Please check Internet Connection", code:403, userInfo:nil)
            completionFailed(errorTemp)
            //    Toast(text: "No Internet Connection").show()
            return
        }
    }
    
    
    
    //MARK:- API_UPLOAD
    func API_UPLOAD(Url:String,Params:[String : AnyObject],fileName:String,fileData:Data,mimeType:String,fileTag:String,Authentication:Bool,Progress:Bool,Alert:Bool,Offline:Bool,SuperVC:UIViewController,uploadingProgress: @escaping apiResponseUploading,completionSuccess: @escaping apiResponseSuccess,completionFailed: @escaping apiResponseFailed) {
        
        if (IS_LOG_ENABLE)
        {
            print(Url)
            print( prettyPrint(with: Params))
        }
        
        if ApiUtillity.sharedInstance.isReachable() {
            
            
            if Progress{
                UtilityClass.showhud()
            }
            
            let authHeader = self.authHeader(ISAuth: Authentication)
            let URL = try! URLRequest(url: Url, method: .post, headers: authHeader)
            
            Alamofire.upload(multipartFormData: { (multipartFormData) in
                multipartFormData.append(fileData, withName: fileTag, fileName: fileName, mimeType: mimeType)
            }, with: URL) { (result) in
                switch result {
                    
                case .success(let upload, _, _):
                    
                    if Progress{
                        UtilityClass.hidehud()
                    }
                    
                    upload.uploadProgress(closure: { (progress) in
                        
                        if (IS_LOG_ENABLE)
                        {
                            print("Upload Progress: \(progress.fractionCompleted)")
                        }
                        uploadingProgress(progress.fractionCompleted)
                    })
                    
                    upload.responseJSON { response in
                        guard
                            let responseJSON = response.result.value
                            
                            else {
                                print(response.error!)
                                completionFailed(response.error!)
                                return
                        }
                        if (IS_LOG_ENABLE)
                        {
                            print(responseJSON)
                        }
                        if response.response != nil
                        {
                            if let resultModel = Mapper<M>().map(JSONObject: responseJSON)
                            {
                                completionSuccess(resultModel)
                                return
                            }
                        }
                    }
                case .failure(let encodingError):
                    
                    if Progress{
                        UtilityClass.hidehud()
                    }
                    
                    print(encodingError)
                }
            }
        }
        else {
            let errorTemp = NSError(domain:"Please check Internet Connection", code:403, userInfo:nil)
            completionFailed(errorTemp)
            //  Toast(text: "No Internet Connection").show()
            return
        }
    }
    
    func API_READ_JSON(filePath:URL,completionSuccess: @escaping apiResponseSuccess,completionFailed: @escaping apiResponseFailed)
    {
        do {
            
            let data = try Data(contentsOf: filePath)
            let json = try JSONSerialization.jsonObject(with: data, options: [])
            if let responseJSON = json as? [String: Any] {
                // json is a dictionary
                if let resultModel = Mapper<M>().map(JSON: responseJSON)
                {
                    completionSuccess(resultModel)
                    return
                }
                
            } else if let object = json as? [Any] {
                // json is an array
                print(object)
            } else {
                print("JSON is invalid")
            }
            
        } catch {
            print(error.localizedDescription)
        }
    }
    
}


