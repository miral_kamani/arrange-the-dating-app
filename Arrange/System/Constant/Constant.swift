//
//  Constant.swift
//  Arrange
//
//  Created by Miral Kamani on 07/02/20.
//  Copyright © 2020 Miral Kamani. All rights reserved.
//

import Foundation
import UIKit

//MARK:- APP Code Colour
let Iris = UIColor(red: Int(94.0/255.0), green: Int(90.0/255.0), blue: Int(186.0/255.0), a: 1)
let Twilight = UIColor(red: Int(83.0/255.0), green: Int(180.0/255.0), blue: Int(157.0/255.0), a: 1)
let CharcoalGrey = UIColor(red: Int(67.0/255.0), green: Int(66.0/255.0), blue: Int(72.0/255.0), a: 1)
let BabyPurple = UIColor(red: Int(192.0/255.0), green: Int(152.0/255.0), blue: Int(255.0/255.0), a: 1)
let LighterPurple = UIColor(red: Int(140.0/255.0), green: Int(94.0/255.0), blue: Int(255.0/255.0), a: 1)
let white42 = UIColor(red: Int(248.0/255.0), green: Int(248.0/255.0), blue: Int(248.0/255.0), a: Int(0.42))

//MARK:- Constant
var constant = String()
var Religion = Int()
var Community = Int()
var Career = Int()
var Education = String()
var traits = [Int]()
var RaisedIn = [Int]()
var age = String()
var selectedCountry = String()
var selectedState = String()
var selectedCity = String()
var Distance = String()
let kconstantDevice = "iphone"
var ProfileImage = UIImage()

//MARK:- No data found
let noLike = "noLike"
let noChat = "noChat"
let noMatch =  "noMatch"
let imageSize = CGSize(width: 65, height: 65)

// MARK:- API

let baseURL = "http://socialinfotech.in/development/dateing_app/"
let appVersion = "api/v1/"

//MARK:- API URL

let ws_Post_social_login = baseURL+appVersion+"social_login"
let ws_Get_Career = baseURL+appVersion+"career"
let ws_Get_Community = baseURL+appVersion+"community"
let ws_Get_Raised_in = baseURL+appVersion+"raised_in"
let ws_Get_Religion = baseURL+appVersion+"religion"
let ws_Get_Card = baseURL+appVersion+"user_card"
let ws_get_Interest = baseURL+appVersion+"traits"
let WS_ProfileImageURL = baseURL+"uploads/original/"
let ws_Image_Upload = baseURL+appVersion+"upload"
let ws_Edit_Profile = baseURL+appVersion+"editprofile"
let ws_Insert_Images = baseURL+appVersion+"user_images"
let ws_Get_Images = baseURL+appVersion+"user_images"
let ws_Delete_Account = baseURL+appVersion+"delete_account"
let ws_Post_Logout = baseURL+appVersion+"logout"
let ws_Post_Update = baseURL+appVersion+"update_profile"
let ws_Get_Country = baseURL+appVersion+"get_countries"
let ws_Get_State = baseURL+appVersion+"get_states/"
let ws_Get_City = baseURL+appVersion+"get_cities/"
let ws_Post_LikeProfile = baseURL+appVersion+"is_liked"
let ws_Get_Matches = baseURL+appVersion+"get_matched"
let ws_Get_Received_Likes = baseURL+appVersion+"received"
let ws_Get_Given_Likes = baseURL+appVersion+"given"
let ws_Post_ChatRoom = baseURL+appVersion+"chat_room"
let ws_Get_Chatroom = baseURL+appVersion+"chat_room"
let ws_Put_isAccept = baseURL+appVersion+"is_accepted/"
let ws_Post_ChatMessage = baseURL+appVersion+"chat_message/"
let ws_Get_OldMessages = baseURL+appVersion+"messages_old/?offsetID="
let ws_Post_Filter = baseURL+appVersion+"filter"
let ws_Delete_Image = baseURL+appVersion+"delete_image"
let ws_Put_Unmatch = baseURL+appVersion+"unmatch"
