//
//  ReportVC.swift
//  Arrange
//
//  Created by Miral Kamani on 22/02/20.
//  Copyright © 2020 Miral Kamani. All rights reserved.
//

import UIKit

class ReportVC: UIViewController {

    //MARK:- Outlets
    @IBOutlet var txtEmail: UITextField!
    @IBOutlet var txtTitle: UITextField!
    @IBOutlet var txtDescription: UITextView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
       self.txtDescription.text = "Description"
            self.txtDescription.textColor = UIColor.lightGray
            self.txtDescription.font = UIFont(name: "Avenir-Book", size: 16)
    }
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    @IBAction func btnBack(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
}
extension ReportVC: UITextViewDelegate
{
    func textViewDidBeginEditing(_ textView: UITextView) {
        if textView == txtDescription
        {
            if textView.textColor == UIColor.lightGray {
                textView.text = nil
                textView.textColor = UIColor.black
            }
        }
        
    }
    func textViewDidEndEditing(_ textView: UITextView) {
        if textView == txtDescription
        {
            if textView.text.isEmpty {
                textView.text = "Description"
                textView.textColor = UIColor.lightGray
            }
        }
    }
}


