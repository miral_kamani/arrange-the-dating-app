//
//  UnmatchVC.swift
//  Arrange
//
//  Created by Miral Kamani on 20/02/20.
//  Copyright © 2020 Miral Kamani. All rights reserved.
//

import UIKit
import Toaster
class UnmatchVC: UIViewController {
    
    //MARK:- Outlets

    @IBOutlet var btnUnmatchPartner: UIButton!
    @IBOutlet var btnCancel: UIButton!
    @IBOutlet var ViewCancel: UIViewX!
    @IBOutlet var ViewUnmatch: UIViewX!
    @IBOutlet var BottomView: UIView!
    @IBOutlet var tblReload: UITableView!
    
     //MARK:- Variable
    
    var arrayCommunity = ["Inappropriate profile","Inappropriate messages","Stolen photo","Harasser","Spam","Unresponsive","Not interested","Other"]
    var selectedIndex: Int?
    var UnmatchID = Int()
    var chatRoomId = Int()
    override func viewDidLoad() {
        super.viewDidLoad()
        self.RegisterXIB()
        tblReload.tableFooterView = UIView()
        self.btnUnmatchPartner.addTarget(self, action: #selector(OnClickUnmatch), for: .touchUpInside)
        self.btnCancel.addTarget(self, action: #selector(OnClickCancel), for: .touchUpInside)
        self.ViewUnmatch.isHidden = true
    }
    override func viewWillAppear(_ animated: Bool) {
        self.ViewUnmatch.isHidden = true
    }
    @IBAction func backTapped(_ sender: UIButton)
    {
        self.navigationController?.popViewController(animated: true)
    }
    @objc func OnClickCellbtn(sender: UIButton)
    {
        selectedIndex = sender.tag
        tblReload.reloadData()
    }
    @objc func OnClickCancel(sender: UIButton)
    {
        self.ViewUnmatch.isHidden = true
        self.navigationController?.popViewController(animated: true)
    }
    @objc func OnClickUnmatch(sender: UIButton)
    {
        self.WS_Put_Unmatch()
    }
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
}


extension UnmatchVC: UITableViewDataSource,UITableViewDelegate
{
    func RegisterXIB()
    {
        self.tblReload.register(UINib(nibName: "GenderCell", bundle: nil), forCellReuseIdentifier: "GenderCell")
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrayCommunity.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        let cell = tableView.dequeueReusableCell(withIdentifier: "GenderCell", for: indexPath) as! GenderCell
        cell.labelOption.text = arrayCommunity[indexPath.row]
        if selectedIndex == indexPath.row
        {
            cell.imageTick.image = UIImage(named: "check")
            cell.imageTick.isHidden = false
            self.ViewUnmatch.isHidden = false
        }
        else
        {
            cell.imageTick.isHidden = true
        }
        cell.btnClick.tag = indexPath.row
        cell.btnClick.addTarget(self, action: #selector(OnClickCellbtn), for: .touchUpInside)
        return cell
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 50.0
    }
    
    
}

extension UnmatchVC
{
    func WS_Put_Unmatch()
    {
        var parameters = [String: Any]()
       
        parameters = [
            "receiver_id": UnmatchID,
            "chat_room_id": chatRoomId]
        print(parameters)
        APIClient<ModelBaseSuccess>().API_PUT(Url: ws_Put_Unmatch, Params: parameters as [String:AnyObject], Authentication: true, Progress: true, Alert: true, Offline: false, SuperVC: self, completionSuccess: { (modelResponse) in
            UtilityClass.showhud()
            print("---------------------")
            print("Login Result:",modelResponse.toJSONString(prettyPrint: true)!)
            print("---------------------")
            UtilityClass.hidehud()
            if(modelResponse.success == true){
                
                Toast(text: modelResponse.message ?? "").show()
                let vc = self.storyboard?.instantiateViewController(withIdentifier: "MainCardVC") as! MainCardVC
                self.navigationController?.pushViewController(vc, animated: true)
                
            }
            else {
                Toast(text: modelResponse.message ?? "").show()
            }
            
        }) { (failed) in
        }
    }
}
