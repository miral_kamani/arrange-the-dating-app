//
//  GenderVC.swift
//  Arrange
//
//  Created by Miral Kamani on 13/02/20.
//  Copyright © 2020 Miral Kamani. All rights reserved.
//

import UIKit

class GenderVC: UIViewController {
    
    //MARK:- Outlets
    
    @IBOutlet var CollectionReload: UICollectionView!
    
    //MARK:- Variable
    
    var arrayGender = ["Male","Female","Both"]
    static var Gender = String()
    static var Remove = Bool()
    override func viewDidLoad() {
        super.viewDidLoad()
        self.RegisterXIB()
    }
    @IBAction func backTapped(_ sender: UIButton)
    {
        self.navigationController?.popViewController(animated: true)
    }
    override func viewWillAppear(_ animated: Bool) {

    }
    @objc func OnClickCellbtn(sender: UIButton)
    {
        let dict = arrayGender[sender.tag]
        GenderVC.Gender = dict
        Defaults[.Gender] = GenderVC.Gender
        DilMilFilterVC.isRemove = 1
        CollectionReload.reloadData()
    }
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    @IBAction func btnDone(_ sender: UIButton)
    {
        self.navigationController?.popViewController(animated: true)
    }

}

extension GenderVC: UICollectionViewDelegate,UICollectionViewDelegateFlowLayout,UICollectionViewDataSource
{
    func RegisterXIB()
    {
        self.CollectionReload.register(UINib(nibName: "UserSelectionCell", bundle: nil), forCellWithReuseIdentifier: "UserSelectionCell")
    }
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return arrayGender.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "UserSelectionCell", for: indexPath) as! UserSelectionCell
        cell.labelTitleName.text = arrayGender[indexPath.row]
        if GenderVC.Gender == arrayGender[indexPath.row]
        {
            cell.WholeView.layer.borderColor = UIColor(red: 140, green: 94, blue: 255).cgColor
            cell.WholeView.layer.borderWidth = 2.0
        }
        else
        {
            cell.WholeView.layer.borderColor = UIColor.clear.cgColor
        }
        cell.btnSelect.tag = indexPath.row
        cell.btnSelect.addTarget(self, action: #selector(OnClickCellbtn), for: .touchUpInside)
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView,layout collectionViewLayout: UICollectionViewLayout,sizeForItemAt indexPath: IndexPath) -> CGSize
    {
        return CGSize(width: CollectionReload.frame.size.width - 20, height: 75.0)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 10.0
    }
}
