//
//  EducationVC.swift
//  Arrange
//
//  Created by Miral Kamani on 13/02/20.
//  Copyright © 2020 Miral Kamani. All rights reserved.
//

import UIKit

class EducationVC: UIViewController {
    
    //MARK:- Outlets
    
    @IBOutlet var tblReload: UITableView!
    
     //MARK:- Variable
    
    var arrayEducation = ["Any","Doctorate","Masters","Bachlores","Associates","Trade School","High School","No Education"]
    var selectedIndex: Int?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        tblReload.tableFooterView = UIView()
        self.RegisterXIB()
    }
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    @IBAction func backTapped(_ sender: UIButton)
    {
        self.navigationController?.popViewController(animated: true)
    }
    @objc func OnClickCellbtn(sender: UIButton)
    {
        selectedIndex = sender.tag
        tblReload.reloadData()
        
    }
}
extension EducationVC: UITableViewDataSource,UITableViewDelegate
{
    func RegisterXIB()
    {
        self.tblReload.register(UINib(nibName: "GenderCell", bundle: nil), forCellReuseIdentifier: "GenderCell")
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrayEducation.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        let cell = tableView.dequeueReusableCell(withIdentifier: "GenderCell", for: indexPath) as! GenderCell
        cell.labelOption.text = arrayEducation[indexPath.row]
        if selectedIndex == indexPath.row
        {
            cell.imageTick.image = UIImage(named: "check")
            constant = cell.labelOption.text!
            cell.imageTick.isHidden = false
        }
        else
        {
            cell.imageTick.isHidden = true
        }
        cell.btnClick.tag = indexPath.row
        cell.btnClick.addTarget(self, action: #selector(OnClickCellbtn), for: .touchUpInside)
        return cell
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 50.0
    }
    
}
