//
//  RaisedInVC.swift
//  Arrange
//
//  Created by Miral Kamani on 13/02/20.
//  Copyright © 2020 Miral Kamani. All rights reserved.
//

import UIKit
import Toaster

class RaisedInVC: UIViewController {
    
    //MARK:- Outlets
    
    @IBOutlet var CollectionReload: UICollectionView!
    
    //MARK:- Variable
    
    var arrayRaisedIn = [ModelRaisedInResult]()
    static var RaisedInID = Int()
    static var RaisedInName = String()
    static var Remove = Bool()
    override func viewDidLoad() {
        super.viewDidLoad()
        self.RegisterXIB()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.WS_Get_RaisedIn()
    }
    
    @IBAction func backTapped(_ sender: UIButton)
    {
        constant = ""
        self.navigationController?.popViewController(animated: true)
    }
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    @objc func OnClickCellbtn(sender: UIButton)
    {
        let dict = arrayRaisedIn[sender.tag]
        RaisedInVC.RaisedInID = dict.raisedInId!
        RaisedInVC.RaisedInName = dict.raisedInName!
        Defaults[.raisedIn] = RaisedInVC.RaisedInName
        DilMilFilterVC.isRemove = 1
        CollectionReload.reloadData()
    }
    
    @IBAction func btnDone(_ sender: UIButton)
    {
        self.navigationController?.popViewController(animated: true)
    }
    
}

extension RaisedInVC: UICollectionViewDelegate,UICollectionViewDelegateFlowLayout,UICollectionViewDataSource
{
    func RegisterXIB()
    {
        self.CollectionReload.register(UINib(nibName: "UserSelectionCell", bundle: nil), forCellWithReuseIdentifier: "UserSelectionCell")
    }
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return arrayRaisedIn.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "UserSelectionCell", for: indexPath) as! UserSelectionCell
        let dict = arrayRaisedIn[indexPath.row]
        if let name = dict.raisedInName, name.count != 0
        {
            cell.labelTitleName.text = name
        }
        if RaisedInVC.RaisedInID == dict.raisedInId
        {
            cell.WholeView.layer.borderColor = UIColor(red: 140, green: 94, blue: 255).cgColor
            cell.WholeView.layer.borderWidth = 2.0
        }
        else
        {
            cell.WholeView.layer.borderColor = UIColor.clear.cgColor
        }
        cell.btnSelect.tag = indexPath.row
        cell.btnSelect.addTarget(self, action: #selector(OnClickCellbtn), for: .touchUpInside)
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView,layout collectionViewLayout: UICollectionViewLayout,sizeForItemAt indexPath: IndexPath) -> CGSize
    {
        return CGSize(width: CollectionReload.frame.size.width - 20, height: 75.0)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 10.0
    }
}

extension RaisedInVC
{
    func WS_Get_RaisedIn()
    {
        APIClient<ModelRaisedInBase>().API_GET(Url: ws_Get_Raised_in, Params: [:], Spinner: 1, Authentication: true, Progress: true, Alert: true, Offline: false, SuperVC: self, completionSuccess: { (modelResponse) in
            
            print("---------------------")
            print("Result:",modelResponse.toJSONString(prettyPrint: true)!)
            print("---------------------")
            self.arrayRaisedIn.removeAll()
            if(modelResponse.success == true)
            {
                if let result = modelResponse.raisedInResult, result.count != 0
                {
                    for data in result
                    {
                        self.arrayRaisedIn.append(data)
                    }
                }
            }
            else
            {
                if modelResponse.message == "Authentication API is invalid!" {
                    let vc = self.storyboard?.instantiateViewController(withIdentifier: "LogoutVc") as! LogoutVc
                    self.navigationController?.pushViewController(vc, animated: false)
                }
                else {
                    UtilityClass.hidehud()
                    Toast(text: modelResponse.message ?? "").show()
                }
            }
            self.CollectionReload.reloadData()
        }) { (failed) in
            
        }
    }
}
