//
//  AgeVC.swift
//  Arrange
//
//  Created by Miral Kamani on 13/02/20.
//  Copyright © 2020 Miral Kamani. All rights reserved.
//

import UIKit
import RangeSeekSlider
class AgeVC: UIViewController {
    
    //MARK:- Outlets
    
    @IBOutlet fileprivate weak var rangeSlider: RangeSeekSlider!
    
     //MARK:- Variable
    
    static var MinAge = String()
    static var MaxAge = String()
    static var Age = String()
    override func viewDidLoad() {
        super.viewDidLoad()
        setup()
    }
    override func viewWillAppear(_ animated: Bool) {
        
    }
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    private func setup() {
        
        rangeSlider.delegate = self
        rangeSlider.minValue = 18
        rangeSlider.maxValue = 60
        rangeSlider.handleColor = UIColor(red: 192, green: 162, blue: 255)
        rangeSlider.handleDiameter = 30.0
        rangeSlider.selectedHandleDiameterMultiplier = 1.3
        rangeSlider.numberFormatter.numberStyle = .none
        rangeSlider.minLabelFont = UIFont(name: "Avenir-Book", size: 15.0)!
        rangeSlider.maxLabelFont = UIFont(name: "Avenir-Book", size: 15.0)!
    }
    @IBAction func backTapped(_ sender: UIButton)
    {
        constant = ""
        self.navigationController?.popViewController(animated: true)
        
    }
    @IBAction func btnDone(_ sender: UIButton)
    {
        if constant == ""
        {
            constant = Defaults[.ages]
            self.navigationController?.popViewController(animated: true)
        }
        else
        {
             self.navigationController?.popViewController(animated: true)
        }
       
    }
}
// MARK: - RangeSeekSliderDelegate

extension AgeVC: RangeSeekSliderDelegate {

    func rangeSeekSlider(_ slider: RangeSeekSlider, didChange minValue: CGFloat, maxValue: CGFloat) {
        
        print("Standard slider updated. Min Value: \(minValue) Max Value: \(maxValue)")
        let min = Int(minValue)
        let max = Int(maxValue)
        AgeVC.Age = "\(min)" + "-" + "\( max)"
        AgeVC.MinAge = "\(min)"
        AgeVC.MaxAge = "\(max)"
        constant = AgeVC.Age
        Defaults[.ages] = AgeVC.Age
    }
    
    func didStartTouches(in slider: RangeSeekSlider) {
        print("did start touches")
    }
    
    func didEndTouches(in slider: RangeSeekSlider) {
        print("did end touches")
    }
}
