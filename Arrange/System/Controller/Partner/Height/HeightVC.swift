//
//  HeightVC.swift
//  Arrange
//
//  Created by Miral Kamani on 13/02/20.
//  Copyright © 2020 Miral Kamani. All rights reserved.
//

import UIKit
import RangeSeekSlider


class HeightFormatter : NumberFormatter{
    override func string(from number: NSNumber) -> String? {
        let feet = number.int32Value/12
        let inches = number.int32Value % 12
        let height = "\(feet)"+"'"+"\(inches)"+"\""
        return height
    }
}
class HeightVC: UIViewController {
    
    //MARK:- Outlets
    
    @IBOutlet fileprivate weak var rangeSlider: RangeSeekSlider!
    
    //MARK:- Variable
    
    static var Height = String()
    static var MaxHeight = String()
    static var MinHeight = String()
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setup()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        constant = ""
    }
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    private func setup() {
        
        rangeSlider.delegate = self
        rangeSlider.minValue = 56
        rangeSlider.maxValue = 80
        rangeSlider.handleColor = UIColor(red: 192, green: 162, blue: 255)
        rangeSlider.handleDiameter = 30.0
        rangeSlider.selectedHandleDiameterMultiplier = 1.3
        rangeSlider.numberFormatter = HeightFormatter()
        rangeSlider.minLabelFont = UIFont(name: "Avenir-Book", size: 15.0)!
        rangeSlider.maxLabelFont = UIFont(name: "Avenir-Book", size: 15.0)!
    }
    
    @IBAction func backTapped(_ sender: UIButton)
    {
        constant = ""
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func btnDone(_ sender: UIButton)
    {
        self.navigationController?.popViewController(animated: true)
    }
    
}

// MARK: - RangeSeekSliderDelegate

extension HeightVC: RangeSeekSliderDelegate {
    
    func InchesToFeetInt(inches:Float) -> Float
    {
        return Float(inches/12)
    }
    
    func rangeSeekSlider(_ slider: RangeSeekSlider, didChange minValue: CGFloat, maxValue: CGFloat) {
        
        print("Standard slider updated. Min Value: \(minValue) Max Value: \(maxValue)")
        print(maxValue)
        print(minValue)
        let a = InchesToFeetInt(inches: Float(minValue))
        let value = Float(a)
        let integer = Int(value)
        let decimal = value.truncatingRemainder(dividingBy: 1)
        let round = decimal.roundeds(toPlaces: 1)
        let v = Int(round*10)
        let Inch = "\(integer)"+"'"+"\(v)"+"\""
        print(Inch)
        
        let b = InchesToFeetInt(inches: Float(maxValue))
        let Max = Float(b)
        let int = Int(Max)
        let deci = Max.truncatingRemainder(dividingBy: 1)
        let ro = deci.roundeds(toPlaces: 1)
        let m = Int(ro*10)
        let Inchss = "\(int)"+"'"+"\(m)"+"\""
        print(Inchss)
        HeightVC.Height = "\(Inch)" + "-" + "\( Inchss)"
        HeightVC.MinHeight = "\(Inch)"
        HeightVC.MaxHeight = "\(Inchss)"
        Defaults[.Height] = HeightVC.Height
    }
    
    func didStartTouches(in slider: RangeSeekSlider) {
        print("did start touches")
    }
    
    func didEndTouches(in slider: RangeSeekSlider) {
        print("did end touches")
    }
}
