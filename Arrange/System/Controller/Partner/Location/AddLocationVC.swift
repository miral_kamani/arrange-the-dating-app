//
//  AddLocationVC.swift
//  Arrange
//
//  Created by Miral Kamani on 19/02/20.
//  Copyright © 2020 Miral Kamani. All rights reserved
//

import UIKit
import GoogleMaps
import GooglePlaces

class AddLocationVC: UIViewController,UISearchBarDelegate , LocateOnTheMap,GMSAutocompleteFetcherDelegate {
    
    @IBOutlet var GoogleMapContainer: UIView!
    var GoogleMapsView: GMSMapView!
    var searchResultController: SearchResultController!
    var resultsArray = [String]()
    var gmsFetcher: GMSAutocompleteFetcher!
    override func viewDidLoad() {
        super.viewDidLoad()
     
    }
    public func didFailAutocompleteWithError(_ error: Error) {
      
      }
      
      
      public func didAutocomplete(with predictions: [GMSAutocompletePrediction]) {
       
          for prediction in predictions {
              
            if let prediction = prediction as GMSAutocompletePrediction?{
                  self.resultsArray.append(prediction.attributedFullText.string)
              }
          }
          self.searchResultController.reloadDataWithArray(self.resultsArray)
        
          print(resultsArray)
      }
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(true)
        self.GoogleMapsView = GMSMapView(frame: self.GoogleMapContainer.frame)
        self.view.addSubview(self.GoogleMapsView)
        searchResultController = SearchResultController()
        searchResultController.delegate = self
        gmsFetcher = GMSAutocompleteFetcher()
        gmsFetcher.delegate = self
    }
    
    @IBAction func btnSearch(_ sender: UIButton)
    {
        let searchController = UISearchController(searchResultsController: nil)
        searchController.searchBar.delegate = self
        self.present(searchController, animated: true, completion: nil)
    }
  
    
    func locateWithLongitude(_ lon: Double, andLatitude lat: Double, andTitle title: String) {
           
           
           
           DispatchQueue.main.async { () -> Void in
               
               let position = CLLocationCoordinate2DMake(lat, lon)
               let marker = GMSMarker(position: position)
               
               let camera = GMSCameraPosition.camera(withLatitude: lat, longitude: lon, zoom: 10)
               self.GoogleMapsView.camera = camera
               
               marker.title = "Address : \(title)"
               marker.map = self.GoogleMapsView
               
           }
           
       }
      
       func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
           
     
          self.resultsArray.removeAll()
           gmsFetcher?.sourceTextHasChanged(searchText)
              print(searchText)
           
       }
    
}

