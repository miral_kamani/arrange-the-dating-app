//
//  SelectCityVC.swift
//  Arrange
//
//  Created by Miral Kamani on 24/02/20.
//  Copyright © 2020 Miral Kamani. All rights reserved.
//

import UIKit

class SelectCityVC: UIViewController {
    
    var arrayCountry = ["Alaska",
                        "Alabama",
                        "Arkansas",
                        "American Samoa",
                        "Arizona",
                        "California",
                        "Colorado",
                        "Connecticut",
                        "District of Columbia",
                        "Delaware",
                        "Florida",
                        "Georgia",
                        "Guam",
                        "Hawaii",
                        "Iowa",
                        "Idaho",
                        "Illinois",
                        "Indiana",
                        "Kansas",
                        "Kentucky",
                        "Louisiana",
                        "Massachusetts",
                        "Maryland",
                        "Maine",
                        "Michigan",
                        "Minnesota",
                        "Missouri",
                        "Mississippi",
                        "Montana",
                        "North Carolina",
                        " North Dakota",
                        "Nebraska",
                        "New Hampshire",
                        "New Jersey",
                        "New Mexico",
                        "Nevada",
                        "New York",
                        "Ohio",
                        "Oklahoma",
                        "Oregon",
                        "Pennsylvania",
                        "Puerto Rico",
                        "Rhode Island",
                        "South Carolina",
                        "South Dakota",
                        "Tennessee",
                        "Texas",
                        "Utah",
                        "Virginia",
                        "Virgin Islands",
                        "Vermont",
                        "Washington",
                        "Wisconsin",
                        "West Virginia",
                        "Wyoming"]
    
    var FilterArrayCountry = [String]()
    var selectedIndex: Int?
    var isSearching = Bool()
    @IBOutlet var SearchBar: UISearchBar!
    @IBOutlet var tblReload: UITableView!
    override func viewDidLoad() {
        super.viewDidLoad()
        tblReload.tableFooterView = UIView()
        self.RegisterXIB()
        
    }
    @IBAction func backTapped(_ sender: UIButton)
    {
        self.navigationController?.popViewController(animated: true)
    }
    
    @objc func OnClickCellbtn(sender: UIButton)
    {
        selectedIndex = sender.tag
        tblReload.reloadData()
        
    }
}

extension SelectCityVC: UITableViewDataSource,UITableViewDelegate
{
    func RegisterXIB()
    {
        self.tblReload.register(UINib(nibName: "GenderCell", bundle: nil), forCellReuseIdentifier: "GenderCell")
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if isSearching
        {
            return FilterArrayCountry.count
        }
        else
        {
            return arrayCountry.count
        }
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        let cell = tableView.dequeueReusableCell(withIdentifier: "GenderCell", for: indexPath) as! GenderCell
        if isSearching
        {
            cell.labelOption.text = FilterArrayCountry[indexPath.row]
        }
        else
        {
            cell.labelOption.text = arrayCountry[indexPath.row]
        }
        if selectedIndex == indexPath.row
        {
            cell.imageTick.image = UIImage(named: "check")
            selectedCity = cell.labelOption.text!
            cell.imageTick.isHidden = false
            let navigate = self.storyboard?.instantiateViewController(withIdentifier: "LocationVC") as! LocationVC
            navigate.Selection = 1
            self.navigationController?.pushViewController(navigate, animated: true)
        }
        else
        {
            cell.imageTick.isHidden = true
        }
        cell.btnClick.tag = indexPath.row
        cell.btnClick.addTarget(self, action: #selector(OnClickCellbtn), for: .touchUpInside)
        return cell
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 50.0
    }
    
}

extension SelectCityVC: UISearchBarDelegate
{
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String)
    {
        
        FilterArrayCountry = arrayCountry.filter({ name -> Bool in
            if searchText.isEmpty
            {
                return true
            }
            return (name.lowercased().prefix(searchText.count).contains(searchText.lowercased()))
        })
        self.isSearching = true
        
        self.tblReload.reloadData()
        
    }
    
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar)
    {
        self.isSearching = false
        self.SearchBar.text = ""
        self.tblReload.reloadData()
    }
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar)
    {
        isSearching = false
        self.SearchBar.endEditing(true)
    }
}

