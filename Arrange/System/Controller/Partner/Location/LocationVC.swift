//
//  LocationVC.swift
//  Arrange
//
//  Created by Miral Kamani on 18/02/20.
//  Copyright © 2020 Miral Kamani. All rights reserved.
//

import UIKit
import TagListView
import Toaster
class LocationVC: UIViewController,TagListViewDelegate {
    
    //MARK:- Outlets
    
    @IBOutlet var tagList: TagListView!
    
    @IBOutlet var CountryView: UIView!
    @IBOutlet var StateView: UIView!
    @IBOutlet var CityView: UIView!
    
    @IBOutlet var tblCountry: UITableView!
    @IBOutlet var tblState: UITableView!
    @IBOutlet var tblCity: UITableView!
    
    @IBOutlet var SearchCountry: UISearchBar!
    @IBOutlet var SearchState: UISearchBar!
    @IBOutlet var SearchCity: UISearchBar!
    
    @IBOutlet var btnBack: UIButton!
     //MARK:- Variable
    
    var tblNumber: Int?
    var arrayAddress = [String]()
    
    var isSearchCountry = Bool()
    var isSearchState = Bool()
    var isSearchCity = Bool()
    
    var filteredCountry = [ModelCountryResult]()
    var filteredState = [ModelStateResult]()
    var filteredCities = [ModelCityResult]()
    var filteredCity = [String]()
    var currentIndex = Int()
    var tag = String()
    var SelectesCountry = String()
    var SelectedState = String()
    var SelectedCity = String()
    var isStateDelete = Bool()
    var isRemoveAllTag = Bool()
    var arrayCountry = [ModelCountryResult]()
    var arrayState = [ModelStateResult]()
    var arrayCity = [ModelCityResult]()
    var tmpCountryID = Int()
    var tmpStateID = Int()
    var selectedCityID = Int()
    
    static var FilterCountry = String()
    static var FilterState = String()
    static var FilterCity = String()
    
    override func viewDidLoad() {
        
        super.viewDidLoad()
        self.setUptagList()
        self.RegisterXIB()
        self.btnBack.addTarget(self, action: #selector(OnClickBack), for: .touchUpInside)
        self.CountryView.isHidden = false
        self.StateView.isHidden = true
        self.CityView.isHidden = true
    }
    
    override func viewWillAppear(_ animated: Bool) {
        constant = ""
        self.WS_Get_Country()
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    func setUptagList()
    {
        tagList.delegate = self
        tagList.textFont = UIFont(name: "Avenir-Heavy", size: 12)!
        tagList.shadowRadius = 2
        tagList.shadowOpacity = 0.4
        tagList.shadowColor = UIColor.white
        tagList.shadowOffset = CGSize(width: 1, height: 1)
        tagList.alignment = .center
    }
    
    @objc func OnClickBack(sender: UIButton)
    {
        self.navigationController?.popViewController(animated: true)
    }
    
    func tagPressed(_ title: String, tagView: TagView, sender: TagListView)
    {
        print("Tag pressed: \(title), \(sender)")
    }
    
    func tagRemoveButtonPressed(_ title: String, tagView: TagView, sender: TagListView) {
        
        print(title)
        
        if title == self.SelectesCountry{
            tagList.removeAllTags()
            
            self.SelectesCountry = ""
            LocationVC.FilterCountry = ""
            self.tmpCountryID = 0
            
            self.SelectedState = ""
            LocationVC.FilterState = ""
            self.tmpStateID = 0
            
            self.SelectedCity = ""
            LocationVC.FilterCity = ""
            self.selectedCityID = 0
            
            self.CountryView.isHidden = false
            self.StateView.isHidden = true
            self.CityView.isHidden = true
        }else if title == self.SelectedState{
            
            tagList.removeTag(title)
            tagList.removeTag(self.SelectedCity)
            
            self.SelectedState = ""
            LocationVC.FilterState = ""
            self.tmpStateID = 0
            
            self.SelectedCity = ""
            LocationVC.FilterCity = ""
            self.selectedCityID = 0
            
            self.CountryView.isHidden = true
            self.StateView.isHidden = false
            self.CityView.isHidden = true
        }else if title == self.SelectedCity{
            
            tagList.removeTag(title)
            
            self.SelectedCity = ""
            LocationVC.FilterCity = ""
            self.selectedCityID = 0
            
            self.CountryView.isHidden = true
            self.StateView.isHidden = true
            self.CityView.isHidden = false
        }
        
    }
}

extension LocationVC: UITableViewDelegate,UITableViewDataSource
{
    func RegisterXIB()
    {
        self.tblCountry.register(UINib(nibName: "LocationCell", bundle: nil), forCellReuseIdentifier: "LocationCell")
        self.tblState.register(UINib(nibName: "LocationCell", bundle: nil), forCellReuseIdentifier: "LocationCell")
        self.tblCity.register(UINib(nibName: "LocationCell", bundle: nil), forCellReuseIdentifier: "LocationCell")
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if tableView == tblCountry
        {
            if isSearchCountry
            {
                return filteredCountry.count
            }
            else
            {
                return arrayCountry.count
            }
        }
        else if tableView == tblState
        {
            if isSearchState
            {
                return filteredState.count
            }
            else
            {
                return arrayState.count
            }
        }
        else
        {
            if isSearchCity
            {
                return filteredCity.count
            }
            else
            {
                return arrayCity.count
            }
        }
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "LocationCell", for: indexPath) as! LocationCell
        if tableView == tblCountry
        {
            var dict: ModelCountryResult?
            
            if !isSearchCountry{
                dict = arrayCountry[indexPath.row]
            }else{
                dict = filteredCountry[indexPath.row]
            }
            
            if let name = dict?.name, name.count != 0{
                cell.labelOption.text = name
            }else{
                cell.labelOption.text = ""
            }
            
            if self.tmpCountryID == dict!.countriesId
            {
                cell.imageTick.image = UIImage(named: "check")
            }
            else
            {
                cell.imageTick.isHidden = true
            }
            
            return cell
        }
        else if tableView == tblState
        {
            var dict: ModelStateResult?
            
            if !isSearchState{
                dict = arrayState[indexPath.row]
            }else{
                dict = filteredState[indexPath.row]
            }
            
            if let name = dict?.name, name.count != 0{
                cell.labelOption.text = name
            }else{
                cell.labelOption.text = ""
            }
            
            if self.tmpStateID == dict!.statesId
            {
                cell.imageTick.image = UIImage(named: "check")
            }
            else
            {
                cell.imageTick.isHidden = true
            }
            
            return cell
        }
        else
        {
            if arrayCity.count != 0
            {
                var dict: ModelCityResult?
                
                if !isSearchState{
                    dict = arrayCity[indexPath.row]
                }else{
                    dict = filteredCities[indexPath.row]
                }
                
                if let name = dict?.name, name.count != 0{
                    cell.labelOption.text = name
                }else{
                    cell.labelOption.text = ""
                }
                
                if self.selectedCityID == dict!.citiesId
                {
                    cell.imageTick.image = UIImage(named: "check")
                }
                else
                {
                    cell.imageTick.isHidden = true
                }
            }
            else
            {
                
            }
            return cell
        }
        
        
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        if tableView == tblCountry
        {
            var dict: ModelCountryResult?
            
            if !isSearchCountry{
                dict = arrayCountry[indexPath.row]
            }else{
                dict = filteredCountry[indexPath.row]
            }
            
            if let data = dict{
                if let id = data.countriesId{
                    self.tmpCountryID = id
                }
                
                if let name = data.name, name.count != 0{
                    LocationVC.FilterCountry = name
                    self.SelectesCountry = name
                }
            }
            
            self.WS_Get_State()
            
            tagList.addTag(self.SelectesCountry).tagBackgroundColor = UIColor.blue
            self.CountryView.isHidden = true
            self.StateView.isHidden = false
            self.CityView.isHidden = true
            self.tblCountry.reloadData()
            
        }
        else if tableView == tblState
        {
            var dict: ModelStateResult?
            
            if !isSearchState{
                dict = arrayState[indexPath.row]
            }else{
                dict = filteredState[indexPath.row]
            }
            
            if let data = dict{
                if let id = data.statesId{
                    self.tmpStateID = id
                }
                
                if let name = data.name, name.count != 0{
                    LocationVC.FilterState = name
                    self.SelectedState = name
                }
            }
            
            self.WS_Get_City()
            tagList.addTag(self.SelectedState).tagBackgroundColor = UIColor.systemGreen
            self.CountryView.isHidden = true
            self.StateView.isHidden = true
            self.CityView.isHidden = false
            self.tblState.reloadData()
        }
        else if tableView == tblCity
        {
            
            var dict: ModelCityResult?
            
            if !isSearchState{
                dict = arrayCity[indexPath.row]
            }else{
                dict = filteredCities[indexPath.row]
            }
            
            if let data = dict{
                if let id = data.citiesId{
                    self.selectedCityID = id
                }
                
                if let name = data.name, name.count != 0{
                    LocationVC.FilterCity = name
                    self.SelectedCity = name
                }
            }
            
            tagList.addTag(self.SelectedCity).tagBackgroundColor = UIColor.systemPink
            self.CountryView.isHidden = false
            self.StateView.isHidden = true
            self.CityView.isHidden = true
            self.tblCity.reloadData()
        }
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 50.0
    }
}
extension LocationVC: UISearchBarDelegate
{
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String)
    {
        if searchBar == SearchCountry
        {
            let tempArr = self.arrayCountry.filter({ names -> Bool in
                if searchText.isEmpty
                {
                    return true
                }
                return (names.name?.lowercased().prefix(searchText.count).contains(searchText.lowercased()))!
            })
            
            if tempArr.count != 0{
                self.isSearchCountry = true
                filteredCountry = tempArr
            }else{
                self.isSearchCountry = false
            }
            
            self.tblCountry.reloadData()
        }
            
        else if searchBar == SearchState
        {
            let tempArr = self.arrayState.filter({ names -> Bool in
                if searchText.isEmpty
                {
                    return true
                }
                return (names.name?.lowercased().prefix(searchText.count).contains(searchText.lowercased()))!
            })
            
            if tempArr.count != 0{
                self.isSearchState = true
                filteredState = tempArr
            }else{
                self.isSearchState = false
            }
            self.tblState.reloadData()
        }
        else if searchBar == SearchCity
        {
            let tempArr = self.arrayCity.filter({ names -> Bool in
                if searchText.isEmpty
                {
                    return true
                }
                return (names.name?.lowercased().prefix(searchText.count).contains(searchText.lowercased()))!
            })
            
            if tempArr.count != 0{
                self.isSearchCity = true
                filteredCities = tempArr
            }else{
                self.isSearchCity = false
            }
            
            self.tblCity.reloadData()
        }
    }
    
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar)
    {
        self.isSearchCountry = false
        self.isSearchState = false
        self.isSearchCity = false
        self.SearchCountry.text = ""
        self.SearchState.text = ""
        self.SearchCity.text = ""
        self.tblCountry.reloadData()
        self.tblState.reloadData()
        self.tblCity.reloadData()
    }
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar)
    {
        self.isSearchCountry = false
        self.isSearchState = false
        self.isSearchCity = false
        self.SearchCountry.endEditing(true)
        self.SearchState.endEditing(true)
        self.SearchCity.endEditing(true)
    }
}

extension LocationVC
{
    func WS_Get_Country()
    {
        APIClient<ModelCountryBase>().API_GET(Url: ws_Get_Country, Params: [:], Spinner: 1, Authentication: true, Progress: true, Alert: true, Offline: false, SuperVC: self, completionSuccess: { (modelResponse) in
            
            print("---------------------")
            print("Result:",modelResponse.toJSONString(prettyPrint: true)!)
            print("---------------------")
            
            self.arrayCountry.removeAll()
            if(modelResponse.success == true)
            {
                if let result = modelResponse.countryResult, result.count != 0
                {
                    for data in result
                    {
                        self.arrayCountry.append(data)
                    }
                }
            }
            else
            {
                 if modelResponse.message == "Authentication API is invalid!" {
                     let vc = self.storyboard?.instantiateViewController(withIdentifier: "LogoutVc") as! LogoutVc
                     self.navigationController?.pushViewController(vc, animated: false)
                 }
                 else {
                     UtilityClass.hidehud()
                     Toast(text: modelResponse.message ?? "").show()
                 }
            }
            self.tblCountry.reloadData()
        }) { (failed) in
            
        }
    }
    
    func WS_Get_State()
    {
        APIClient<ModelStateBase>().API_GET(Url: ws_Get_State+"\(tmpCountryID)", Params: [:], Spinner: 1, Authentication: true, Progress: true, Alert: true, Offline: false, SuperVC: self, completionSuccess: { (modelResponse) in
            
            print("---------------------")
            print("Result:",modelResponse.toJSONString(prettyPrint: true)!)
            print("---------------------")
            
            self.arrayState.removeAll()
           
            if(modelResponse.success == true)
            {
                if let result = modelResponse.stateResult, result.count != 0
                {
                    for data in result
                    {
                        self.arrayState.append(data)
                        self.filteredState = self.arrayState
                       
                    }
                }
            }
            else
            {
                if modelResponse.message == "Authentication API is invalid!" {
                     let vc = self.storyboard?.instantiateViewController(withIdentifier: "LogoutVc") as! LogoutVc
                     self.navigationController?.pushViewController(vc, animated: false)
                 }
                 else {
                     UtilityClass.hidehud()
                     Toast(text: modelResponse.message ?? "").show()
                 }
            }
            self.tblState.reloadData()
        }) { (failed) in
            
        }
    }
    
    func WS_Get_City()
    {
        APIClient<ModelCityBase>().API_GET(Url: ws_Get_City+"\(tmpStateID)", Params: [:], Spinner: 1, Authentication: true, Progress: true, Alert: true, Offline: false, SuperVC: self, completionSuccess: { (modelResponse) in
            
            print("---------------------")
            print("Result:",modelResponse.toJSONString(prettyPrint: true)!)
            print("---------------------")
            
            self.arrayCity.removeAll()
            
            if(modelResponse.success == true)
            {
                if let result = modelResponse.cityResult, result.count != 0
                {
                    for data in result
                    {
                        self.arrayCity.append(data)
                        self.filteredCities = self.arrayCity
                    }
                }
            }
            else
            {
                 if modelResponse.message == "Authentication API is invalid!" {
                     let vc = self.storyboard?.instantiateViewController(withIdentifier: "LogoutVc") as! LogoutVc
                     self.navigationController?.pushViewController(vc, animated: false)
                 }
                 else {
                     UtilityClass.hidehud()
                     Toast(text: modelResponse.message ?? "").show()
                 }
            }
            self.tblCity.reloadData()
        }) { (failed) in
            
        }
    }
}
