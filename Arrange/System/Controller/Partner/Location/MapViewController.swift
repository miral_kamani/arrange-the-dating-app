//
//  MapViewController.swift
//  Arrange
//
//  Created by Miral Kamani on 19/02/20.
//  Copyright © 2020 Miral Kamani. All rights reserved.
//

import UIKit
import GoogleMaps
import GooglePlaces
import ObjectMapper
import Alamofire

class MapViewController: UIViewController, UIGestureRecognizerDelegate, UITextFieldDelegate, UITableViewDataSource, UITableViewDelegate {
    
    @IBOutlet weak var statusbar: UIView!
    @IBOutlet weak var headerview: UIView!
    @IBOutlet weak var map_view: GMSMapView!
    @IBOutlet weak var view_search: UIView!
    @IBOutlet weak var txt_search: UITextField!
    
    @IBOutlet weak var location_view: UIView!
    @IBOutlet weak var tableview_location: UITableView!
    
    var locationManager = CLLocationManager()
    var currentLocation: CLLocation?
    var placesClient: GMSPlacesClient!
    var zoomLevel: Float = 10.0
    
    var Currenctlattitude = Double()
    var Currenctlongitude = Double()
    
    var address = String()
    var AddressDataArr = [String]()
    var ResultArr = [ModelGoogleAddressresults]()
    
    static var selectedAddress = String()
    static var selectedAddressLat = Double()
    static var selectedAddressLng = Double()
    
    var isNavigateEditFavvorScreen = Bool()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.navigationController?.interactivePopGestureRecognizer?.delegate = self
        self.navigationController?.interactivePopGestureRecognizer?.isEnabled = true
        
        self.location_view.isHidden = true
        
        locationManager = CLLocationManager()
        locationManager.desiredAccuracy = kCLLocationAccuracyBest
        locationManager.requestWhenInUseAuthorization()
        locationManager.distanceFilter = 50
        locationManager.startUpdatingLocation()
        locationManager.delegate = self
        
        tableview_location.register(UINib(nibName: "MapCell", bundle: nil), forCellReuseIdentifier: "MapCell")
        
        self.addSearchObserver()
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle
    {
        return .default
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        self.removeSearchObserver()
    }
    
    // MARK:- custom function
    
    func mapReload() {
        
        self.map_view.clear()
        let camera = GMSCameraPosition.camera(withLatitude: self.Currenctlattitude, longitude: self.Currenctlongitude, zoom: zoomLevel, bearing: 0, viewingAngle: 0)
        self.map_view.camera = camera
        
        if self.Currenctlattitude != 0.0 && self.Currenctlongitude != 0.0
        {
            let position = CLLocationCoordinate2D(latitude: CLLocationDegrees(self.Currenctlattitude), longitude: CLLocationDegrees(self.Currenctlongitude))
            let marker = GMSMarker(position: position)
            marker.map = self.map_view
        }
    }
    
    // MARK:- UITextField Method
    
    func addSearchObserver() {
        NotificationCenter.default.addObserver(self, selector: #selector(self.textChange(_:)), name: UITextField.textDidChangeNotification, object: nil)
    }
    
    func removeSearchObserver() {
        self.txt_search.resignFirstResponder()
        
        NotificationCenter.default.removeObserver(self, name: UITextField.textDidChangeNotification, object: nil)
        // Do any additional setup after loading the view.
    }
    
    @objc func textChange(_ notification: Notification) {
        searchRecordsFirstName(str: self.txt_search.text!)
    }
    
    func searchRecordsFirstName(str: String) {
        self.address = str
        
        let char_length = str.count % 3;
        
        if (str.count > 2) {
            self.location_view.isHidden = false
        }
        else
        {
            self.location_view.isHidden = true
        }
        
        if (char_length == 0) {
           self.get_address_data(address: self.address)
        }
    }
    
    // MARK:- Tableview Method
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return AddressDataArr.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "MapCell", for: indexPath) as! MapCell
        cell.labelAddress.text = self.AddressDataArr[indexPath.row]
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let dict = self.AddressDataArr[indexPath.row]
        self.txt_search.text = dict
        
      self.Api_GoogleLatAndLngForAddress(fullAddress: dict)
    }
    
//    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
//        return IS_IPAD() ? 60 : 44
//    }
//
//    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
//        return IS_IPAD() ? 60 : 44
//    }
    
    @IBAction func btnBack(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    
    func getAddressFromLatLon(pdblLatitude: String, withLongitude pdblLongitude: String) {
        var center : CLLocationCoordinate2D = CLLocationCoordinate2D()
        let lat: Double = Double("\(pdblLatitude)")!
        //21.228124
        let lon: Double = Double("\(pdblLongitude)")!
        //72.833770
        let ceo: CLGeocoder = CLGeocoder()
        center.latitude = lat
        center.longitude = lon
        
        let loc: CLLocation = CLLocation(latitude:center.latitude, longitude: center.longitude)
        
        
        ceo.reverseGeocodeLocation(loc, completionHandler:
            {(placemarks, error) in
                if (error != nil)
                {
                    print("reverse geodcode fail: \(error!.localizedDescription)")
                }
                let pm = placemarks!
                
                if pm.count > 0 {
                    let pm = placemarks![0]
                    
                    var addressString : String = ""
                    if pm.subLocality != nil {
                        addressString = addressString + pm.subLocality! + ", "
                    }
                    if pm.thoroughfare != nil {
                        addressString = addressString + pm.thoroughfare! + ", "
                    }
                    if pm.locality != nil {
                        addressString = addressString + pm.locality! + ", "
                    }
                    if pm.country != nil {
                        
                        addressString = addressString + pm.country! + ", "
                        
                    }
                    if pm.postalCode != nil {
                        addressString = addressString + pm.postalCode! + " "
                    }
                    if pm.name != nil {
                        addressString = addressString + pm.name! + " "
                    }
                    if pm.isoCountryCode != nil {
                        addressString = addressString + pm.isoCountryCode! + " "
                    }
                    if pm.administrativeArea != nil {
                        addressString = addressString + pm.administrativeArea! + " "
                    }
                    if pm.thoroughfare != nil {
                        addressString = addressString + pm.thoroughfare! + " "
                    }
                    
//                    if self.isNavigateEditFavvorScreen{
//
//                        self.isNavigateEditFavvorScreen = false
//
//                        if let perData = editFavvorVC.favvorEditData.performLocation{
//
//                            if pm.country != "United States" {
//                                self.view.makeToast("Please select only united states location")
//                            }else{
//
//                                perData.address = addressString
//                                perData.city = pm.locality ?? ""
//                                perData.country = pm.country ?? "US"
//                                perData.countryCode = pm.isoCountryCode ?? ""
//                                perData.latitude = "\(self.Currenctlattitude)"
//                                perData.longitude = "\(self.Currenctlongitude)"
//                                perData.name = pm.name ?? ""
//                                perData.postalCode = pm.postalCode ?? ""
//                                perData.state = pm.administrativeArea ?? ""
//                                perData.stateCode = pm.administrativeArea ?? ""
//                                perData.streetAddress = pm.thoroughfare ?? ""
//                                   self.navigationController?.popViewController(animated: true)
//                            }
//                        }
//
//
//                    }else{
//
//                        if pm.country != "United States" {
//                            self.view.makeToast("Please select only united states location")
//                        }else{
//                            FavvorDetailVC.address = addressString
//                            FavvorDetailVC.city = pm.locality ?? ""
//                            FavvorDetailVC.country = pm.country ?? ""
//                            FavvorDetailVC.country_code = pm.isoCountryCode ?? ""
//                            FavvorDetailVC.latitude = "\(self.Currenctlattitude)"
//                            FavvorDetailVC.longitude = "\(self.Currenctlongitude)"
//                            FavvorDetailVC.name = pm.name ?? ""
//                            FavvorDetailVC.postalcode =  pm.postalCode ?? ""
//                            FavvorDetailVC.state = pm.administrativeArea ?? ""
//                            FavvorDetailVC.statecode = pm.administrativeArea ?? ""
//                            FavvorDetailVC.streetaddress = pm.thoroughfare ?? ""
//                            CustomFavvorVC.strLocation = addressString
//                            print(addressString)
//                             self.navigationController?.popViewController(animated: true)
//                        }
//
//                    }
                }
        })
    }
    
    enum States: String, CaseIterable {
        case alaska             = "AK"
        case alabama            = "AL"
        case arkansas           = "AR"
        case americanSamoa      = "AS"
        case arizona            = "AZ"
        case california         = "CA"
        case colorado           = "CO"
        case connecticut        = "CT"
        case districtOfColumbia = "DC"
        case delaware           = "DE"
        case florida            = "FL"
        case georgia            = "GA"
        case guam               = "GU"
        case hawaii             = "HI"
        case iowa               = "IA"
        case idaho              = "ID"
        case illinois           = "IL"
        case indiana            = "IN"
        case kansas             = "KS"
        case kentucky           = "KY"
        case louisiana          = "LA"
        case massachusetts      = "MA"
        case maryland           = "MD"
        case maine              = "ME"
        case michigan           = "MI"
        case minnesota          = "MN"
        case missouri           = "MO"
        case mississippi        = "MS"
        case montana            = "MT"
        case northCarolina      = "NC"
        case northDakota        = "ND"
        case nebraska           = "NE"
        case newHampshire       = "NH"
        case newJersey          = "NJ"
        case newMexico          = "NM"
        case nevada             = "NV"
        case newYork            = "NY"
        case ohio               = "OH"
        case oklahoma           = "OK"
        case oregon             = "OR"
        case pennsylvania       = "PA"
        case ruertoRico         = "PR"
        case rhodeIsland        = "RI"
        case southCarolina      = "SC"
        case southDakota        = "SD"
        case tennessee          = "TN"
        case texas              = "TX"
        case utah               = "UT"
        case virginia           = "VA"
        case virginIslands      = "VI"
        case vermont            = "VT"
        case washington         = "WA"
        case wisconsin          = "WI"
        case westVirginia       = "WV"
        case wyoming            = "WY"
    }
    
    @IBAction func click_selectLocation(_ sender: UIButton) {
        MapViewController.selectedAddress = self.txt_search.text!
        MapViewController.selectedAddressLat = self.Currenctlattitude
        MapViewController.selectedAddressLng = self.Currenctlongitude
        getAddressFromLatLon(pdblLatitude: "\(self.Currenctlattitude)", withLongitude: "\(self.Currenctlongitude)")
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func click_back(_ sender: UIButton) {
        self.isNavigateEditFavvorScreen = false
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func click_search(_ sender: UIButton) {
        self.view_search.isHidden = false
    }
    
    @IBAction func click_searchBack(_ sender: UIButton) {
        self.view_search.isHidden = false
    }

    // MARK:- Data Parsing

    func get_address_data(address: String)
    {

        let originalString = address
        let escapedString = originalString.addingPercentEncoding(withAllowedCharacters: .urlHostAllowed)

        let url = "https://maps.googleapis.com/maps/api/place/autocomplete/json?input="+escapedString!+"&types=geocode&key=AIzaSyBZR53LeDpCYf2YjpG9fPkuyMmuJl6vsh4"

        Alamofire.request(url, method: .get, parameters: ["":""], encoding: URLEncoding.default, headers: nil).responseJSON
            { response in
                //            debugPrint(response)
                guard
                    let responseJSON = response.result.value

                    else {
                        print(response.error!)
                        return
                }
                print(responseJSON)

                if response.result.value != nil
                {
                    let resultModel = Mapper<ModelBaseGetAddressData>().map(JSONObject: responseJSON)

                    if(resultModel?.status == "OK")
                    {
                        self.tableview_location.isHidden = false
                        let resul_array_tmp_new = resultModel!.predictions! as NSArray
                        if resul_array_tmp_new.count > 0
                        {
                            self.AddressDataArr.removeAll()
                            for employee in (resultModel?.predictions)!
                            {
                                self.AddressDataArr.append(employee.descriptionValue!)
                            }
                        }
                    }
                    self.tableview_location.reloadData()
                }
                else {
                    print(response.error!)
                    self.tableview_location.reloadData()
                }
        }
    }

    func Api_GoogleLatAndLngForAddress(fullAddress: String) {

        self.location_view.isHidden = true
        let params = ["address":fullAddress,"key": "AIzaSyBZR53LeDpCYf2YjpG9fPkuyMmuJl6vsh4"]

        Alamofire.request("https://maps.googleapis.com/maps/api/geocode/json", method: .get, parameters: params, encoding: URLEncoding.default, headers: nil).responseJSON
            { response in
                //            debugPrint(response)
                guard
                    let responseJSON = response.result.value

                    else {
                        print(response.error!)
                        return
                }
                print(responseJSON)

                if response.result.value != nil
                {
                    print(response)
                    let resultModel = Mapper<ModelBaseGoogleAddress>().map(JSONObject: response.result.value)
                    if(resultModel?.status == "OK") {
                        let ModelResultArr = resultModel!.googleAddressresults! as NSArray
                        if ModelResultArr.count > 0  {
                            self.ResultArr.removeAll()
                            for result in (resultModel?.googleAddressresults)!  {
                                self.ResultArr.append(result)
                            }

                            for i in 0..<self.ResultArr.count {
                                self.Currenctlattitude = Double((self.ResultArr[i].geometry?.location?.lat)!)
                                self.Currenctlongitude = Double((self.ResultArr[i].geometry?.location?.lng)!)
                            }
                            self.tableview_location.reloadData()
                            self.mapReload()
                        }else{

                        }
                    }
                }
                else
                {
                    print(response.error!)
                    self.tableview_location.reloadData()
                }
        }
    }
}

extension MapViewController: CLLocationManagerDelegate {
    
    // Handle incoming location events.
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        let location: CLLocation = locations.last!
        print("Location: \(location)")
        
        let camera = GMSCameraPosition.camera(withLatitude: location.coordinate.latitude,
                                              longitude: location.coordinate.longitude,
                                              zoom: zoomLevel)
        map_view.camera = camera
        self.map_view.clear()
        
        self.Currenctlattitude = location.coordinate.latitude
        self.Currenctlongitude = location.coordinate.longitude
        
        let marker = GMSMarker()
        marker.position = CLLocationCoordinate2DMake(location.coordinate.latitude, location.coordinate.longitude)
        marker.map = map_view
        
        self.locationManager.stopUpdatingLocation()
        
    }
    
    
    // Handle authorization for the location manager.
    func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus) {
        switch status {
        case .restricted:
            print("Location access was restricted.")
        case .denied:
            print("User denied access to location.")
            // Display the map using the default location.
            map_view.isHidden = false
        case .notDetermined:
            print("Location status not determined.")
        case .authorizedAlways: fallthrough
        case .authorizedWhenInUse:
            print("Location status is OK.")
        default:
            break
        }
    }
    
    // Handle location manager errors.
    func locationManager(_ manager: CLLocationManager, didFailWithError error: Error) {
        locationManager.stopUpdatingLocation()
        print("Error: \(error)")
    }
}

extension MapViewController: GMSMapViewDelegate {
    
    func mapView(_ mapView: GMSMapView, didTapAt coordinate: CLLocationCoordinate2D) {
        print("Tapped at coordinate: " + String(coordinate.latitude) + " "
            + String(coordinate.longitude))
        self.map_view.clear()
        let marker = GMSMarker()
        marker.position = CLLocationCoordinate2DMake(coordinate.latitude, coordinate.longitude)
        marker.map = map_view
        
        self.Currenctlattitude = coordinate.latitude
        self.Currenctlongitude = coordinate.longitude
    }
}
