//
//  LocationWorks.swift
//  Arrange
//
//  Created by Miral Kamani on 18/02/20.
//  Copyright © 2020 Miral Kamani. All rights reserved.
//

import UIKit

class LocationWorks: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    @IBAction func btnGotIt(_ sender: UIButton)
    {
        self.navigationController?.popViewController(animated: true)
    }
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
}
