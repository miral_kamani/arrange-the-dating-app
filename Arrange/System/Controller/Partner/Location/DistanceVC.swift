//
//  DistanceVC.swift
//  Arrange
//
//  Created by Miral Kamani on 24/02/20.
//  Copyright © 2020 Miral Kamani. All rights reserved.
//

import UIKit

class DistanceVC: UIViewController {
     
        var arrayDistance = ["100km","200km","300km","400km","500km","600km","700km","800km"]
        var selectedIndex: Int?
        @IBOutlet var tblReload: UITableView!
        override func viewDidLoad() {
            super.viewDidLoad()
            tblReload.tableFooterView = UIView()
            self.RegisterXIB()
            
        }
        @IBAction func backTapped(_ sender: UIButton)
        {
            self.navigationController?.popViewController(animated: true)
        }
        
        @objc func OnClickCellbtn(sender: UIButton)
        {
            selectedIndex = sender.tag
            tblReload.reloadData()
            
        }
    }

    extension DistanceVC: UITableViewDataSource,UITableViewDelegate
    {
        
        
        func RegisterXIB()
        {
            self.tblReload.register(UINib(nibName: "GenderCell", bundle: nil), forCellReuseIdentifier: "GenderCell")
        }
        func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
            return arrayDistance.count
        }
        
        func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
        {
            let cell = tableView.dequeueReusableCell(withIdentifier: "GenderCell", for: indexPath) as! GenderCell
            cell.labelOption.text = arrayDistance[indexPath.row]
            if selectedIndex == indexPath.row
            {
                cell.imageTick.image = UIImage(named: "check")
                Distance = cell.labelOption.text!
                cell.imageTick.isHidden = false
            }
            else
            {
                cell.imageTick.isHidden = true
            }
            cell.btnClick.tag = indexPath.row
            cell.btnClick.addTarget(self, action: #selector(OnClickCellbtn), for: .touchUpInside)
            return cell
        }
        func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
            return 50.0
        }
        
    }
