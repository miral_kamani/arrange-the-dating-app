//
//  UserAgeVC.swift
//  Arrange
//
//  Created by Miral Kamani on 14/02/20.
//  Copyright © 2020 Miral Kamani. All rights reserved.
//

import UIKit
import Toaster
class UserAgeVC: UIViewController,UIGestureRecognizerDelegate {
    
    //MARK:- Outlets
    
    @IBOutlet var txtDate: UnderLineImageTextField!
    @IBOutlet var txtMonth: UnderLineImageTextField!
    @IBOutlet var txtYear: UnderLineImageTextField!
    @IBOutlet var PageControl: UIPageControl!
     //MARK:- Variable
    
    static var Age = Int()
    static var BirthDate = String()
    var isUpdate = Bool()
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.interactivePopGestureRecognizer?.delegate = self
        self.navigationController?.interactivePopGestureRecognizer?.isEnabled = true
    }
    
    @IBAction func btnTapped(_ sender: UIButton)
    {
        self.navigationController?.popViewController(animated: true)
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    @IBAction func OnClickNext(_ sender: UIButton)
    {
        let year = Int(txtYear.text!)
        let month = Int(txtMonth.text!)
        let date = Int(txtDate.text!)
        let dob = Calendar.current.date(from: DateComponents(year: year, month: month, day: date))
        let age = dob?.age
        UserAgeVC.Age = age!
        UserAgeVC.BirthDate = "\(date ?? 0)"+"/"+"\(month ?? 0)"+"/"+"\(year ?? 0)"
        if isUpdate == true
        {
            let navigate = self.storyboard?.instantiateViewController(withIdentifier: "UserHeightVC") as! UserHeightVC
            navigate.isUpdate = true
            self.navigationController?.pushViewController(navigate, animated: true)
        }
        else
        {
            self.navigationController?.popViewController(animated: true)
        }
    }
    
    func Validation(textField : UITextField)
    {
        if txtDate.text != "" && txtMonth.text != "" && txtYear.text != ""
        {
            if textField == txtDate
            {
                let text = Int(textField.text!)
                if text! < 1 || text! > 31
                {
                    textField.text = ""
                    Toast(text: "Please enter valid date").show()
                }
            }
            
            if textField == txtMonth
            {
                let text = Int(textField.text!)
                if text! < 1 || text! > 12
                {
                    textField.text = ""
                    Toast(text: "Please enter valid Month").show()
                }
                else
                {
                    if text == 01 || text == 03 || text == 05 || text == 07 || text == 08 || text == 10 || text == 12
                    {
                        let date = Int(txtDate.text!)
                        if date! > 31
                        {
                            textField.text = ""
                            Toast(text: "Please enter valid date").show()
                        }
                    }
                    else if text == 02
                    {
                        let year = Int(txtYear.text!)
                        let date = Int(txtDate.text!)
                        if year! / 4 == 0
                        {
                            if date! > 29
                            {
                                textField.text = ""
                                Toast(text: "Please enter valid date").show()
                            }
                        }
                        else
                        {
                            if date! > 28
                            {
                                textField.text = ""
                                Toast(text: "Please enter valid date").show()
                            }
                        }
                    }
                    else
                    {
                        let date = Int(txtDate.text!)
                        if date! > 30
                        {
                            textField.text = ""
                            Toast(text: "Please enter valid date").show()
                        }
                    }
                }
            }
            else if textField == txtYear
            {
                let text = Int(textField.text!)
                if text! > 2002 || text! < 1955
                {
                    textField.text = ""
                    Toast(text: "Please enter valid Year").show()
                }
            }
            else
            {
                
            }
        }
        else
        {
            if txtDate.text?.count == 0
            {
                Toast(text: "Please enter Date").show()
            }
            else if txtMonth.text?.count == 0
            {
                Toast(text: "Please enter Month").show()
            }
            else
            {
                Toast(text: "Please enter year").show()
            }
            
        }
    }
    
}

extension UserAgeVC : UITextFieldDelegate
{
    func textFieldDidBeginEditing(_ textField: UITextField) {
        
    }
    func textFieldDidEndEditing(_ textField: UITextField) {
    
    }
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        if textField.text == txtYear.text{
            if range.length > 0 {
                return true
            }
            if string == "" {
                return false
            }
            if range.location > 3{
                return false
            }
            let originalText = textField.text
            let replacementText = string.replacingOccurrences(of: " ", with: "")
            
            if !CharacterSet.decimalDigits.isSuperset(of: CharacterSet(charactersIn: replacementText)) {
                return false
            }
            if range.location == 2 {
                // originalText?.append(“ ")
                txtYear.text = originalText
            }
        }
            
        else if textField.text == txtMonth.text
        {
            if range.length > 0 {
                return true
            }
            if string == "" {
                return false
            }
            if range.location > 1 {
                return false
            }
            var originalText = textField.text
            let replacementText = string.replacingOccurrences(of: " ", with: "")
            
            if !CharacterSet.decimalDigits.isSuperset(of: CharacterSet(charactersIn: replacementText)) {
                return false
            }
            if range.location == 2 {
                //originalText?.append(“/")
                txtYear.text = originalText
            }
        }
            
        else
        {
            if range.length > 0 {
                return true
            }
            if string == "" {
                return false
            }
            if range.location > 1 {
                return false
            }
            var originalText = textField.text
            let replacementText = string.replacingOccurrences(of: " ", with: "")
            if !CharacterSet.decimalDigits.isSuperset(of: CharacterSet(charactersIn: replacementText)) {
                return false
            }
            if range.location == 2 {
                //originalText?.append(“/")
                txtYear.text = originalText
            }
        }
        return true
    }
}
