//
//  UserHeightVC.swift
//  Arrange
//
//  Created by Miral Kamani on 14/02/20.
//  Copyright © 2020 Miral Kamani. All rights reserved.
//

import UIKit

class UserHeightVC: UIViewController {
    
    //MARK:- Outlets
    @IBOutlet var CollectionReload: UICollectionView!
    
     //MARK:- Variable
    
    var arrayHeight = ["4'8","4'9","4'10","4'11","5'0","5'1","5'2","5'3","5'4","5'5","5'6","5'7","5'8","5'9","5'10","5'11","6'0","6'1","6'2","6'3","6'4","6'5","6'6","6'7","6'8"]
    static var Height = String()
    var isUpdate = Bool()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.RegisterXIB()
        
    }
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    @IBAction func backTapped(_ sender: UIButton)
    {
        self.navigationController?.popViewController(animated: true)
    }
    @objc func OnClickCellbtn(sender: UIButton)
    {
        UserHeightVC.Height = arrayHeight[sender.tag]
        DispatchQueue.main.asyncAfter(deadline: .now() + 1) {
            if self.isUpdate == true
            {
                let navigate = self.storyboard?.instantiateViewController(withIdentifier: "UserReligionVC") as! UserReligionVC
                navigate.isUpdate = true
                self.navigationController?.pushViewController(navigate, animated: true)
            }
            else
            {
                self.navigationController?.popViewController(animated: true)
            }
        }
        
        CollectionReload.reloadData()
        
    }
}

extension UserHeightVC: UICollectionViewDelegate,UICollectionViewDelegateFlowLayout,UICollectionViewDataSource
{
    func RegisterXIB()
    {
        self.CollectionReload.register(UINib(nibName: "UserSelectionCell", bundle: nil), forCellWithReuseIdentifier: "UserSelectionCell")
    }
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return arrayHeight.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "UserSelectionCell", for: indexPath) as! UserSelectionCell
        cell.labelTitleName.text = arrayHeight[indexPath.row]
        if UserHeightVC.Height == arrayHeight[indexPath.row]
        {
            cell.WholeView.layer.borderColor = UIColor(red: 140, green: 94, blue: 255).cgColor
            cell.WholeView.layer.borderWidth = 2.0
        }
        else
        {
            cell.WholeView.layer.borderColor = UIColor.clear.cgColor
        }
        cell.btnSelect.tag = indexPath.row
        cell.btnSelect.addTarget(self, action: #selector(OnClickCellbtn), for: .touchUpInside)
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView,layout collectionViewLayout: UICollectionViewLayout,sizeForItemAt indexPath: IndexPath) -> CGSize
    {
        return CGSize(width: CollectionReload.frame.size.width - 20, height: 75.0)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 10.0
    }
}
