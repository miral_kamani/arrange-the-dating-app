//
//  UserEducationVC.swift
//  Arrange
//
//  Created by Miral Kamani on 14/02/20.
//  Copyright © 2020 Miral Kamani. All rights reserved.
//

import UIKit

class UserEducationVC: UIViewController {

    //MARK:- Outlets
    @IBOutlet var txtUniversity: UnderLineImageTextField!
    @IBOutlet var btnDone: UIButton!
    @IBOutlet var PageControl: UIPageControl!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.btnDone.addTarget(self, action: #selector(OnClickDone), for: .touchUpInside)
        
    }
    override func viewWillAppear(_ animated: Bool) {
        Defaults[.education] = txtUniversity.text ?? ""
    }
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    @IBAction func backTapped(_ sender: UIButton)
    {
        self.navigationController?.popViewController(animated: true)
    }
    @objc func OnClickDone(sender: UIButton)
    {
        let navigate = self.storyboard?.instantiateViewController(withIdentifier: "UserCareerVC") as! UserCareerVC
        navigate.isUpdate = true
        self.navigationController?.pushViewController(navigate, animated: true)
    }
}

