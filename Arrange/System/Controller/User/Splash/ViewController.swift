//
//  ViewController.swift
//  Arrange
//
//  Created by Miral Kamani on 07/02/20.
//  Copyright © 2020 Miral Kamani. All rights reserved.
//

import UIKit
import FBSDKCoreKit
import FBSDKLoginKit
import Toaster

var loginManager = LoginManager()

class ViewController: UIViewController,UIGestureRecognizerDelegate {
    
    //MARK:- Outlets
    @IBOutlet var backGroundView: UIView!
    
     //MARK:- Variable
    var f_email = String()
    var f_name =  String()
    var f_imgURL = String()
    var f_lName = String()
    var f_id = String()
    var dict = [String:AnyObject]()
    static var isUpdate = Bool()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.interactivePopGestureRecognizer?.delegate = self
        self.navigationController?.interactivePopGestureRecognizer?.isEnabled = false
        
    }
    func convertIntoJSONString(arrayObject: [Any]) -> String? {
        
        do {
            let jsonData: Data = try JSONSerialization.data(withJSONObject: arrayObject, options: [])
            if  let jsonString = NSString(data: jsonData, encoding: String.Encoding.utf8.rawValue) {
                return jsonString as String
            }
            
        } catch let error as NSError {
            print("Array convertIntoJSON - \(error.description)")
        }
        return nil
    }
    override func viewWillAppear(_ animated: Bool) {
        
        super.viewWillAppear(animated)
    }
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    @IBAction func BtnFacebook(_ sender: UIButton)
    {
        self.view.endEditing(true)
        self.onClickFBBtn()
    }
    
    func onClickFBBtn() {
        loginManager = LoginManager()
        loginManager.logIn(permissions: ["public_profile", "email"], from: self) { (loginResult, error) in
            
            if (error == nil) {
                let fbloginresult : LoginManagerLoginResult = loginResult!
                 fbloginresult.grantedPermissions = ["public_profile", "email"]
                if(fbloginresult.grantedPermissions.contains("email")) {
                    self.getFBUserData()
                }
                else {
                    // Toast(text: "The user canceled the sign-in flow.").show()
                    
                }
            }
            else {
                //  Toast(text: error!.localizedDescription).show()
                UtilityClass.hidehud()
            }
        }
    }
    
    func getFBUserData(){
        if((AccessToken.current) != nil){
            
            GraphRequest(graphPath: "me", parameters: ["fields": "id, name, email, picture.type(large)"]).start(completionHandler: { (connection, result, error) -> Void in
                if (error == nil){
                    if let rdict = result
                    {
                        self.dict = (rdict as! [String : AnyObject])
                        if  let email = self.dict["email"] as? String, email.count != 0{
                            self.f_email = email
                        } else {
                            self.f_email = ""
                        }
                        
                        if  let name = self.dict["name"] as? String, name.count != 0{
                            self.f_name = name
                        } else {
                            self.f_name = ""
                        }
                        
                        guard let id = self.dict["id"] else {
                            //  self.view.makeToast("Not able to ferch ID, Please remove privacy to allow")
                            return
                        }
                        let userIDs = self.dict["id"] as! String
                        let userID = "https://graph.facebook.com/"+userIDs+"/picture?width=350&height=500&redirect=false"
                    
                        self.f_id = id as! String
                        self.f_imgURL = userID
                        self.WS_FacbookLogin()
                    }
                } else{
                    //  self.view.makeToast(error?.localizedDescription)
                    UtilityClass.hidehud()
                }
            })
        }
    }
}


extension ViewController
{
    func WS_FacbookLogin()
    {
        var parameters = [String: Any]()
      
        parameters = [
            "user_social_id": self.f_id,
            "user_name": self.f_name,
            "user_email": self.f_email,
            "user_device": kconstantDevice,
            "user_token": Defaults[.FCMToken],
            "user_login_type":"facebook",
            "user_image": f_imgURL,
            "user_age": "20",
            "user_gender": "Female"
            
        ]
        print(parameters)
        APIClient<ModelSocialLoginBase>().API_POST(Url: ws_Post_social_login, Params: parameters as [String:AnyObject], Authentication: true, Progress: true, Alert: true, Offline: false, SuperVC: self, completionSuccess: { (modelResponse) in
            UtilityClass.showhud()
            print("---------------------")
            print("Login Result:",modelResponse.toJSONString(prettyPrint: true)!)
            print("---------------------")
            
            if(modelResponse.success == true){
                UtilityClass.hidehud()
                ApiUtillity.sharedInstance.setLoginModel(modelResponse: modelResponse)
                Toast(text: modelResponse.message ?? "").show()
                if AppSettingVC.isLogout == true
                {
                    modelResponse.facebookResult?.isUpdated = 1
                }
                if Defaults[.UserIsLogin] == true && modelResponse.facebookResult?.isUpdated == 0
                {
                    let vc = self.storyboard?.instantiateViewController(withIdentifier: "UserGenderVC") as! UserGenderVC
                    vc.isUpdate = true
                    self.navigationController?.pushViewController(vc, animated: true)
                    
                }
                else{
                     let vc = self.storyboard?.instantiateViewController(withIdentifier: "MainCardVC") as! MainCardVC
                     self.navigationController?.pushViewController(vc, animated: true)
                }
                
            }
            else {
                
                Toast(text: modelResponse.message ?? "").show()
                UtilityClass.hidehud()
                
            }
            
        }) { (failed) in
        }
    }
}
