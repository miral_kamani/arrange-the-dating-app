//
//  IntroVC.swift
//  Arrange
//
//  Created by Miral Kamani on 20/02/20.
//  Copyright © 2020 Miral Kamani. All rights reserved.
//

import UIKit

var UUID = String()

class IntroVC: UIViewController, UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    
    //MARK:- Outlets
    @IBOutlet weak var C_view: UICollectionView!
    @IBOutlet weak var pager: UIPageControl!
    
    @IBOutlet weak var next1: UIButton!
    @IBOutlet weak var Skip: UIButton!
    
    @IBOutlet var NextWidth: NSLayoutConstraint!
    
     //MARK:- Variable
    
    var temp = 1
    var defaultIndex = Int()
    var btn_frame = CGRect()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if #available(iOS 13.0, *) {
            // Always adopt a light interface style.
            overrideUserInterfaceStyle = .light
        }
        
        //btn_frame = next1.frame
        NotificationCenter.default.addObserver(
            self,
            selector: #selector(self.movetoIndex(notification:)),
            name: Notification.Name("chagePositionOfPaging"),
            object: nil)
        
        self.C_view.register(UINib(nibName: "IntroCell", bundle: nil), forCellWithReuseIdentifier: "IntroCell")
        
        //        self.ws_GetLanguageData()
    }
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 4
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let first_cell = C_view.dequeueReusableCell(withReuseIdentifier: "IntroCell", for: indexPath) as! IntroCell
        
        first_cell.ViewOne.isHidden = true
        first_cell.ViewTwo.isHidden = true
        first_cell.ViewThree.isHidden = true
        first_cell.ViewFour.isHidden = true
        
        
        if (pager.currentPage == 0)
        {
            first_cell.ViewOne.isHidden = false
        }
        else if (pager.currentPage == 1)
        {
            first_cell.ViewTwo.isHidden = false
        }
        else if (pager.currentPage == 2)
        {
            first_cell.ViewThree.isHidden = false
        }
        else
        {
            first_cell.ViewFour.isHidden = false
        }
        
        return first_cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let screenSize = UIScreen.main.bounds
        let screenWidth = screenSize.width
        let screenHeight = screenSize.height
        return CGSize(width: screenWidth, height: screenHeight)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return UIEdgeInsets.init(top: 0, left: 0, bottom: 0, right: 0)
    }
    
    
    //Use for interspacing
    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 0.0
    }
    
    func collectionView(_ collectionView: UICollectionView, layout
        collectionViewLayout: UICollectionViewLayout,
                        minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 0.0
    }
    
    
    @objc func movetoIndex(notification: NSNotification){
        
        pager.currentPage = defaultIndex
        let indexToScrollTo = NSIndexPath(item: defaultIndex, section: 0)
        C_view.scrollToItem(at: indexToScrollTo as IndexPath, at: .left, animated: true)
        self.C_view.reloadData()
    }
    
    
    func scrollViewWillEndDragging(_ scrollView: UIScrollView, withVelocity velocity: CGPoint, targetContentOffset: UnsafeMutablePointer<CGPoint>) {
        
        pager.currentPage = defaultIndex+1
        let pageNumber = Int(targetContentOffset.pointee.x / view.frame.width)
        pager.currentPage = pageNumber
        
        if pager.currentPage == 3
        {
            temp = 0
            Skip.isHidden = true
            next1.setImage(nil, for: .normal)
            next1.setTitle("Done", for: .normal)
            
            self.NextWidth.constant = 50
            // next1.frame = CGRect(x: next1.frame.origin.x, y: next1.frame.origin.y, width: next1.frame.size.width + 20, height: next1.frame.size.height)
            
        }
        else
        {
            Skip.isHidden = false
            next1.setTitle("", for: .normal)
            next1.setImage(#imageLiteral(resourceName: "next"), for: .normal)
            // next1.frame = btn_frame
            self.NextWidth.constant = 28
            if temp == 0
            {
                temp = 1
                // next1.frame = btn_frame
                self.NextWidth.constant = 28
            }
        }
        self.C_view.reloadData()
    }
    
    @IBAction func Next_Tapped(_ sender: UIButton) {
        print(pager.currentPage)
        if pager.currentPage == 3
        {
            print("Done")
            let myVC = storyboard?.instantiateViewController(withIdentifier: "ViewController") as! ViewController
           // myVC.isUpdate = true
            self.navigationController?.pushViewController(myVC, animated: true)
        }
        else if pager.currentPage == 2
        {
            Skip.isHidden = true
            next1.setImage(nil, for: .normal)
            
            next1.setTitle("Done", for: .normal)
            
            // next1.frame = CGRect(x: next1.frame.origin.x, y: //next1.frame.origin.y, width: next1.frame.size.width + 20, height: next1.frame.size.height)
            self.NextWidth.constant = 50
            pager.currentPage += 1
            defaultIndex = pager.currentPage
            NotificationCenter.default.post(name: Notification.Name("chagePositionOfPaging"), object: nil)
        }
        else {
            pager.currentPage += 1
            defaultIndex = pager.currentPage
            //   next1.frame = btn_frame
            self.NextWidth.constant = 28.0
            NotificationCenter.default.post(name: Notification.Name("chagePositionOfPaging"), object: nil)
        }
    }
    
    
    @IBAction func Skip_Tapped(_ sender: UIButton) {
        print("Done")
        let myVC = storyboard?.instantiateViewController(withIdentifier: "ViewController") as! ViewController
        //myVC.isUpdate = true
        self.navigationController?.pushViewController(myVC, animated: true)
    }
    
}


