//
//  MainCardVCViewController.swift
//  Arrange
//
//  Created by Miral Kamani on 07/02/20.
//  Copyright © 2020 Miral Kamani. All rights reserved.
//

import UIKit
import TTSegmentedControl
import Cartography
import TagListView
import Kingfisher
import ImageSlideshow
import Toaster
class MainCardVC: UIViewController,TagListViewDelegate,UIGestureRecognizerDelegate
{
    //MARK:- Outlets
    
    @IBOutlet weak var segmentControl: TTSegmentedControl!
    @IBOutlet weak var flipCard: UIView!
    @IBOutlet var tagList: TagListView!
    @IBOutlet var ScrollView: UIScrollView!
    @IBOutlet var ViewMoreHeight: NSLayoutConstraint!
    @IBOutlet var labelTitleName: UILabel!
    
    @IBOutlet var labelDistance: UILabel!
    @IBOutlet var imageProfile: UIImageView!
    @IBOutlet var ProfileView: UIView!
    @IBOutlet var btnProfile: UIButton!
    @IBOutlet var btnFilter: UIButton!
    @IBOutlet var InformationView: UIView!
    @IBOutlet var labelInfo: CustomLabel!
    @IBOutlet var labelInfoHeight: NSLayoutConstraint!
    @IBOutlet var labelInfoTop: NSLayoutConstraint!
    @IBOutlet var FlipCardHeight: NSLayoutConstraint!
    @IBOutlet var FlipCardBottom: NSLayoutConstraint!
    @IBOutlet var InformationViewTop: NSLayoutConstraint!
    
    //MARK:- Variable
    
    let image = ["1","2"]
    let LabelHobby = ["Ambitious","Love Music","Perfectionist","Wanderluster","Travelling"]
    var swipeableView: ZLSwipeableView!
    var colorIndex = 0
    var smartCardView: SmartCardView?
    var ArrayCard = [ModelCardResult]()
    var card = [String]()
    var ArrayLikedProfile = [ModelLikeResult]()
    var LikedCard = Int()
    var isLike = Int()
    var imagedata = [KingfisherSource]()
    override func viewDidLoad() {
        super.viewDidLoad()
        self.Segment()
        self.navigationController?.interactivePopGestureRecognizer?.delegate = self
        self.navigationController?.interactivePopGestureRecognizer?.isEnabled = false
        self.WS_Get_Card()
        self.setUptagList()
        if UIDevice.current.userInterfaceIdiom == .pad
        {
            self.ProfileView.layer.cornerRadius = 25
        }
        else
        {
            self.ProfileView.layer.cornerRadius = ProfileView.frame.width / 2
        }
        self.btnFilter.addTarget(self, action: #selector(OnclickFilter), for: .touchUpInside)
        
        self.btnProfile.addTarget(self, action: #selector(OnClickProfile), for: .touchUpInside)
    }
    override var preferredStatusBarStyle: UIStatusBarStyle {
        if #available(iOS 13.0, *) {
            return .darkContent
        } else {
            return .default
        }
    }
    override func viewDidLayoutSubviews()
    {
        super.viewDidLayoutSubviews()
        if let swview = swipeableView
        {
            swview.nextView = {
                return self.nextCardView()
            }
        }
    }
    
    func setUptagList()
    {
        tagList.delegate = self
        tagList.textFont = UIFont(name: "Avenir-Heavy", size: 12)!
        tagList.shadowRadius = 2
        tagList.shadowOpacity = 0.4
        tagList.shadowColor = UIColor.white
        tagList.shadowOffset = CGSize(width: 1, height: 1)
        tagList.alignment = .center
    }
    
    func SetProfilenName()
    {
        let loginData = ApiUtillity.sharedInstance.getLoginModel()
        if let BookImage = loginData?.facebookResult?.userProfileImage, BookImage.count != 0
        {
            let url = WS_ProfileImageURL + BookImage
            
            print("profile image URL", url)
            self.imageProfile.kf.setImage(with: URL(string: url), placeholder: UIImage(named: "cancel"), options: nil, progressBlock: nil, completionHandler: { (result) in
                switch result {
                case .success(let value):
                    print("Task done for: \(value.source.url?.absoluteString ?? "")")
                    
                case .failure(let error):
                    print("Job failed: \(error.localizedDescription)")
                }
            })
        }
    }
    
    func Segment()
    {
        
        segmentControl.allowChangeThumbWidth = false
        segmentControl.itemTitles = ["ALL","NEW"]
        segmentControl.didSelectItemWith = { (index, title) -> () in
            print("Selected item \(index)")
        }
        segmentControl.defaultTextColor = UIColor(red: 67, green: 66, blue: 72)
        if UIDevice.current.userInterfaceIdiom == .phone
        {
            segmentControl.selectedTextFont = UIFont(name: "Avenir-Black", size: 12)!
            segmentControl.defaultTextFont = UIFont(name: "Avenir-Black", size: 12)!
        }
        else
        {
            segmentControl.selectedTextFont = UIFont(name: "Avenir-Black", size: 16)!
            segmentControl.defaultTextFont = UIFont(name: "Avenir-Black", size: 16)!
        }
        segmentControl.selectedTextColor = UIColor.white
        segmentControl.thumbGradientColors = [UIColor(red: 192, green: 152, blue: 255), UIColor(red: 140, green: 94, blue: 255)]
        segmentControl.useShadow = true
        segmentControl.layoutSubviews()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.SetProfilenName()
        
        if UIDevice.current.userInterfaceIdiom == .phone
        {
            self.FlipCardHeight.constant = view.frame.size.height  - 100
            labelInfoHeight.constant = labelInfo.bounds.size.height
        }
        else
        {
            self.FlipCardHeight.constant = view.frame.size.height  - 120
            labelInfoHeight.constant = labelInfo.bounds.size.height
        }
    }
    @objc func OnClickViewMore(sender: UIButton)
    {
        ScrollView.setContentOffset(CGPoint(x: 0, y: 400), animated: true)
        // ScrollView.isScrollEnabled = false
        let cards = self.swipeableView.activeViews() as! [SmartCardView]
        
        if cards.count > 0{
            cards.first?.contentview.imageShape.isHidden = true
            cards.first?.contentview.btnViewMore.isHidden = true
            cards.first?.contentview.btnSelectedViewMore.isHidden = false
            cards.first?.contentview.LabelTitle.isHidden = true
            cards.first?.contentview.LabelUniversity.isHidden = true
            cards.first?.contentview.btnViewMore.isHidden = true
        }
        
    }
    
    @objc func OnClickViewMoreSelected(sender: UIButton)
    {
        // ScrollView.isScrollEnabled = true
        ScrollView.setContentOffset(CGPoint(x: 0, y: 0), animated: true)
        let cards = self.swipeableView.activeViews() as! [SmartCardView]
        
        if cards.count > 0{
            cards.first?.contentview.imageShape.isHidden = false
            cards.first?.contentview.btnSelectedViewMore.isHidden = true
            cards.first?.contentview.btnViewMore.isHidden = false
            cards.first?.contentview.LabelTitle.isHidden = false
            cards.first?.contentview.LabelUniversity.isHidden = false
            cards.first?.contentview.btnViewMore.isHidden = false
        }
    }
    
    @objc func OnClickBoost(sender: UIButton)
    {
        if UIDevice.current.userInterfaceIdiom == .phone {
            let Storyboard = UIStoryboard(name: "Main", bundle: nil)
            let navigation = Storyboard.instantiateViewController(withIdentifier: "BoostVC") as! BoostVC
            self.navigationController?.pushViewController(navigation, animated: true)
            print("running on iPhone")
        }
        else
        {
            let loginstoryboard = UIStoryboard(name: "iPadStoryboard", bundle: nil)
            let loginController = loginstoryboard.instantiateViewController(withIdentifier: "iPadBoostVC") as? iPadBoostVC
            self.navigationController?.pushViewController(loginController!, animated: true)
        }
        
    }
    
    @objc func OnclickChat(sender: UIButton)
    {
        let nav = storyboard?.instantiateViewController(withIdentifier: "MainTabVC") as! MainTabVC
        self.navigationController?.pushViewController(nav, animated: true)
        
    }
    @objc func OnClickProfile(sender: UIButton)
    {
        let nav = storyboard?.instantiateViewController(withIdentifier: "ProfileVC") as! ProfileVC
        self.navigationController?.pushViewController(nav, animated: true)
    }
    @objc func OnclickFilter(sender: UIButton)
    {
        let nav = storyboard?.instantiateViewController(withIdentifier: "DilMilFilterVC") as! DilMilFilterVC
        self.navigationController?.pushViewController(nav, animated: true)
    }
    
    @objc func OnclickViewPhotos(sender: UIButton)
    {
        let nav = storyboard?.instantiateViewController(withIdentifier: "PhotosCollectionVC") as! PhotosCollectionVC
        self.navigationController?.pushViewController(nav, animated: true)
    }
    
    func cardInit(){
        self.SetViewData()
        swipeableView = ZLSwipeableView()
        view.addSubview(swipeableView)
        swipeableView.numberOfActiveView = UInt(LabelHobby.count)
        swipeableView.didStart = {view, location, direction in
            print("Did start swiping view at location: \(direction)")
            
            if direction.description == "Left"
            {
                print(view.tag)
                self.isLike = 1
                self.setUnLike(tag: view.tag, view: view as! SmartCardView)
                
            }
            else
            {
                print(view.tag)
                self.isLike = 0
                self.setLike(tag: view.tag, view: view as! SmartCardView)
                
            }
        }
        
        swipeableView.swiping = {view, location, translation in
            print("Swiping at view location: \(location) translation: \(translation)")
        }
        
        swipeableView.didEnd = {view, location in
            print("Swiping at did end")
            
        }
        
        swipeableView.didSwipe = {view, direction, vector in
            
        }
        
        swipeableView.didCancel = {view in
            print("Swiping at didcancel)")
            self.hideBothLable(view: view as! SmartCardView)
        }
        
        swipeableView.didTap = {view, location in
            
        }
        
        swipeableView.didDisappear = { view in
            print("Did disappear swiping view")
            
            let dict = self.ArrayCard[view.tag]
            if let cardId = dict.userId
            {
                self.LikedCard = cardId
            }
            self.WS_Post_LikeProfile()
            
            
            self.hideBothLable(view: view as! SmartCardView)
        }
        
        constrain(swipeableView, self.flipCard) { view1, view2 in
            view1.left == view2.left
            view1.right == view2.right
            view1.top == view2.top
            view1.bottom == view2.bottom - 10
        }
    }
    
    func nextCardView() -> UIView?
    {
        //        self.SetViewData()
        let arrayData = self.ArrayCard
        imagedata.removeAll()
        if (self.colorIndex >= arrayData.count)
        {
            return nil
        }
        self.smartCardView = SmartCardView(frame: swipeableView.bounds)
        self.smartCardView!.contentview = Bundle.main.loadNibNamed("smartCardContentView", owner: self, options: nil)?.first! as? SmartCardView
        //   self.smartCardView?.contentview.imageSlideShow.delegate = self
        
        let dict = self.ArrayCard[self.colorIndex]
        
        if let name = dict.userProfileName, name.count != 0
        {
            if let age = dict.userProfileAge, age != 0
            {
                self.smartCardView?.contentview.LabelTitle.text = "\(name), \(age)"
                self.labelTitleName.text = "\(name), \(age)"
            }else{
                self.smartCardView?.contentview.LabelTitle.text = "\(name)"
                self.labelTitleName.text = "\(name)"
            }
        }
        if let image = dict.userProfileImage, image.count != 0
        {
            if let url = KingfisherSource(urlString: WS_ProfileImageURL+image)
            {
                // imagedata.append(url)
                imagedata.insert(url, at: 0)
            }
            self.smartCardView?.contentview.imageSlideShow.setImageInputs(imagedata)
        }
        
        if let arr = dict.userPhotos, arr.count != 0
        {
            
            for item in arr
            {
                if let iamge = item.userImages, iamge.count != 0
                {
                    if let url = KingfisherSource(urlString: WS_ProfileImageURL+iamge)
                    {
                        imagedata.append(url)
                    }
                }
            }
            
            
            
            self.smartCardView?.contentview.imageSlideShow.setImageInputs(imagedata)
            //            self.smartCardView?.contentview.imageSlideShow.contentScaleMode = UIView.ContentMode.scaleAspectFill
        }
        
        if let univerty = dict.userProfileEducation, univerty.count != 0
        {
            self.smartCardView?.contentview.LabelUniversity.text = univerty
        }else{
            self.smartCardView?.contentview.LabelUniversity.text = ""
        }
        
        //        if let image = dict.userProfileImage, image.count != 0
        //        {
        //            let url = WS_ProfileImageURL + image
        //
        //            print("profile image URL", url)
        //
        //            self.smartCardView?.contentview.ImageFullProfile.kf.setImage(with: URL(string: url), placeholder: UIImage(named: "cancel"), options: nil, progressBlock: nil, completionHandler: { (result) in
        //                switch result {
        //                case .success(let value):
        //                    print("Task done for: \(value.source.url?.absoluteString ?? "")")
        //
        //                case .failure(let error):
        //                    print("Job failed: \(error.localizedDescription)")
        //                }
        //            })
        //        }else{
        //            self.smartCardView?.contentview.ImageFullProfile.image = UIImage(named: "3")
        //        }
        
        if let Traits = dict.userTraits, Traits.count != 0
        {
            var traits = [String]()
            traits.removeAll()
            for item in Traits
            {
                if let t_name = item.TraitName, t_name.count != 0{
                    traits.append(t_name)
                }
            }
            self.tagList.addTags(traits)
        }
        
        self.smartCardView?.contentview.btnSelectedViewMore.isHidden = true
        self.smartCardView?.contentview.btnViewMore.addTarget(self, action: #selector(OnClickViewMore), for: .touchUpInside)
        self.smartCardView?.contentview.btnSelectedViewMore.addTarget(self, action: #selector(OnClickViewMoreSelected), for: .touchUpInside)
        self.smartCardView?.contentview.btnChat.addTarget(self, action: #selector(OnclickChat), for: .touchUpInside)
        self.smartCardView?.contentview.btnBoost.addTarget(self, action: #selector(OnClickBoost), for: .touchUpInside)
        self.smartCardView?.contentview.LikeView.isHidden = true
        self.smartCardView?.contentview.UnlikeView.isHidden = true
        self.smartCardView!.contentview.superVC = self
        self.smartCardView?.contentview.btnViewPhotos.addTarget(self, action: #selector(OnclickViewPhotos), for: .touchUpInside)
        self.smartCardView!.contentview.translatesAutoresizingMaskIntoConstraints = false
        self.smartCardView!.contentview.backgroundColor = self.smartCardView!.backgroundColor
        self.smartCardView!.addSubview(self.smartCardView!.contentview)
        
        constrain(self.smartCardView!.contentview, self.smartCardView!) { view1, view2 in
            view1.left == view2.left
            view1.top == view2.top
            view1.width == self.smartCardView!.bounds.width
            view1.height == self.smartCardView!.bounds.height
        }
        
        print("Color Index::::::::: \(colorIndex)")
        self.smartCardView!.tag = self.colorIndex
        colorIndex += 1
        return self.smartCardView
    }
    
    func setLike(tag: Int, view: SmartCardView)
    {
        let cards = self.swipeableView.activeViews() as! [SmartCardView]
        print("current card tag",tag)
        if cards.count > 0{
            cards.first?.contentview.LikeView.isHidden = false
        }
    }
    func SetViewData()
    {
        let dict = ArrayCard[view.tag]
        if let name = dict.userProfileName, name.count != 0
        {
            if let age = dict.userProfileAge, age != 0
            {
                self.labelTitleName.text = "\(name), \(age)"
            }
        }
        
        if let Traits = dict.userTraits, Traits.count != 0
        {
            var traits = [String]()
            traits.removeAll()
            for item in Traits
            {
                if let t_name = item.TraitName, t_name.count != 0{
                    traits.append(t_name)
                }
            }
            self.tagList.addTags(traits)
        }
        
        if let about = dict.userProfileAbout, about.count != 0
        {
            self.labelInfo.text = about
        }
        
    }
    func setUnLike(tag: Int, view: SmartCardView)
    {
        let cards = self.swipeableView.activeViews() as! [SmartCardView]
        print("current card tag",tag)
        if cards.count > 0{
            cards.first?.contentview.UnlikeView.isHidden = false
        }
    }
    
    func hideBothLable(view: SmartCardView){
        let cards = self.swipeableView.activeViews() as! [SmartCardView]
        
        if cards.count > 0{
            cards.first?.contentview.UnlikeView.isHidden = true
            cards.first?.contentview.LikeView.isHidden = true
        }
    }
}

extension MainCardVC
{
    func WS_Get_Card()
    {
        APIClient<ModelGetCard>().API_GET(Url: ws_Get_Card, Params: [:], Spinner: 1, Authentication: true, Progress: true, Alert: true, Offline: false, SuperVC: self, completionSuccess: { (modelResponse) in
            
            print("---------------------")
            print("Result:",modelResponse.toJSONString(prettyPrint: true)!)
            print("---------------------")
            
            if(modelResponse.success == true)
            {
                if let result = modelResponse.cardResult
                {
                    for data in result
                    {
                        self.ArrayCard.append(data)
                    }
                    self.cardInit()
                    self.SetViewData()
                }
            }
            else
            {
                if modelResponse.message == "Authentication API is invalid!" {
                    let vc = self.storyboard?.instantiateViewController(withIdentifier: "LogoutVc") as! LogoutVc
                    self.navigationController?.pushViewController(vc, animated: false)
                }
                else {
                    UtilityClass.hidehud()
                    Toast(text: modelResponse.message ?? "").show()
                }
            }
            
        }) { (failed) in
            
        }
    }
    
    func WS_Post_LikeProfile()
    {
        var parameters = [String: Any]()
        
        parameters = ["ref_to_user_id" : LikedCard,
                      "is_liked" : isLike]
        print(parameters)
        APIClient<ModelLikeProfileBase>().API_POST(Url: ws_Post_LikeProfile, Params: parameters as [String:AnyObject], Authentication: true, Progress: true, Alert: true, Offline: false, SuperVC: self, completionSuccess: { (modelResponse) in
            
            print("---------------------")
            print("Login Result:",modelResponse.toJSONString(prettyPrint: true)!)
            print("---------------------")
            
            if(modelResponse.success == true){
//                Toast(text: modelResponse.message ?? "").show()
            }
            else {
                
            }
            
        }) { (failed) in
        }
    }
    
    
}
