//
//  InterestVC.swift
//  Arrange
//
//  Created by Miral Kamani on 03/03/20.
//  Copyright © 2020 Miral Kamani. All rights reserved.
//

import UIKit
import Kingfisher
import CoreLocation
import Toaster
class InterestVC: UIViewController {
    
    //MARK:- Outlets
    
    @IBOutlet var imageStatic: UIImageView!
    @IBOutlet var CView: UICollectionView!
    @IBOutlet var btnDone: UIButton!
    
    //MARK:- Variable
    
    var arrayTraits = [ModelResult]()
    var selectedIndex = Int()
    var Index = [Int]()
    var del = Int()
    var tmpID = Int()
    
    var locationManager = CLLocationManager()
    var lat = Double()
    var long = Double()
    var addressStr = String()
    var Country = String()
    var State = String()
    var City = String()
    static var SelectedTraits = [Int]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.btnDone.addTarget(self, action: #selector(OnClickDone), for: .touchUpInside)
        self.RegisterXIB()
    }
    @objc func OnClickDone(sender: UIButton)
    {
        if InterestVC.SelectedTraits.isEmpty
        {
            Toast(text: "Please select at least five Interest").show()
        }
        else
        {
            self.WS_Post_Update()
        }
    }
    @IBAction func backTapped(_ sender: UIButton)
    {
        self.navigationController?.popViewController(animated: true)
    }
    override func viewWillAppear(_ animated: Bool) {
        self.WS_Get_Interest()
        self.getAdd()
    }
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    func convertIntoJSONString(arrayObject: [Any]) -> String? {
        
        do {
            let jsonData: Data = try JSONSerialization.data(withJSONObject: arrayObject, options: [])
            if  let jsonString = NSString(data: jsonData, encoding: String.Encoding.utf8.rawValue) {
                return jsonString as String
            }
            
        } catch let error as NSError {
            print("Array convertIntoJSON - \(error.description)")
        }
        return nil
    }
    
    func getAdd() {
        locationManager.delegate = self
        locationManager.requestWhenInUseAuthorization()
        locationManager.desiredAccuracy = kCLLocationAccuracyBest
        locationManager.startUpdatingLocation()
        
    }
    
    @objc func OnClickDeselect(sender: UIButton)
    {
        
        self.selectedIndex = sender.tag
        if InterestVC.SelectedTraits.contains(selectedIndex)
        {
            for same in Index
            {
                if same == selectedIndex
                {
                    print("Found \(same) for index \(del)")
                    del = InterestVC.SelectedTraits.firstIndex(of: selectedIndex) ?? 0
                    InterestVC.SelectedTraits.remove(at: del)
                    break
                }
                
            }
        }
        self.CView.reloadData()
    }
}
extension InterestVC: UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout
{
    func RegisterXIB()
    {
        self.CView.register(UINib(nibName: "InterestCell", bundle: nil), forCellWithReuseIdentifier: "InterestCell")
    }
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return arrayTraits.count
    }
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "InterestCell", for: indexPath) as! InterestCell
        let dict = arrayTraits[indexPath.row]
        if InterestVC.SelectedTraits.contains(dict.traitsId!) {
            cell.RoundView.layer.borderColor = UIColor(red: 192, green: 162, blue: 255).cgColor
            cell.RoundView.layer.borderWidth = 2.0
            
        }
        else {
            cell.RoundView.layer.borderColor = UIColor.clear.cgColor
            cell.RoundView.layer.borderWidth = 2.0
        }
        cell.contentView.layer.shadowColor = UIColor.black.cgColor
        cell.contentView.layer.shadowOpacity = 0.2
        cell.contentView.layer.shadowRadius = 2.0
        if let BookImage = dict.traitsImage, BookImage.count != 0
        {
            let url = WS_ProfileImageURL + BookImage
            
            print("profile image URL", url)
            cell.imageInterest.kf.setImage(with: URL(string: url), placeholder: UIImage(named: "default"), options: nil, progressBlock: nil, completionHandler: { (result) in
                switch result {
                case .success(let value):
                    print("Task done for: \(value.source.url?.absoluteString ?? "")")
                    
                case .failure(let error):
                    print("Job failed: \(error.localizedDescription)")
                }
            })
        }
        cell.labelName.text = dict.traitsName
        return cell
    }
    func collectionView(_ collectionView: UICollectionView,layout collectionViewLayout: UICollectionViewLayout,sizeForItemAt indexPath: IndexPath) -> CGSize
    {
        
        if UIDevice.current.userInterfaceIdiom == .phone
        {
            return CGSize(width: CView.frame.size.width / 4 , height: 100)
        }
        else
        {
            return CGSize(width: CView.frame.size.width / 4, height: 150)
        }
    }
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let dict = arrayTraits[indexPath.row]
        selectedIndex = indexPath.row
        if InterestVC.SelectedTraits.contains(dict.traitsId!)
        {
            InterestVC.SelectedTraits = InterestVC.SelectedTraits.filter{$0 != dict.traitsId!}
        }
        else
        {
            InterestVC.SelectedTraits.append(dict.traitsId!)
            
        }
        CView.reloadData()
        //CView.reloadItems(at: [indexPath])
        
    }
}

extension InterestVC
{
    func WS_Get_Interest()
    {
        APIClient<ModelTraitsBase>().API_GET(Url: ws_get_Interest, Params: [:], Spinner: 1, Authentication: true, Progress: true, Alert: true, Offline: false, SuperVC: self, completionSuccess: { (modelResponse) in
            
            print("---------------------")
            print("Result:",modelResponse.toJSONString(prettyPrint: true)!)
            print("---------------------")
            self.arrayTraits.removeAll()
            if(modelResponse.success == true)
            {
                if let result = modelResponse.result, result.count != 0
                {
                    for data in result
                    {
                        self.arrayTraits.append(data)
                    }
                }
            }
            else
            {
                
            }
            self.CView.reloadData()
            
        }) { (failed) in
            
        }
    }
    
    func WS_Post_Update()
    {
        var parameters = [String: Any]()
        let TraitsJsonData = self.convertIntoJSONString(arrayObject: InterestVC.SelectedTraits)
        parameters = [
            "dob": UserAgeVC.BirthDate,
            "religion": UserReligionVC.ReligionID,
            "community": UserCommunityVC.CommunityID,
            "career": UserCareerVC.CareerID,
            "traits": TraitsJsonData!,
            "raised_in": UserRaisedInVC.RaisedId,
            "user_height": UserHeightVC.Height,
            "latitude": lat,
            "longitude": long,
            "address": addressStr,
            "age" : "20",
            "gender": "Female",
            "country": Country,
            "state": State,
            "city": City]
        print(parameters)
        APIClient<ModelSocialLoginBase>().API_PUT(Url: ws_Post_Update, Params: parameters as [String:AnyObject], Authentication: true, Progress: true, Alert: true, Offline: false, SuperVC: self, completionSuccess: { (modelResponse) in
            UtilityClass.showhud()
            print("---------------------")
            print("Login Result:",modelResponse.toJSONString(prettyPrint: true)!)
            print("---------------------")
            UtilityClass.hidehud()
            if(modelResponse.success == true){
                ViewController.isUpdate = true
                ApiUtillity.sharedInstance.setLoginModel(modelResponse: modelResponse)
                let vc = self.storyboard?.instantiateViewController(withIdentifier: "MainCardVC") as! MainCardVC
                self.navigationController?.pushViewController(vc, animated: true)
                
            }
            else {
                Toast(text: modelResponse.message ?? "").show()
            }
            
        }) { (failed) in
        }
    }
}

extension InterestVC: CLLocationManagerDelegate
{
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        
        let userlocation:CLLocation = locations[0] as CLLocation
        
        lat = userlocation.coordinate.latitude
        long = userlocation.coordinate.longitude
        
        CLGeocoder().reverseGeocodeLocation(userlocation) { [weak self] (placemarks, error) in
            guard self != nil else {return}
            
            if let _ = error
            {
                return
            }
            
            guard let placemark = placemarks?.first else
            {
                return
            }
            
            let streetName = placemark.thoroughfare ?? ""
            let CityName = placemark.locality ?? ""
            let streetNumber = placemark.subThoroughfare ?? ""
            let postalCode = placemark.postalCode ?? ""
            let countryName = placemark.country ?? ""
            let stateName = placemark.administrativeArea ?? ""
            self?.Country = countryName
            self?.State = stateName
            self?.City = CityName
            
            DispatchQueue.main.async {
                self?.addressStr = "\(streetNumber),\(streetName),\(CityName), \(stateName), \(countryName), \(postalCode)"
                print(self?.addressStr)
            }
        }
        
    }
}
