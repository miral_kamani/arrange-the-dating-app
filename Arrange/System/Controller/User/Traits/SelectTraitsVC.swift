//
//  SelectTraitsVC.swift
//  Arrange
//
//  Created by Miral Kamani on 26/02/20.
//  Copyright © 2020 Miral Kamani. All rights reserved.
//

import UIKit

class SelectTraitsVC: UIViewController {
  
    //MARK:- Outlets
    @IBOutlet var txtTraits: UnderLineImageTextField!
    @IBOutlet var btnDone: UIButton!
    @IBOutlet var PageControl: UIPageControl!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.btnDone.addTarget(self, action: #selector(OnClickDone), for: .touchUpInside)
        
    }
    @IBAction func backTapped(_ sender: UIButton)
    {
        self.navigationController?.popViewController(animated: true)
    }
    @objc func OnClickDone(sender: UIButton)
    {
        let navigate = self.storyboard?.instantiateViewController(withIdentifier: "MainCardVC") as! MainCardVC
        self.navigationController?.pushViewController(navigate, animated: true)
    }
 
}
