//
//  DilMilFilterVC.swift
//  Arrange
//
//  Created by Miral Kamani on 13/02/20.
//  Copyright © 2020 Miral Kamani. All rights reserved.
//

import UIKit
import Toaster

class DilMilFilterVC: UIViewController,UIGestureRecognizerDelegate {
    
    //MARK:- Variable
    
    let arrayImageIconOne = ["gender","age","height","ying-yang","location-1","community","career","raisedIn"]
    let arrayTitleNameOne = ["Gender","Age","Height","Religion","Location","Community","Career","Raised In"]
    var selectedIndex: Int?
    var sectionID: Int?
     static var isRemove = Int()
    //MARK:- Outlets
    
    @IBOutlet var tblReload: UITableView!
    @IBOutlet var btnBack: UIButton!
    @IBOutlet var btnApply: UIButton!
    @IBOutlet var btnCancel: UIButton!
    
    var isClear = Bool()
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.interactivePopGestureRecognizer?.delegate = self
        self.navigationController?.interactivePopGestureRecognizer?.isEnabled = true
        self.btnBack.addTarget(self, action: #selector(OnClickBack), for: .touchUpInside)
        self.btnApply.addTarget(self, action: #selector(OnClickApply), for: .touchUpInside)
        self.btnCancel.addTarget(self, action: #selector(OnClickCancel), for: .touchUpInside)
        self.RegisterXIB()
    }
    override func viewWillAppear(_ animated: Bool) {
        
        if DilMilFilterVC.isRemove == 0
        {
            Defaults[.religion] = ""
            Defaults[.Gender] = ""
            Defaults[.Height] = ""
            Defaults[.community] = ""
            Defaults[.career] = ""
            Defaults[.raisedIn] = ""
            Defaults[.ages] = ""
            Defaults[.Location] = ""
            ReligionVC.ReligionID = 0
            CommunityVC.CommunityID = 0
            CareerVC.CareerID = 0
            RaisedInVC.RaisedInID = 0
            GenderVC.Gender = ""
        }
        else if DilMilFilterVC.isRemove == 2
        {
            self.tblReload.reloadData()
            
        }
        else
        {
            self.tblReload.reloadData()
            DilMilFilterVC.isRemove = 0
        }
    }
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    @objc func OnClickBack(sender: UIButton){
        self.navigationController?.popViewController(animated: true)
    }
    @objc func OnClickApply(sender: UIButton)
    {
        self.WS_Post_Filter()
    }
    
    @objc func OnClickCancel(sender: UIButton)
    {
        Defaults[.religion] = ""
        Defaults[.Gender] = ""
        Defaults[.Height] = ""
        Defaults[.community] = ""
        Defaults[.career] = ""
        Defaults[.raisedIn] = ""
        Defaults[.ages] = ""
        Defaults[.Location] = ""
        ReligionVC.ReligionID = 0
        CommunityVC.CommunityID = 0
        CareerVC.CareerID = 0
        RaisedInVC.RaisedInID = 0
        GenderVC.Gender = ""
        self.tblReload.reloadData()
    }
    @objc func OnClickCell(sender: DCustomButton)
    {
        selectedIndex = sender.tag
        print(selectedIndex)
        tblReload.deselectRow(at: IndexPath(row: selectedIndex ?? 0, section: 0), animated: true)
        if selectedIndex == 0
        {
            let navigation = storyboard?.instantiateViewController(withIdentifier: "GenderVC") as! GenderVC
            self.navigationController?.pushViewController(navigation, animated: true)
        }
        else if selectedIndex == 1
        {
            let navigation = storyboard?.instantiateViewController(withIdentifier: "AgeVC") as! AgeVC
            self.navigationController?.pushViewController(navigation, animated: true)
        }
        else if selectedIndex == 2
        {
           let navigation = storyboard?.instantiateViewController(withIdentifier: "HeightVC") as! HeightVC
           self.navigationController?.pushViewController(navigation, animated: true)
        }
        else if selectedIndex == 3
        {
            let navigation = storyboard?.instantiateViewController(withIdentifier: "ReligionVC") as! ReligionVC
            self.navigationController?.pushViewController(navigation, animated: true)
        }
        else if selectedIndex == 4
        {
            let navigation = storyboard?.instantiateViewController(withIdentifier: "LocationVC") as! LocationVC
            self.navigationController?.pushViewController(navigation, animated: true)
        }
        else if selectedIndex == 5
        {
            let navigation = storyboard?.instantiateViewController(withIdentifier: "CommunityVC") as! CommunityVC
            self.navigationController?.pushViewController(navigation, animated: true)
        }
        else if selectedIndex == 6
        {
            let navigation = storyboard?.instantiateViewController(withIdentifier: "CareerVC") as! CareerVC
            self.navigationController?.pushViewController(navigation, animated: true)
        }
        else if selectedIndex == 7
        {
            let navigation = storyboard?.instantiateViewController(withIdentifier: "RaisedInVC") as! RaisedInVC
            self.navigationController?.pushViewController(navigation, animated: true)
        }
    }
}
extension DilMilFilterVC: UITableViewDataSource,UITableViewDelegate
{
    func RegisterXIB()
    {
        self.tblReload.register(UINib(nibName: "DilMilFilterCell", bundle: nil), forCellReuseIdentifier: "DilMilFilterCell")
    }
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrayTitleNameOne.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "DilMilFilterCell", for: indexPath) as! DilMilFilterCell
        cell.labelTitle.text = arrayTitleNameOne[indexPath.row]
        let image = arrayImageIconOne[indexPath.row]
        cell.ImageIcon.image = UIImage(named: image)
        cell.btnClick.tag = indexPath.row
        
        if indexPath.row == 0
        {
            cell.labelSelectionData.text = Defaults[.Gender]
        }
        
        else if indexPath.row == 1
        {
            cell.labelSelectionData.text = Defaults[.ages]
        }
        
        else if indexPath.row == 2
        {
            cell.labelSelectionData.text = Defaults[.Height]
        }
        else if indexPath.row == 3
        {
            cell.labelSelectionData.text = Defaults[.religion]
        }
        else if indexPath.row == 4
        {
            cell.labelSelectionData.text = Defaults[.Location]
        }
        else if indexPath.row == 5
        {
            cell.labelSelectionData.text = Defaults[.community]
        }
        else if indexPath.row == 6
        {
            cell.labelSelectionData.text = Defaults[.career]
        }
        else if indexPath.row == 7
        {
            cell.labelSelectionData.text = Defaults[.raisedIn]
        }
        
        cell.btnClick.addTarget(self, action: #selector(OnClickCell), for: .touchUpInside)
        return cell
        
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
}

extension DilMilFilterVC
{
    func WS_Post_Filter()
    {
        var parameters = [String : Any]()
        parameters = ["gender" : Defaults[.Gender],
                      "religion" :  Defaults[.religion],
                      "community" : Defaults[.community],
                      "career" : Defaults[.career],
                      "raised_in" : Defaults[.raisedIn],
                      "min_age" : AgeVC.MinAge,
                      "max_age" : AgeVC.MaxAge,
                      "min_height" : HeightVC.MinHeight,
                      "max_height" : HeightVC.MaxHeight,
                      "country" : LocationVC.FilterCountry,
                      "state" : LocationVC.FilterState,
                      "city" : LocationVC.FilterCity]
        print(parameters)

               APIClient<ModelGetCard>().API_POST(Url: ws_Post_Filter, Params: parameters as [String:AnyObject], Authentication: true, Progress: true, Alert: true, Offline: false, SuperVC: self, completionSuccess: { (modelResponse) in
                   UtilityClass.showhud()
                   print("---------------------")
                   print("Login Result:",modelResponse.toJSONString(prettyPrint: true)!)
                   print("---------------------")

                   if(modelResponse.success == true){
                       UtilityClass.hidehud()
                       DilMilFilterVC.isRemove = 2
                       Toast(text: "Filter applied Successfully.").show()
                   }
                   else {
                        DilMilFilterVC.isRemove = 2
                       Toast(text: "Filter applied Successfully.").show()
                       UtilityClass.hidehud()
                   }

               }) { (failed) in
               }
    }
}
