//
//  UserReligionVC.swift
//  Arrange
//
//  Created by Miral Kamani on 14/02/20.
//  Copyright © 2020 Miral Kamani. All rights reserved.
//

import UIKit
import ObjectMapper
import Toaster
class UserReligionVC: UIViewController,UIGestureRecognizerDelegate {
    
    //MARK:- Outlets
    
    @IBOutlet var CollectionReload: UICollectionView!
    @IBOutlet var PageControl: UIPageControl!
    
    //MARK:- Variable
    
    var arrayReligion = [ModelReligionResult]()
    var isUpdate = Bool()
    static var ReligionName = String()
    static var ReligionID = Int()
    override func viewDidLoad() {
        self.WS_Get_Religion()
        self.navigationController?.interactivePopGestureRecognizer?.delegate = self
        self.navigationController?.interactivePopGestureRecognizer?.isEnabled = true
        super.viewDidLoad()
        self.RegisterXIB()
    }
    override func viewWillAppear(_ animated: Bool) {
        
    }
    
    @IBAction func backTapped(_ sender: UIButton)
    {
        self.navigationController?.popViewController(animated: true)
    }
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    @objc func OnClickCellbtn(sender: UIButton)
    {
        let dict = arrayReligion[sender.tag]
        UserReligionVC.ReligionName = dict.religionName!
        UserReligionVC.ReligionID = dict.religionId!
        CollectionReload.reloadData()
        DispatchQueue.main.asyncAfter(deadline: .now() + 1) {
            if self.isUpdate == true
            {
                let navigate = self.storyboard?.instantiateViewController(withIdentifier: "UserCommunityVC") as! UserCommunityVC
                navigate.isUpdate = true
                self.navigationController?.pushViewController(navigate, animated: true)
            }
            else
            {
                self.navigationController?.popViewController(animated: true)
            }
        }
    }
}

extension UserReligionVC: UICollectionViewDelegate,UICollectionViewDelegateFlowLayout,UICollectionViewDataSource
{
    func RegisterXIB()
    {
        self.CollectionReload.register(UINib(nibName: "UserSelectionCell", bundle: nil), forCellWithReuseIdentifier: "UserSelectionCell")
    }
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return arrayReligion.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "UserSelectionCell", for: indexPath) as! UserSelectionCell
        let dict = arrayReligion[indexPath.row]
        if let name = dict.religionName, name.count != 0
        {
            cell.labelTitleName.text = name
        }
        if UserReligionVC.ReligionID == dict.religionId
        {
            cell.WholeView.layer.borderColor = UIColor(red: 140, green: 94, blue: 255).cgColor
            cell.WholeView.layer.borderWidth = 2.0
        }
        else
        {
            cell.WholeView.layer.borderColor = UIColor.clear.cgColor
        }
        cell.btnSelect.tag = indexPath.row
        cell.btnSelect.addTarget(self, action: #selector(OnClickCellbtn), for: .touchUpInside)
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView,layout collectionViewLayout: UICollectionViewLayout,sizeForItemAt indexPath: IndexPath) -> CGSize
    {
        return CGSize(width: CollectionReload.frame.size.width - 20, height: 75.0)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 10.0
    }
}

extension UserReligionVC
{
    func WS_Get_Religion()
    {
        APIClient<ModelReligionBase>().API_GET(Url: ws_Get_Religion, Params: [:], Spinner: 1, Authentication: true, Progress: true, Alert: true, Offline: false, SuperVC: self, completionSuccess: { (modelResponse) in
            
            print("---------------------")
            print("Result:",modelResponse.toJSONString(prettyPrint: true)!)
            print("---------------------")
            self.arrayReligion.removeAll()
            if(modelResponse.success == true)
            {
                if let result = modelResponse.religionResult, result.count != 0
                {
                    for data in result
                    {
                        self.arrayReligion.append(data)
                    }
                }
            }
            else
            {
                 if modelResponse.message == "Authentication API is invalid!" {
                     let vc = self.storyboard?.instantiateViewController(withIdentifier: "LogoutVc") as! LogoutVc
                     self.navigationController?.pushViewController(vc, animated: false)
                 }
                 else {
                     UtilityClass.hidehud()
                     Toast(text: modelResponse.message ?? "").show()
                 }
            }
            self.CollectionReload.reloadData()
        }) { (failed) in
            
        }
    }
}
