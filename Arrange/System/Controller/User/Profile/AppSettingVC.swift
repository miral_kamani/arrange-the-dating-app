//
//  AppSettingVC.swift
//  Arrange
//
//  Created by Miral Kamani on 18/02/20.
//  Copyright © 2020 Miral Kamani. All rights reserved.
//

import UIKit
import FBSDKCoreKit
import FBSDKLoginKit
import Toaster
class AppSettingVC: UIViewController,UIGestureRecognizerDelegate {
    
   //MARK:- Outlets
    
    @IBOutlet var SwitchNewMatches: UISwitch!
    @IBOutlet var SwitchMessages: UISwitch!
    @IBOutlet var SwitchMomentsLike: UISwitch!
    @IBOutlet var btnDeleteAccount: UIButton!
    
    @IBOutlet var btnLogout: RoundButton!
    @IBOutlet var tblReload: UITableView!
    
     //MARK:- Variable
    
     let arraytable = ["Privacy Policy","Terms and Services","Report"]
    static var isLogout = Bool()
    override func viewDidLoad() {
        super.viewDidLoad()
        tblReload.tableFooterView = UIView()
        self.navigationController?.interactivePopGestureRecognizer?.delegate = self
        self.navigationController?.interactivePopGestureRecognizer?.isEnabled = true
        self.btnLogout.addTarget(self, action: #selector(OnClickLogout), for: .touchUpInside)
        self.btnDeleteAccount.addTarget(self, action: #selector(OnClickDeleteAccount), for: .touchUpInside)
        self.RegisterXIB()
    }
    
    @IBAction func btnBack(_ sender: UIButton)
    {
        self.navigationController?.popViewController(animated: true)
    }
    func RegisterXIB()
    {
        self.tblReload.register(UINib(nibName: "LegalCell", bundle: nil), forCellReuseIdentifier: "LegalCell")
    }
    
    @objc func OnClickDeleteAccount(sender: UIButton)
    {
        self.WS_DeleteAccountAPI()
    }
    
    @objc func OnClickLogout(sender: UIButton)
    {
        self.WS_LogoutAPI()
    }
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
}
extension AppSettingVC: UITableViewDelegate,UITableViewDataSource
{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arraytable.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "LegalCell", for: indexPath) as! LegalCell
        cell.labelTitleNAme.text = arraytable[indexPath.row]
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 60.0
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
    {
        if indexPath.row == 0
        {
            
        }
        else if indexPath.row == 1
        {
            
        }
        else
        {
            let navigate = self.storyboard?.instantiateViewController(withIdentifier: "ReportVC") as! ReportVC
            self.navigationController?.pushViewController(navigate, animated: true)
        }
    }
}

extension AppSettingVC
{
    //MARK:- Logout API Parsing
    func WS_LogoutAPI()
    {
        let parameters = [String: Any]()
        var url = String()
        url = ws_Post_Logout
        
        APIClient<ModelBaseSuccess>().API_POST(Url: url, Params: parameters as [String:AnyObject], Authentication: true, Progress: true, Alert: true, Offline: false, SuperVC: self, completionSuccess: { (modelResponse) in
            
            print("---------------------")
            print("Logout Result:",modelResponse.toJSONString(prettyPrint: true)!)
            print("---------------------")
            
            if(modelResponse.success == true)
            {
                Toast(text: modelResponse.message ?? "").show()
                loginManager.logOut()
                AppSettingVC.isLogout = true
                let fcm = Defaults[.FCMToken]
                Defaults.removeAll()
                Defaults[.FCMToken] = fcm
                DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) {
                    
                    let vc = self.storyboard?.instantiateViewController(withIdentifier: "ViewController") as! ViewController
                    self.navigationController?.pushViewController(vc, animated: true)
                }
            }
            else
            {
                
            }
            
        }) { (failed) in
            
        }
    }
    
    //MARK:- Delete Account Parsing
    func WS_DeleteAccountAPI() {
        let parameters = [String: Any]()
        var url = String()
        url = ws_Delete_Account
        
        APIClient<ModelBaseSuccess>().API_DELETE(Url: url, Params: parameters as [String:AnyObject], Authentication: true, Progress: true, Alert: true, Offline: false, SuperVC: self, completionSuccess: { (modelResponse) in
            
            print("---------------------")
            print("Delete Result:",modelResponse.toJSONString(prettyPrint: true)!)
            print("---------------------")
             
            if(modelResponse.success == true) {
                DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) {
                    Toast(text: modelResponse.message ?? "").show()
                    UserAgeVC.Age = 0
                    UserHeightVC.Height = ""
                    UserCareerVC.CareerID = 0
                    UserCommunityVC.CommunityID = 0
                    UserReligionVC.ReligionID = 0
                    UserRaisedInVC.RaisedId = 0
                    InterestVC.SelectedTraits.removeAll()
                    let fcm = Defaults[.FCMToken]
                    Defaults.removeAll()
                    Defaults[.FCMToken] = fcm
                    let vc = self.storyboard?.instantiateViewController(withIdentifier: "ViewController") as! ViewController
                    self.navigationController?.pushViewController(vc, animated: true)
                }
            }
            else {
               
            }
            
        }) { (failed) in}
    }
}



