//
//  ProfileVC.swift
//  Arrange
//
//  Created by Miral Kamani on 14/02/20.
//  Copyright © 2020 Miral Kamani. All rights reserved.
//

import UIKit
import Kingfisher
import ObjectMapper
import Toaster

class ProfileVC: UIViewController,UIImagePickerControllerDelegate,UINavigationControllerDelegate,UIGestureRecognizerDelegate {
    
    // MARK:- Outlets
    
    @IBOutlet var ProfileView: UIViewX!
    @IBOutlet var imageProfile: RoundImageView!
    @IBOutlet var labelUserName: UILabel!
    @IBOutlet var btnAddPhotos: UIButton!
    //   @IBOutlet var PageControl: UIPageControl!
    @IBOutlet var CollectionReload: UICollectionView!
    @IBOutlet var tblReload: UITableView!
    @IBOutlet var view_coll_height: NSLayoutConstraint!
    @IBOutlet weak var collectionMainView: UIView!
    
    @IBOutlet var btnDeletePhotos: UIButton!
    // MARK:- variables
    var imagePicker: UIImagePickerController!
    var selectedImage = UIImage()
    let arrayTableData = ["Account Details","Settings","ContactUs","Share"]
    var arrayimages = [ModelUserImageResult]()
    let arrayImageIcon = ["account","settings","call","share"]
    var images = [UIImage]()
    var uploadedImageNames = String()
    var ImageID = [Int]()
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setDevice()
    
        self.btnDeletePhotos.addTarget(self, action: #selector(OnclickDeleteImage), for: .touchUpInside)
        self.navigationController?.interactivePopGestureRecognizer?.delegate = self
        self.navigationController?.interactivePopGestureRecognizer?.isEnabled = true
        tblReload.tableFooterView = UIView()
        self.btnAddPhotos.addTarget(self, action: #selector(OnClickAddPhotos), for: .touchUpInside)
        self.RegisterXIB()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.SetProfilenName()
        self.CollectionReload.reloadData()
        self.WS_Get_UploadedImage()
    }
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    @objc func OnClickSelectImage(sender: UIButton)
    {
        let dict = arrayimages[sender.tag]
        print(sender.tag)
        if ImageID.contains(dict.userImagesId!)
        {
            self.ImageID = ImageID.filter{$0 != dict.userImagesId!}
        }
        else
        {
            self.ImageID.append(dict.userImagesId!)
        }
        CollectionReload.reloadData()
    }
    @objc func OnclickDeleteImage(sender: UIButton)
    {
        self.WS_Delete_Image()
    }
    func convertIntoJSONString(arrayObject: [Any]) -> String? {
        
        do {
            let jsonData: Data = try JSONSerialization.data(withJSONObject: arrayObject, options: [])
            if  let jsonString = NSString(data: jsonData, encoding: String.Encoding.utf8.rawValue) {
                return jsonString as String
            }
            
        } catch let error as NSError {
            print("Array convertIntoJSON - \(error.description)")
        }
        return nil
    }
    func SetProfilenName()
    {
        let loginData = ApiUtillity.sharedInstance.getLoginModel()
        if let uName = loginData?.facebookResult?.userProfileName, uName.count != 0
        {
            self.labelUserName.text = uName.capitalized
        }
        self.imageProfile.image = ProfileImage
        if let BookImage = loginData?.facebookResult?.userProfileImage, BookImage.count != 0
        {
            let url = WS_ProfileImageURL + BookImage
            
            print("profile image URL", url)
            self.imageProfile.kf.setImage(with: URL(string: url), placeholder: UIImage(named: "default"), options: nil, progressBlock: nil, completionHandler: { (result) in
                switch result {
                case .success(let value):
                    print("Task done for: \(value.source.url?.absoluteString ?? "")")
                    
                case .failure(let error):
                    print("Job failed: \(error.localizedDescription)")
                }
            })
        }
        
    }
    func setDevice()
    {
        if UIDevice.current.userInterfaceIdiom == .phone
        {
            self.ProfileView.layer.cornerRadius = 55.0
        }
        else
        {
            self.ProfileView.layer.cornerRadius = 100
        }
    }
    
    func RegisterXIB()
    {
        self.tblReload.register(UINib(nibName: "TableCell", bundle: nil), forCellReuseIdentifier: "TableCell")
        self.CollectionReload.register(UINib(nibName: "ImageCell", bundle: nil), forCellWithReuseIdentifier: "ImageCell")
    }
    
    @IBAction func backTapped(_ sender: UIButton)
    {
        self.navigationController?.popViewController(animated: true)
    }
    
    func displayShareSheet(shareContent:String, sourceRect: CGRect?) {
        
        
        let activityViewController = UIActivityViewController(activityItems: [shareContent as NSString], applicationActivities: nil)
        activityViewController.modalPresentationStyle = .popover
        activityViewController.popoverPresentationController?.sourceView = activityViewController.view
        if let sourceRect = sourceRect {
            activityViewController.popoverPresentationController?.sourceRect = sourceRect
        }
        self.present(activityViewController, animated: true, completion: {})
    }
    
    @objc func OnClickAddPhotos(sender: UIButton)
    {
        self.showAlertForImagePicker()
    }
    
    func showAlertForImagePicker()
    {
        
        let alert = UIAlertController(title: "Choose Image", message: nil, preferredStyle: .actionSheet)
        alert.popoverPresentationController?.sourceView = self.view
        alert.addAction(UIAlertAction(title: "Camera", style: .default, handler: { _ in
            self.openCamera()
        }))
        
        alert.addAction(UIAlertAction(title: "Gallery", style: .default, handler: { _ in
            self.openGallery()
        }))
        
        alert.addAction(UIAlertAction.init(title: "Cancel", style: .cancel, handler: nil))
        
        self.present(alert, animated: true, completion: nil)
    }
    
    func openGallery()
    {
        if UIImagePickerController.isSourceTypeAvailable(UIImagePickerController.SourceType.photoLibrary){
            let imagePicker = UIImagePickerController()
            imagePicker.delegate = self
            imagePicker.allowsEditing = true
            imagePicker.sourceType = UIImagePickerController.SourceType.photoLibrary
            self.present(imagePicker, animated: true, completion: nil)
        }
        else
        {
            let alert  = UIAlertController(title: "Warning", message: "You don't have permission to access gallery.", preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
            self.present(alert, animated: true, completion: nil)
        }
    }
    
    func openCamera()
    {
        if UIImagePickerController.isSourceTypeAvailable(UIImagePickerController.SourceType.camera) {
            let imagePicker = UIImagePickerController()
            imagePicker.delegate = self
            imagePicker.sourceType = UIImagePickerController.SourceType.camera
            imagePicker.allowsEditing = false
            self.present(imagePicker, animated: true, completion: nil)
        }
        else
        {
            let alert  = UIAlertController(title: "Warning", message: "You don't have camera", preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
            self.present(alert, animated: true, completion: nil)
        }
    }
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
       
        if let pickedImage = info[.originalImage] as? UIImage
        {
            self.selectedImage = pickedImage
            self.images.append(selectedImage)
            if let pngdata = pickedImage.resize(maxWidth: 500, maxHeight: 250).pngData()
            {
                self.uploadPicture(imageData: pngdata)
            }
        }
        
        self.dismiss(animated: true, completion: nil)
        self.CollectionReload.reloadData()
    }
    
}


extension ProfileVC: UITableViewDelegate,UITableViewDataSource
{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrayTableData.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "TableCell", for: indexPath) as! TableCell
        cell.labelTitleNAme.text = arrayTableData[indexPath.row]
        let image = arrayImageIcon[indexPath.row]
        cell.ImageIcon.image = UIImage(named: image)
        return cell
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 60.0
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
    {
        if indexPath.row == 0
        {
            let navigate = self.storyboard?.instantiateViewController(withIdentifier: "ProfileInfoVC") as! ProfileInfoVC
            self.navigationController?.pushViewController(navigate, animated: true)
        }
        else if indexPath.row == 1
        {
            let navigate = self.storyboard?.instantiateViewController(withIdentifier: "AppSettingVC") as! AppSettingVC
            self.navigationController?.pushViewController(navigate, animated: true)
        }
        else if indexPath.row == 2
        {
            
        }
        else
        {
            self.displayShareSheet(shareContent: "Share", sourceRect: nil)
        }
    }
}
extension ProfileVC: UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout
{
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.arrayimages.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "ImageCell", for: indexPath) as! ImageCell
        let dict = self.arrayimages[indexPath.row]
        if ImageID.contains(dict.userImagesId!)
        {
            cell.imageCheck.image = UIImage(named: "check-mark")
        }
        else
        {
            cell.imageCheck.image = UIImage(named: "empty")
        }
        if let CellImage = dict.userImages, CellImage.count != 0
        {
            let url = WS_ProfileImageURL + CellImage
            print("profile image URL", url)
            cell.ImagePhoto.kf.setImage(with: URL(string: url), placeholder: UIImage(named: "default"), options: nil, progressBlock: nil, completionHandler: { (result) in
                switch result {
                case .success(let value):
                    print("Task done for: \(value.source.url?.absoluteString ?? "")")
                    
                case .failure(let error):
                    print("Job failed: \(error.localizedDescription)")
                }
            })
        }
         cell.btnRemovePhotos.tag = indexPath.row
        cell.btnRemovePhotos.addTarget(self, action: #selector(OnClickSelectImage), for: .touchUpInside)
       
        cell.contentView.shadowColorV = UIColor.black
        cell.contentView.shadowRadiusV = 2.0
        cell.contentView.shadowOpacityV = 0.2
        cell.ImagePhoto.layer.cornerRadius = 10.0
        return cell
    }
    func collectionView(_ collectionView: UICollectionView,layout collectionViewLayout: UICollectionViewLayout,sizeForItemAt indexPath: IndexPath) -> CGSize
    {
        if self.arrayimages.count == 1{
            return CGSize(width: self.CollectionReload.frame.size.width / 3, height: self.CollectionReload.frame.size.width / 3)
        }
        else if self.arrayimages.count > 1{
            return CGSize(width: self.CollectionReload.frame.size.width / 3, height: self.CollectionReload.frame.size.height / 2)
        }else{
            return CGSize.zero
        }
    }
    
    
}
extension ProfileVC
{
    func uploadPicture(imageData:Data)
    {
        
        APIClient1().API_UPLOAD(Url: ws_Image_Upload, Params: ["" : "" as AnyObject], fileName: "profile.png", fileData: imageData, mimeType: "image/png", Authentication: true, mapObject: ModelFileUploadBase.self, SuperVC: self, completionSuccess: { (modelResponse) in
            
            let resultModel = Mapper<ModelFileUploadBase>().map(JSONObject: modelResponse)
            DispatchQueue.main.asyncAfter(deadline: .now() + 0.5, execute: {
                
            })
            if(resultModel?.success == true), let userImg = resultModel?.imageName, userImg.count != 0
            {
                self.uploadedImageNames = userImg
                self.WS_Post_UploadedImage()
            }
            else
            {
                
            }
            
        }) { (failed) in
            
        }
    }
    func WS_Post_UploadedImage()
    {
        var parameters = [String : Any]()
        parameters = ["user_images" : uploadedImageNames]
        
        APIClient<ModelUserImage>().API_POST(Url: ws_Insert_Images, Params: parameters as [String:AnyObject], Authentication: true, Progress: true, Alert: true, Offline: false, SuperVC: self, completionSuccess: { (modelResponse) in
            UtilityClass.showhud()
            print("---------------------")
            print("Login Result:",modelResponse.toJSONString(prettyPrint: true)!)
            print("---------------------")
            
            if(modelResponse.success == true){
                UtilityClass.hidehud()
                self.WS_Get_UploadedImage()
            }
            else {
                // Toast(text: modelResponse.message ?? "").show()
                UtilityClass.hidehud()
            }
            
        }) { (failed) in
        }
    }
    
    func WS_Get_UploadedImage()
    {
        APIClient<ModelUserImage>().API_GET(Url: ws_Get_Images, Params: [:], Spinner: 1, Authentication: true, Progress: true, Alert: true, Offline: false, SuperVC: self, completionSuccess: { (modelResponse) in
            
            print("---------------------")
            print("Result:",modelResponse.toJSONString(prettyPrint: true)!)
            print("---------------------")
            self.arrayimages.removeAll()
            if(modelResponse.success == true)
            {
                if let result = modelResponse.userImageResult, result.count != 0
                {
                    for data in result
                    {
                        self.arrayimages.append(data)
                    }
                }
                
                if self.arrayimages.count == 1{
                    self.view_coll_height.constant = 138.0
                } else if self.arrayimages.count > 1{
                    self.view_coll_height.constant = 276.0
                }else{
                    self.view_coll_height.constant = 0.0
                }
                self.collectionMainView.layoutIfNeeded()
                self.CollectionReload.reloadData()
                
            }
            else
            {
                
            }
            
        }) { (failed) in
            
        }
    }
    
    func WS_Delete_Image()
    {
        var parameters = [String: Any]()
        var url = ws_Delete_Image
        let ImageJsonData = self.convertIntoJSONString(arrayObject: ImageID)
        parameters = ["image_id" : ImageJsonData]
        APIClient<ModelBaseSuccess>().API_DELETE(Url: url, Params: parameters as [String:AnyObject], Authentication: true, Progress: true, Alert: true, Offline: false, SuperVC: self, completionSuccess: { (modelResponse) in
            
            print("---------------------")
            print("Delete Result:",modelResponse.toJSONString(prettyPrint: true)!)
            print("---------------------")
        
            if(modelResponse.success == true) {
                DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) {
                    Toast(text: modelResponse.message ?? "").show()
                }
            }
            else {
                // Toast(text: modelResponse.message ?? "").show()
            }
            self.WS_Get_UploadedImage()
           
        }) { (failed) in}
    }
}

