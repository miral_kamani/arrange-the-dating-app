//
//  ProfileInfoVC.swift
//  Arrange
//
//  Created by Miral Kamani on 14/02/20.
//  Copyright © 2020 Miral Kamani. All rights reserved.
//

import UIKit

class ProfileInfoVC: UIViewController,UIImagePickerControllerDelegate,UINavigationControllerDelegate {
    
    //MARK:- Outlets
    @IBOutlet var MyDetailView: UIView!
    @IBOutlet var PersonalInfoView: UIView!
    @IBOutlet var imageProfile: RoundImageView!
    @IBOutlet var TxtViewHeadline: UITextView!
    @IBOutlet var TxtViewAboutMe: UITextView!
    @IBOutlet var imageAge: UIImageView!
    @IBOutlet var labelAge: UILabel!
    @IBOutlet var imageRaisedIn: UIImageView!
    @IBOutlet var imageOccupation: UIImageView!
    @IBOutlet var imageEducation: UIImageView!
    @IBOutlet var imageCommunity: UIImageView!
    @IBOutlet var imageReligion: UIImageView!
    @IBOutlet var imageHeight: UIImageView!
    @IBOutlet var labelAboutMeCount: UILabel!
    @IBOutlet var labelHeadlineCount: UILabel!
    @IBOutlet var ProfileView: UIViewX!
    @IBOutlet var txtHeight: UITextField!
    @IBOutlet var txtReligion: UITextField!
    @IBOutlet var txtCommunity: UITextField!
    @IBOutlet var txtOccupation: UITextField!
    @IBOutlet var txtEducation: UITextField!
    @IBOutlet var txtRaisedIn: UITextField!
    @IBOutlet var btnChangePhoto: UIButton!
    @IBOutlet var labelUserName: UILabel!
    @IBOutlet var btnUpdate: UIButton!
    
    @IBOutlet var labelHeight: UILabel!
    @IBOutlet var labelReligion: UILabel!
    @IBOutlet var labelCommunity: UILabel!
    @IBOutlet var labelOccupation: UILabel!
    @IBOutlet var labelraisedIn: UILabel!
    @IBOutlet var labelGender: UILabel!
    
    @IBOutlet var btnHeight: UIButton!
    @IBOutlet var btnReligion: UIButton!
    @IBOutlet var btnCommunity: UIButton!
    @IBOutlet var btnCareer: UIButton!
    @IBOutlet var btnRaisedIn: UIButton!
    @IBOutlet var btnGender: UIButton!
    
     //MARK:- Variable
    var tmpReligionID = Int()
    var tmpCommunityID = Int()
    var tmpCareerID = Int()
    var tmpRaisedInID = Int()
    
    let loginData = ApiUtillity.sharedInstance.getLoginModel()
    
    var imagePicker: UIImagePickerController!
    override func viewDidLoad() {
        super.viewDidLoad()
        self.btnChangePhoto.addTarget(self, action: #selector(OnClickChangePhoto), for: .touchUpInside)
        self.btnUpdate.addTarget(self, action: #selector(OnClickUpdate), for: .touchUpInside)
        // self.setUI()
        
        self.setDevice()
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    override func viewWillAppear(_ animated: Bool) {
        self.SetProfilenName()
        self.setUI()
        self.getdata()
    }
    @objc func OnClickUpdate(sender: UIButton)
    {
        self.WS_Edit_Profile()
    }
    func setUI()
    {
        self.TxtViewHeadline.text = "Dentist"
        self.TxtViewHeadline.textColor = UIColor.lightGray
        if UIDevice.current.userInterfaceIdiom == .phone
        {
            self.TxtViewHeadline.font = UIFont(name: "Avenir-Medium", size: 16)
            self.TxtViewAboutMe.font = UIFont(name: "Avenir-Medium", size: 16)
        }
        else
        {
            self.TxtViewHeadline.font = UIFont(name: "Avenir-Medium", size: 20)
            self.TxtViewAboutMe.font = UIFont(name: "Avenir-Medium", size: 20)
        }
        
        self.TxtViewAboutMe.text = "Write something about yourself and stand out from the crowd"
        self.TxtViewAboutMe.textColor = UIColor.lightGray
        
        
    }
    func SetProfilenName()
    {
        let loginData = ApiUtillity.sharedInstance.getLoginModel()
        if let uName = loginData?.facebookResult?.userProfileName, uName.count != 0
        {
            self.labelUserName.text = uName.capitalized
        }
        
        if let BookImage = loginData?.facebookResult?.userProfileImage//, BookImage.count != 0
        {
            let url = WS_ProfileImageURL + BookImage
            
            print("profile image URL", url)
            self.imageProfile.kf.setImage(with: URL(string: url), placeholder: UIImage(named: "default"), options: nil, progressBlock: nil, completionHandler: { (result) in
                switch result {
                case .success(let value):
                    print("Task done for: \(value.source.url?.absoluteString ?? "")")
                    
                case .failure(let error):
                    print("Job failed: \(error.localizedDescription)")
                }
            })
        }
    }
    
    func getdata()
    {
        if let UserAge = loginData?.facebookResult?.userProfileDob, UserAge != ""
        {
            self.labelAge.text = UserAge
        }
        
        if let UserHeight = loginData?.facebookResult?.userProfileHeight, UserHeight != ""
        {
            if UserHeightVC.Height.count != 0
            {
                self.labelHeight.text = UserHeightVC.Height
            }else{
                UserHeightVC.Height = UserHeight
                self.labelHeight.text = UserHeight
            }
        }
        
         if let UserGender = loginData?.facebookResult?.userProfileGender, UserGender != ""
         {
             if UserGenderVC.GenderName.count != 0
             {
                 self.labelGender.text = UserGenderVC.GenderName
             }else{
                 UserGenderVC.GenderName = UserGender
                 self.labelGender.text = UserGender
             }
         }
        if let UserReligion = loginData?.facebookResult?.religionName, UserReligion.count != 0
        {
            if UserReligionVC.ReligionName.count != 0{
                self.labelReligion.text = UserReligionVC.ReligionName
            }else{
                
                if let r_id = loginData?.facebookResult?.refReligionId{
                    UserReligionVC.ReligionID = r_id
                }
                
                UserReligionVC.ReligionName = UserReligion
                self.labelReligion.text = UserReligion
            }
        }
        
        if let UserCommunity = loginData?.facebookResult?.communityName, UserCommunity.count != 0
        {
            if UserCommunityVC.CommunityName.count != 0{
                self.labelCommunity.text = UserCommunityVC.CommunityName
            }else{
                
                if let c_id = loginData?.facebookResult?.refCommunityId{
                    UserCommunityVC.CommunityID = c_id
                }
                
                UserCommunityVC.CommunityName = UserCommunity
                self.labelCommunity.text = UserCommunity
            }
        }
        
        if let UserCareer = loginData?.facebookResult?.careerName, UserCareer.count != 0
        {
            if UserCareerVC.CareerName.count != 0{
                self.labelOccupation.text = UserCareerVC.CareerName
            }else{
                
                if let c_id = loginData?.facebookResult?.refCareerId{
                    UserCareerVC.CareerID = c_id
                }
                
                UserCareerVC.CareerName = UserCareer
                self.labelOccupation.text = UserCareer
            }
            
        }
        
        if let UserRaisedIn = loginData?.facebookResult?.raisedInName, UserRaisedIn.count != 0
        {
            if UserRaisedInVC.RaisedName.count != 0{
                self.labelraisedIn.text = UserRaisedInVC.RaisedName
            }else{
                
                if let r_id = loginData?.facebookResult?.refRaisedInId{
                    UserRaisedInVC.RaisedId = r_id
                }
                
                UserRaisedInVC.RaisedName = UserRaisedIn
                self.labelraisedIn.text = UserRaisedIn
            }
        }
        
        if let UserHeadline = loginData?.facebookResult?.userProfileHeadline, UserHeadline.count != 0
        {
            self.TxtViewHeadline.text = UserHeadline
        }
        if let UserAboutMe = loginData?.facebookResult?.userProfileAbout, UserAboutMe.count != 0
        {
            self.TxtViewAboutMe.text = UserAboutMe
        }
    }
    
    func setDevice()
    {
        if UIDevice.current.userInterfaceIdiom == .phone
        {
            self.ProfileView.layer.cornerRadius = 60
        }
        else
        {
            self.ProfileView.layer.cornerRadius = 75.0
            
        }
    }
    
    @IBAction func btnHeight(_ sender: Any)
    {
        let navigate = storyboard?.instantiateViewController(withIdentifier: "UserHeightVC") as! UserHeightVC
        navigate.isUpdate = false
        self.navigationController?.pushViewController(navigate, animated: true)
    }
    
    
    @IBAction func btnCommunity(_ sender: Any)
    {
        let navigate = storyboard?.instantiateViewController(withIdentifier: "UserCommunityVC") as! UserCommunityVC
        navigate.isUpdate = false
        self.navigationController?.pushViewController(navigate, animated: true)
    }
    
    @IBAction func btnCareer(_ sender: Any)
    {
        let navigate = storyboard?.instantiateViewController(withIdentifier: "UserCareerVC") as! UserCareerVC
        navigate.isUpdate = false
        self.navigationController?.pushViewController(navigate, animated: true)
    }
    
    @IBAction func btnRaisedIn(_ sender: Any)
    {
        let navigate = storyboard?.instantiateViewController(withIdentifier: "UserRaisedInVC") as! UserRaisedInVC
        navigate.isUpdate = false
        self.navigationController?.pushViewController(navigate, animated: true)
    }
    
    @IBAction func btnReligion(_ sender: Any)
    {
        let navigate = storyboard?.instantiateViewController(withIdentifier: "UserReligionVC") as! UserReligionVC
        navigate.isUpdate = false
        self.navigationController?.pushViewController(navigate, animated: true)
    }
    
    @IBAction func btnBack(_ sender: UIButton)
    {
        self.navigationController?.popViewController(animated: true)
    }
    
    @objc func OnClickChangePhoto(sender: UIButton)
    {
        self.showAlertForImagePicker()
    }
    
    func showAlertForImagePicker()
    {
        
        let alert = UIAlertController(title: "Choose Image", message: nil, preferredStyle: .actionSheet)
        alert.addAction(UIAlertAction(title: "Camera", style: .default, handler: { _ in
            self.openCamera()
        }))
        
        alert.addAction(UIAlertAction(title: "Gallery", style: .default, handler: { _ in
            self.openGallery()
        }))
        
        alert.addAction(UIAlertAction.init(title: "Cancel", style: .cancel, handler: nil))
        
        self.present(alert, animated: true, completion: nil)
    }
    func openGallery()
    {
        if UIImagePickerController.isSourceTypeAvailable(UIImagePickerController.SourceType.photoLibrary){
            let imagePicker = UIImagePickerController()
            imagePicker.delegate = self
            imagePicker.allowsEditing = true
            imagePicker.sourceType = UIImagePickerController.SourceType.photoLibrary
            self.present(imagePicker, animated: true, completion: nil)
        }
        else
        {
            let alert  = UIAlertController(title: "Warning", message: "You don't have permission to access gallery.", preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
            self.present(alert, animated: true, completion: nil)
        }
    }
    
    func openCamera()
    {
        if UIImagePickerController.isSourceTypeAvailable(UIImagePickerController.SourceType.camera) {
            let imagePicker = UIImagePickerController()
            imagePicker.delegate = self
            imagePicker.sourceType = UIImagePickerController.SourceType.camera
            imagePicker.allowsEditing = false
            self.present(imagePicker, animated: true, completion: nil)
        }
        else
        {
            let alert  = UIAlertController(title: "Warning", message: "You don't have camera", preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
            self.present(alert, animated: true, completion: nil)
        }
    }
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        if let pickedImage = info[.originalImage] as? UIImage {
            // imageViewPic.contentMode = .scaleToFill
            self.imageProfile.image = pickedImage
        }
        picker.dismiss(animated: true, completion: nil)
    }
    
}
extension ProfileInfoVC: UITextViewDelegate
{
    func textViewDidBeginEditing(_ textView: UITextView) {
        if textView == TxtViewHeadline
        {
            if textView.textColor == UIColor.lightGray {
                textView.text = nil
                textView.textColor = UIColor.black
            }
        }
        else
        {
            if textView.textColor == UIColor.lightGray {
                textView.text = nil
                textView.textColor = UIColor.black
            }
        }
        
    }
    func textViewDidEndEditing(_ textView: UITextView) {
        if textView == TxtViewHeadline
        {
            if textView.text.isEmpty {
                textView.text = "Dentist"
                textView.textColor = UIColor.lightGray
            }
        }
        else
        {
            if textView.text.isEmpty {
                textView.text = "Write something about yourself and stand out from the crowd"
                textView.textColor = UIColor.lightGray
            }
        }
    }
    
    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        
        if textView == TxtViewHeadline
        {
            let allowedChars = 50
            let charsInTextView = -TxtViewHeadline.text.count
            let remainingChars = allowedChars + charsInTextView
            
            if (text == "\n") {
                let remainingChars = 50 - (TxtViewHeadline.text.count * 2 )
                
                labelHeadlineCount.text = "\(String(remainingChars))" + "/" + "\(50)"
            }
            
            if (text != "\n"){
                labelHeadlineCount.text = "\(String(remainingChars))" + "/" + "\(50)"
            }
            //                 if TxtViewHeadline.text.count >=50
            //                 {
            //
            //                }
            return true
        }
        else
        {
            let allowedChars = 280
            let charsInTextView = -TxtViewAboutMe.text.count
            let remainingChars = allowedChars + charsInTextView
            
            if (text == "\n") {
                let remainingChars = 280 - (TxtViewHeadline.text.count * 2 )
                
                labelAboutMeCount.text = "\(String(remainingChars))" + "/" + "\(280)"
            }
            
            if (text != "\n"){
                labelAboutMeCount.text = "\(String(remainingChars))" + "/" + "\(280)"
            }
            
            return true
        }
        
    }
}

extension ProfileInfoVC
{
    func WS_Edit_Profile()
    {
        var parameters = [String: Any]()
        parameters = [
                      "user_about" : TxtViewAboutMe.text!,
                      "user_headline" : TxtViewHeadline.text!,
                      "religion" : UserReligionVC.ReligionID,
                      "community" : UserCommunityVC.CommunityID,
                      "user_education" : txtEducation.text!,
                      "raised_in" : UserRaisedInVC.RaisedId,
                      "user_height" : UserHeightVC.Height]
        
        print("parameters:::::::::::", parameters)
        
        APIClient<ModelSocialLoginBase>().API_PUT(Url: ws_Edit_Profile, Params: parameters as [String : AnyObject], Authentication: false, Progress: true, Alert: true, Offline: false, SuperVC: self, completionSuccess: { (modelResponse) in
            
            if(modelResponse.success == true) {
                
                ApiUtillity.sharedInstance.setLoginModel(modelResponse: modelResponse)
                self.navigationController?.popViewController(animated: true)
            }
            else {
                
            }
        }) { (error) in}
        
        self.navigationController?.popViewController(animated: true)
    }
}
