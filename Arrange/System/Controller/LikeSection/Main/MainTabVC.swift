//
//  MainTabVC.swift
//  Arrange
//
//  Created by Miral Kamani on 18/02/20.
//  Copyright © 2020 Miral Kamani. All rights reserved.
//

import UIKit
import TTSegmentedControl
import Toaster

class MainTabVC: UIViewController {
    
    //MARK:- Outlets
    @IBOutlet var tblReceiveReload: UICollectionView!
    @IBOutlet var tblGivenReload: UICollectionView!
    @IBOutlet var InnerSegment: UISegmentedControl!
    @IBOutlet var tblReload: UITableView!
    @IBOutlet var segmentControl: TTSegmentedControl!
    @IBOutlet var ChatView: UIViewX!
    @IBOutlet var LikeView: UIViewX!
    @IBOutlet var GivenView: UIView!
    @IBOutlet var ReceivedView: UIView!
    
    //MARK:- Variable
    var arrayGivenLikes = [ModelGivenLikeResult]()
    var arrayReceivedLike = [ModelReceivedLikeResult]()
    var arrayChatListing = [ModelChatResult]()
    static var ChatRoomID = Int()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        tblReload.tableFooterView = UIView()
        self.ReceivedView.isHidden = false
        self.GivenView.isHidden = true
        self.RegisterTableXIB()
        self.RegisterCollectionXIB()
        self.Segment()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.WS_Get_ChatRoom()
        
    }
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    @IBAction func btnBack(_ sender: UIButton)
    {
        self.navigationController?.popViewController(animated: true)
    }
    func Segment()
    {
        segmentControl.allowChangeThumbWidth = false
        segmentControl.itemTitles = ["MESSAGES","LIKES"]
        segmentControl.didSelectItemWith = { (index, title) -> () in
            print("Selected item \(index)")
            if index == 0
            {
                self.ChatView.isHidden = false
                self.LikeView.isHidden = true
            }
            else
            {
                self.ChatView.isHidden = true
                self.LikeView.isHidden = false
            }
        }
        segmentControl.defaultTextColor = UIColor(red: 67, green: 66, blue: 72)
        if UIDevice.current.userInterfaceIdiom == .phone
        {
            segmentControl.selectedTextFont = UIFont(name: "Avenir-Black", size: 12)!
            segmentControl.defaultTextFont = UIFont(name: "Avenir-Black", size: 12)!
        }
        else
        {
            segmentControl.selectedTextFont = UIFont(name: "Avenir-Black", size: 16)!
            segmentControl.defaultTextFont = UIFont(name: "Avenir-Black", size: 16)!
        }
        segmentControl.selectedTextColor = UIColor.white
        segmentControl.thumbGradientColors = [UIColor(red: 192, green: 152, blue: 255), UIColor(red: 140, green: 94, blue: 255)]
        segmentControl.useShadow = true
        segmentControl.layoutSubviews()
    }
    
    func convertUTCToLocal(timeString: String) -> String? {
           
           // let c_UTCTIme = "2020-01-30 6:45"
           
           let dateFormatter = DateFormatter()
           dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
           
           dateFormatter.timeZone = TimeZone.init(abbreviation: "UTC")
           let timeUTC = dateFormatter.date(from: timeString)
           
           if timeUTC != nil {
               dateFormatter.timeZone = NSTimeZone.local
               
               let localTime = dateFormatter.string(from: timeUTC!)
               return localTime
           }
           
           return nil
       }
       func changeDateForamte(_ date: String, conVertFormate:String, currentDateFormate:String) -> String
       {
           let dateFormatter = DateFormatter()
           dateFormatter.dateFormat = currentDateFormate
           let date = dateFormatter.date(from: date)
           dateFormatter.dateFormat = conVertFormate
           return  dateFormatter.string(from: date!)
           
       }
    
    @IBAction func btnMatch(_ sender: UIButton)
    {
        let navigate = self.storyboard?.instantiateViewController(withIdentifier: "MatchesVC") as! MatchesVC
        self.navigationController?.pushViewController(navigate, animated: true)
    }
    @IBAction func InnerSegment(_ sender: UISegmentedControl)
    {
        self.InnerSegment.changeUnderlinePosition()
        print(InnerSegment.selectedSegmentIndex)
        if InnerSegment.selectedSegmentIndex == 0
        {
            self.WS_Get_Received_Likes()
            self.ReceivedView.isHidden = false
            self.GivenView.isHidden = true
            self.tblReceiveReload.reloadData()
            
        }
        else
        {
            self.WS_Get_Given_Likes()
            self.ReceivedView.isHidden = true
            self.GivenView.isHidden = false
            self.tblGivenReload.reloadData()
        }
    }
    
}

extension MainTabVC: UITableViewDataSource,UITableViewDelegate
{
    func RegisterTableXIB()
    {
        self.tblReload.register(UINib(nibName: "ChatCell", bundle: nil), forCellReuseIdentifier: "ChatCell")
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return getRowsforList(tableView, forArrayCount: self.arrayChatListing.count, withPlaceHolder: "There's no messages to show", withColor: CharcoalGrey, imageName: "noChat", imageSize: CGSize(width: 25.0, height: 25.0))
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        let cell = tableView.dequeueReusableCell(withIdentifier: "ChatCell", for: indexPath) as! ChatCell
        let dict = arrayChatListing[indexPath.row]
        if let data = dict.userDetails
        {
            
            if let uname = data.userProfileName, uname.count != 0
            {
                if let uage = data.userProfileAge, uage != 0
                {
                    cell.labelNameAge.text = "\(uname)" + "," + "\(uage)"
                }
            }
            
            if let uoccupation = data.careerName, uoccupation.count != 0
            {
                cell.labelOccupation.text = uoccupation
            }
            
            if let uprofile = data.userProfileImage, uprofile.count != 0
            {
                let url = WS_ProfileImageURL + uprofile
                print("profile image URL", url)
                cell.imageProfile.kf.setImage(with: URL(string: url), placeholder: UIImage(named: "default"), options: nil, progressBlock: nil, completionHandler: { (result) in
                    switch result {
                    case .success(let value):
                        print("Task done for: \(value.source.url?.absoluteString ?? "")")
                        
                    case .failure(let error):
                        print("Job failed: \(error.localizedDescription)")
                    }
                })
            }
            
            if let chatTime = dict.chatRoomDatetime, chatTime.count != 0
            {
                let time = self.convertUTCToLocal(timeString: dict.chatRoomDatetime!)
                cell.labelDate.text = self.changeDateForamte(time!, conVertFormate: "h:mm a", currentDateFormate: "yyyy-MM-dd HH:mm:ss")
            }
            
            if let isAccept = dict.isAccepted
            {
                if isAccept == 0
                {
                    ChatVC.isAccept = 0
                }
                else
                {
                    ChatVC.isAccept = 1
                }
            }
            
        }
        return cell
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if UIDevice.current.userInterfaceIdiom == .phone
        {
            return 104.0
        }
        else
        {
            return 140.0
        }
        
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
    {
        let dict = arrayChatListing[indexPath.row]
        let navigate = self.storyboard?.instantiateViewController(withIdentifier: "ChatVC") as! ChatVC
        navigate.ChatRoomID = dict.chatRoomId!
        navigate.UnmatchID = dict.receiverRefId!
        navigate.ReceiverID = (dict.userDetails?.refUserId)!
        navigate.Username = (dict.userDetails?.userProfileName)!
        navigate.ProductImage = (dict.userDetails!.userProfileImage)!
        self.navigationController?.pushViewController(navigate, animated: true)
    }
}


extension MainTabVC: UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout
{
    func RegisterCollectionXIB()
    {
        self.tblReceiveReload.register(UINib(nibName: "LikeCell", bundle: nil), forCellWithReuseIdentifier: "LikeCell")
        self.tblGivenReload.register(UINib(nibName: "LikeCell", bundle: nil), forCellWithReuseIdentifier: "LikeCell")
    }
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int
    {
        if collectionView == tblReceiveReload
        {
            return getRowsforList(collectionView, forArrayCount: self.arrayReceivedLike.count, withPlaceHolder: "No likes fonud!", withColor: CharcoalGrey, imageName: noLike, imageSize: imageSize)
        }
        else
        {
            return getRowsforList(collectionView, forArrayCount: self.arrayGivenLikes.count, withPlaceHolder: "No likes fonud!", withColor: CharcoalGrey, imageName: noLike, imageSize: imageSize)
        }
        
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        if collectionView == tblReceiveReload
        {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "LikeCell", for: indexPath) as! LikeCell
            let dict = arrayReceivedLike[indexPath.row]
            if let data = dict.userDetails
            {
                if let name = data.userProfileName, name.count != 0
                {
                    cell.labelName.text = name
                }
                
                if let age = data.userProfileAge, age != 0
                {
                    cell.labelAge.text = "\(age)"
                }
                
                if let image = data.userProfileImage, image.count != 0
                {
                    let url = WS_ProfileImageURL + image
                    print("profile image URL", url)
                    cell.imageProfile.kf.setImage(with: URL(string: url), placeholder: UIImage(named: "1-1"), options: nil, progressBlock: nil, completionHandler: { (result) in
                        switch result {
                        case .success(let value):
                            print("Task done for: \(value.source.url?.absoluteString ?? "")")
                            
                        case .failure(let error):
                            print("Job failed: \(error.localizedDescription)")
                        }
                    })
                }
                
                if let occupation = data.careerName, occupation.count != 0
                {
                    cell.labelOccupation.text = occupation
                }
                
                if let distance = data.distance, distance != 0
                {
                    cell.labelDistance.text = "\(distance)"
                }
    
            }
            
            return cell
        }
        else
        {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "LikeCell", for: indexPath) as! LikeCell
            let dict = arrayGivenLikes[indexPath.row]
            if let data = dict.userDetails
            {
                if let name = data.userProfileName, name.count != 0
                {
                    cell.labelName.text = name
                }
                
                if let age = data.userProfileAge, age != 0
                {
                    cell.labelAge.text = "\(age)"
                }
                
                if let image = data.userProfileImage, image.count != 0
                {
                    let url = WS_ProfileImageURL + image
                    print("profile image URL", url)
                    cell.imageProfile.kf.setImage(with: URL(string: url), placeholder: UIImage(named: "2-1"), options: nil, progressBlock: nil, completionHandler: { (result) in
                        switch result {
                        case .success(let value):
                            print("Task done for: \(value.source.url?.absoluteString ?? "")")
                            
                        case .failure(let error):
                            print("Job failed: \(error.localizedDescription)")
                        }
                    })
                }
                
                if let occupation = data.careerName, occupation.count != 0
                {
                    cell.labelOccupation.text = occupation
                }
                
                if let distance = data.distance, distance != 0
                {
                    cell.labelDistance.text = "\(distance)"
                }
                
            }
           
            return cell
        }
        
    }
    func collectionView(_ collectionView: UICollectionView,layout collectionViewLayout: UICollectionViewLayout,sizeForItemAt indexPath: IndexPath) -> CGSize
    {
        if collectionView == tblReceiveReload
        {
            let padding: CGFloat = 10
            let collectionViewSize = tblReceiveReload.frame.size.width - padding
            return CGSize(width: collectionViewSize/2, height: collectionViewSize/2)
        }
        else
        {
            let padding: CGFloat = 10
            let collectionViewSize = tblGivenReload.frame.size.width - padding
            return CGSize(width: collectionViewSize/2, height: collectionViewSize/2)
        }
        
    }
    
    
}
extension MainTabVC
{
    func WS_Get_Given_Likes()
    {
        APIClient<ModelGivenLikeBase>().API_GET(Url: ws_Get_Given_Likes, Params: [:], Spinner: 1, Authentication: true, Progress: true, Alert: true, Offline: false, SuperVC: self, completionSuccess: { (modelResponse) in
            
            print("---------------------")
            print("Result:",modelResponse.toJSONString(prettyPrint: true)!)
            print("---------------------")
            self.arrayGivenLikes.removeAll()
            if(modelResponse.success == true)
            {
                if let result = modelResponse.givenLikeResult, result.count != 0
                {
                    for data in result
                    {
                        self.arrayGivenLikes.append(data)
                    }
                }
            }
            else
            {
                 if modelResponse.message == "Authentication API is invalid!" {
                     let vc = self.storyboard?.instantiateViewController(withIdentifier: "LogoutVc") as! LogoutVc
                     self.navigationController?.pushViewController(vc, animated: false)
                 }
                 else {
                     UtilityClass.hidehud()
                     Toast(text: modelResponse.message ?? "").show()
                 }
            }
            self.tblGivenReload.tag = 1
            self.tblGivenReload.reloadData()
            
        }) { (failed) in
            
        }
    }
    
    func WS_Get_Received_Likes()
    {
        APIClient<ModelReceivedLikeBase>().API_GET(Url: ws_Get_Received_Likes, Params: [:], Spinner: 1, Authentication: true, Progress: true, Alert: true, Offline: false, SuperVC: self, completionSuccess: { (modelResponse) in
            
            print("---------------------")
            print("Result:",modelResponse.toJSONString(prettyPrint: true)!)
            print("---------------------")
            self.arrayReceivedLike.removeAll()
            if(modelResponse.success == true)
            {
                if let result = modelResponse.receivedLikeResult, result.count != 0
                {
                    for data in result
                    {
                        self.arrayReceivedLike.append(data)
                    }
                }
            }
            else
            {
                 if modelResponse.message == "Authentication API is invalid!" {
                     let vc = self.storyboard?.instantiateViewController(withIdentifier: "LogoutVc") as! LogoutVc
                     self.navigationController?.pushViewController(vc, animated: false)
                 }
                 else {
                     UtilityClass.hidehud()
                     Toast(text: modelResponse.message ?? "").show()
                 }
            }
            self.tblReceiveReload.tag = 1
            self.tblReceiveReload.reloadData()
            
        }) { (failed) in
            
        }
    }
    
    func WS_Get_ChatRoom()
    {
        APIClient<ModelGetChatRoom>().API_GET(Url: ws_Get_Chatroom, Params: [:], Spinner: 1, Authentication: true, Progress: true, Alert: true, Offline: false, SuperVC: self, completionSuccess: { (modelResponse) in
            
            print("---------------------")
            print("Result:",modelResponse.toJSONString(prettyPrint: true)!)
            print("---------------------")
            self.arrayChatListing.removeAll()
            if(modelResponse.success == true)
            {
                if let result = modelResponse.chatResult, result.count != 0
                {
                    for data in result
                    {
                        self.arrayChatListing.append(data)
                    }
                }
            }
            else
            {
                 if modelResponse.message == "Authentication API is invalid!" {
                     let vc = self.storyboard?.instantiateViewController(withIdentifier: "LogoutVc") as! LogoutVc
                     self.navigationController?.pushViewController(vc, animated: false)
                 }
                 else {
                     UtilityClass.hidehud()
                     Toast(text: modelResponse.message ?? "").show()
                 }
            }
            self.tblReload.tag = 1
            self.tblReload.reloadData()
            
        }) { (failed) in
            
        }
    }
}

