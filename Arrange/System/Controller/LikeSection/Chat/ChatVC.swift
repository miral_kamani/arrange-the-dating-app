//
//  ChatVC.swift
//  CurrencySwap
//
//  Created by SocialInfotech 13 on 8/24/18.
//  Copyright © 2018 SocialInfotech 13. All rights reserved.
//

import UIKit

import ObjectMapper
//import SVProgressHUD
import IQKeyboardManagerSwift
import Alamofire
import Toaster


class ChatVC: UIViewController, UITableViewDataSource, UITableViewDelegate, UITextViewDelegate, UIImagePickerControllerDelegate, UINavigationControllerDelegate, UIActionSheetDelegate {
    
    //MARK:- Outlets
    
    @IBOutlet weak var headerView: UIView!
    @IBOutlet weak var statusBar_view: UIView!
    @IBOutlet weak var tableview_chat: UITableView!
    
    @IBOutlet var MainProfileheaderImage: RoundImageView!
    @IBOutlet var labelUserName: UILabel!
    @IBOutlet weak var img_send: UIImageView!
    @IBOutlet weak var btn_send: UIButton!
    
    @IBOutlet weak var view_chatview: UIView!
    @IBOutlet var textView_message: UITextView!
    
    @IBOutlet var layoutConstraintChatView_height: NSLayoutConstraint!
    @IBOutlet var layoutConstraintChatView_bottom: NSLayoutConstraint!
    
    @IBOutlet var RequestView: UIViewX!
    @IBOutlet var btnAccept: UIButton!
    @IBOutlet var btnDecline: UIButton!
    //MARK:- Variable
    
    var picker: UIImagePickerController = UIImagePickerController()
    var isScrollToBottom = Bool()
    lazy var refresher = UIRefreshControl()
    var isback = Bool()
    var User_image = UIImage()
    var lastContentOffset: CGFloat = 0
    var newMessagesLoadTimer = Timer()
    var Producttitle = String()
    var ProductImage = String()
    var ProductPrice = String()
    var ProductId = Int()
    var Username = String()
    var UserImage = String()
    var chatMsgType = String()
    var ChatRoomId = Int()
    var adminId = Int()
    var offset = Int()
    var userRefID = Int()
    static var ReceiverID = Int()
    var oldMessageArr = [ModelChatMessages]()
    var getNewMessageArr = [ModelChatMessages]()
    var MessageType = String()
    var SendImageName = String()
    var ChatRoomID = Int()
    var ReceiverID = Int()
    var UnmatchID = Int()
    static var isAccept = Int()
    static var isMsgSend = Bool()
    var oldMessageIDsArr = [Int]()
    var newMessageIDsArr = [Int]()
    var isNavigateToChatListingVC = Bool()
    override func viewDidLoad() {
        
        super.viewDidLoad()
        self.WS_GetOldMessagesAPI()
        img_send.image = img_send.image!.withRenderingMode(.alwaysTemplate)
        img_send.tintColor = UIColor.lightGray
        btn_send.isUserInteractionEnabled = false
        
        self.btnAccept.addTarget(self, action: #selector(OnClickAccept), for: .touchUpInside)
        self.btnDecline.addTarget(self, action: #selector(OnClickDecline), for: .touchUpInside)
        self.tableview_chat.register(UINib(nibName: "LeftTextChatTableViewCell", bundle: nil), forCellReuseIdentifier: "LeftTextChatTableViewCell")
        self.tableview_chat.register(UINib(nibName: "RightTextChatTableViewCell", bundle: nil), forCellReuseIdentifier: "RightTextChatTableViewCell")
        self.tableview_chat.register(UINib(nibName: "RightImageChatTableViewCell", bundle: nil), forCellReuseIdentifier: "RightImageChatTableViewCell")
        self.tableview_chat.register(UINib(nibName: "LeftImageChatTableViewCell", bundle: nil), forCellReuseIdentifier: "LeftImageChatTableViewCell")
        
        self.isScrollToBottom = false
        
        self.prepareChatView()
        self.reloadTableView()
        self.hideKeyboardWhenTappedAroundTableview()
        self.addKeyboardNotifier()
        //  self.changeStatusBarBGColor()
    }
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    func convertUTCToLocal(timeString: String) -> String? {
        
        // let c_UTCTIme = "2020-01-30 6:45"
        
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
        
        dateFormatter.timeZone = TimeZone.init(abbreviation: "UTC")
        let timeUTC = dateFormatter.date(from: timeString)
        
        if timeUTC != nil {
            dateFormatter.timeZone = NSTimeZone.local
            
            let localTime = dateFormatter.string(from: timeUTC!)
            return localTime
        }
        
        return nil
    }
    func changeDateForamte(_ date: String, conVertFormate:String, currentDateFormate:String) -> String
    {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = currentDateFormate
        let date = dateFormatter.date(from: date)
        dateFormatter.dateFormat = conVertFormate
        return  dateFormatter.string(from: date!)
        
    }
    
    func setupRefreshControl()
    {
        refresher.tintColor = CharcoalGrey
        refresher.addTarget(self, action: #selector(refreshData), for: .valueChanged)
        self.tableview_chat.addSubview(refresher)
    }
    
    func getAdminData()
    {
        self.labelUserName.text = Username
        if ProductImage.count != 0
        {
            let url = WS_ProfileImageURL + ProductImage
            MainProfileheaderImage.kf.indicatorType = .activity
            MainProfileheaderImage.kf.setImage(with: URL(string: url), placeholder: UIImage(named: "user_setting"), options: [.transition(.fade(0.2))])
        }
        
    }
    @IBAction func btnMore(_ sender: UIButton)
    {
        let navigate = self.storyboard?.instantiateViewController(withIdentifier: "UnmatchVC") as! UnmatchVC
        navigate.UnmatchID = self.UnmatchID
        navigate.chatRoomId = self.ChatRoomID
        self.navigationController?.pushViewController(navigate, animated: true)
    }
    @objc func refreshData(sender: Any)
    {
         self.WS_GetOldMessagesAPI()
    }
    
    @objc func timerAction()
    {
        self.newMessagesLoadTimer.invalidate()
       // self.WS_getNewMessage()
    }
    
    @objc func OnClickAccept(sender: UIButton)
    {
        self.WS_isAccept()
        ChatVC.self.isAccept = 1
    }
    
    @objc func OnClickDecline(sender: UIButton)
    {
        
    }
    override func viewWillAppear(_ animated: Bool) {
        self.getAdminData()
        if ChatVC.self.isAccept ==  1
        {
            self.RequestView.isHidden = true
        }
        else if ChatVC.self.isAccept == 2
        {
             self.RequestView.isHidden = false
        }
        else
        {
            self.RequestView.isHidden = false
        }
        if self.isback == true
        {
            // call image upload funcation.
        }
        self.WS_GetOldMessagesAPI()
        IQKeyboardManager.shared.enable = false
        IQKeyboardManager.shared.enableAutoToolbar = false
        
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        IQKeyboardManager.shared.enable = true
        IQKeyboardManager.shared.enableAutoToolbar = true
    }
    
    override func viewDidAppear(_ animated: Bool) {
        headerView.dropShadow(color: UIColor(red: 51.0/255.0, green: 51.0/255.0, blue: 51.0/255.0, alpha: 0.88), opacity: 1.0, offSet: CGSize(width: 0, height: 5), radius: 3, scale: true)
    }
    
    // MARK:- Custom Funcation
    
    
    func prepareChatView() {
        
        textView_message.tintColorDidChange()
        textView_message.layer.cornerRadius = 6.0
        textView_message.backgroundColor = UIColor(red: Int(192/255.0), green: Int(162/255.0), blue: Int(255/255.0), a: 1)
        textView_message.textContainerInset = UIEdgeInsets(top: 10, left: 10, bottom: 10, right: 10)
        textView_message.delegate = self
    }
    
    func inputPanelUpdate() {
        
        var bottomSafe = CGFloat()
        
        let inputViewHeightMin: CGFloat            = 44.0
        let inputTextHeightMin: CGFloat            = 30.0
        let inputTextHeightMax: CGFloat            = 110.0
        
        let heightView: CGFloat = view.frame.size.height
        
        if #available(iOS 11.0, *) {
            bottomSafe = view.safeAreaInsets.bottom
        } else {
            // Fallback on earlier versions
        }
        
        let widthText: CGFloat = self.textView_message.frame.size.width
        var heightText: CGFloat
        let sizeText = self.textView_message.sizeThatFits(CGSize(width: widthText, height: CGFloat.greatestFiniteMagnitude))
        
        heightText = CGFloat.maximum(inputTextHeightMin, sizeText.height)
        heightText = CGFloat.minimum(inputTextHeightMax, heightText)
        
        let heightInput: CGFloat = heightText + (inputViewHeightMin - inputTextHeightMin)
        
        print(heightView - bottomSafe - heightInput)
        self.layoutConstraintChatView_height.constant = heightInput>58 ? heightInput : 58
        
        self.view_chatview.layoutIfNeeded()
        
        var frameTextInput: CGRect = self.textView_message.frame
        frameTextInput.size.height = heightText
        self.textView_message.frame = frameTextInput
        
        let offset = CGPoint(x: 0, y: sizeText.height - heightText)
        self.textView_message.setContentOffset(offset, animated: false)
    }
    
    
    
    // MARK: - UITextViewDelegate
    //---------------------------------------------------------------------------------------------------------------------------------------------
    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        return true
    }
    
    //---------------------------------------------------------------------------------------------------------------------------------------------
    func textViewDidChange(_ textView: UITextView) {
        
        inputPanelUpdate()
        
        let message = textView.text.trimmed
        
        if message.count != 0
        {
            img_send.image = img_send.image!.withRenderingMode(.alwaysTemplate)
            img_send.tintColor = UIColor.black
            btn_send.isUserInteractionEnabled = true
        }
        else
        {
            img_send.image = img_send.image!.withRenderingMode(.alwaysTemplate)
            img_send.tintColor = UIColor.lightGray
            btn_send.isUserInteractionEnabled = false
        }
    }
    
    
    // MARK:- Tableview Method
    
    func reloadTableView() {
        
        self.tableview_chat.reloadData()
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
 
        return getRowsforList(tableView, forArrayCount: self.oldMessageArr.count, withPlaceHolder: "There's no messages to show", withColor: CharcoalGrey, imageName: noChat, imageSize: imageSize)
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        var cell = UITableViewCell()
               let dict = oldMessageArr[indexPath.row]
               
               let loginData = ApiUtillity.sharedInstance.getLoginModel()
               let uid = loginData?.facebookResult?.userId
               
               if dict.msgSenderId == uid
               {
                   if dict.chatMsgType == "Text"
                   {
                       let rightTextCell = tableView.dequeueReusableCell(withIdentifier: "RightTextChatTableViewCell", for: indexPath) as! ChatTableViewCell
                       rightTextCell.MSGType = "Text"
                       if let msg = dict.chatMsgText, let time = dict.chatMsgDatetime, time.count != 0, msg.count != 0
                       {
                           rightTextCell.lbl_chatMessageText.text = msg.trimmed
                           
                           let time = self.convertUTCToLocal(timeString: time)
                           rightTextCell.lbl_time.text = self.changeDateForamte(time!, conVertFormate: "MM-dd hh:mm a", currentDateFormate: "yyyy-MM-dd HH:mm:ss ")
                       }
                       else
                       {
                          rightTextCell.lbl_chatMessageText.text = ""
                       }
                       
                       if let tmpImgURL = dict.userProfileImage, tmpImgURL.count != 0
                       {
                           let url = WS_ProfileImageURL + tmpImgURL
                           rightTextCell.imageview_chatProfile.kf.indicatorType = .activity
                           rightTextCell.imageview_chatProfile.kf.setImage(with: URL(string: url), placeholder: UIImage(named: "user_setting"), options: [.transition(.fade(0.2))])
                       }
                       rightTextCell.updateCellTextInfo(positionRight: true)
                       cell = rightTextCell
                   }
                   else
                   {
                       let rightImgCell = tableView.dequeueReusableCell(withIdentifier: "RightImageChatTableViewCell", for: indexPath) as! ChatTableViewCell
                       rightImgCell.MSGType = "File"
                       if let time = dict.chatMsgDatetime, time.count != 0
                       {
                           let time = self.convertUTCToLocal(timeString: time)
                           rightImgCell.lbl_time.text = self.changeDateForamte(time!, conVertFormate: "MM-dd hh:mm a", currentDateFormate: "yyyy-MM-dd HH:mm:ss ")
                       }
                       
                       if let tmpImgURL = dict.userProfileImage, tmpImgURL.count != 0
                       {
                           let img = WS_ProfileImageURL + tmpImgURL
                           let url = URL(string: img)
                           rightImgCell.imageview_chatProfile.kf.indicatorType = .activity
                           rightImgCell.imageview_chatProfile.kf.setImage(with: url, placeholder: UIImage(named: "user_setting"), options: [.transition(.fade(0.2))])
                       }
                       
                       if let fileName = dict.chatMsgText, fileName.count != 0
                       {
                           self.SendImageName = fileName
                           let img = WS_ProfileImageURL + fileName
                           let url = URL(string: img)
                           rightImgCell.img_sendUser.kf.indicatorType = .activity
                           rightImgCell.img_sendUser.kf.setImage(with: url, placeholder: UIImage(named: "user_setting"), options: [.transition(.fade(0.2))])
                       }
                       
                       rightImgCell.updateCellTextInfo(positionRight: false)
                       rightImgCell.btn_showImage.tag = indexPath.row
                      rightImgCell.btn_showImage.addTarget(self, action: #selector(self.clickOnImage), for: .touchUpInside)
                       
                       cell = rightImgCell
                   }
               }
               else
               {
                   if dict.chatMsgType == "Text"
                   {
                       let leftTextCell = tableView.dequeueReusableCell(withIdentifier: "LeftTextChatTableViewCell", for: indexPath) as! ChatTableViewCell
                       leftTextCell.MSGType = "Text"
                       if let msg = dict.chatMsgText, let time = dict.chatMsgDatetime, time.count != 0, msg.count != 0
                       {
                           leftTextCell.lbl_chatMessageText.text = msg.trimmed
                           let time = self.convertUTCToLocal(timeString: time)
                           leftTextCell.lbl_time.text = self.changeDateForamte(time!, conVertFormate: "MM-dd hh:mm a", currentDateFormate: "yyyy-MM-dd HH:mm:ss ")
                       }
                       
                       if let tmpImgURL = dict.userProfileImage, tmpImgURL.count != 0
                       {
                           let img = WS_ProfileImageURL + tmpImgURL
                           let url = URL(string: img)
                           leftTextCell.imageview_chatProfile.kf.indicatorType = .activity
                           leftTextCell.imageview_chatProfile.kf.setImage(with: url, placeholder: UIImage(named: "user_setting"), options: [.transition(.fade(0.2))])
                       }
                       leftTextCell.updateCellTextInfo(positionRight: false)
                       cell = leftTextCell
                   }
                   else {
                       let leftImgCell = tableView.dequeueReusableCell(withIdentifier: "LeftImageChatTableViewCell", for: indexPath) as! ChatTableViewCell
                       leftImgCell.MSGType = "File"
         
                       if let time = dict.chatMsgDatetime, time.count != 0 {
                           let time = self.convertUTCToLocal(timeString: time)
                           leftImgCell.lbl_time.text = self.changeDateForamte(time!, conVertFormate: "MM-dd hh:mm a", currentDateFormate: "yyyy-MM-dd HH:mm:ss ")
                       }
                       
                       if let tmpImgURL = dict.userProfileImage, tmpImgURL.count != 0 {
                           let img = WS_ProfileImageURL + tmpImgURL
                           let url = URL(string: img)
                           leftImgCell.imageview_chatProfile.kf.indicatorType = .activity
                           leftImgCell.imageview_chatProfile.kf.setImage(with: url, placeholder: UIImage(named: "user_setting"), options: [.transition(.fade(0.2))])
                       }
                       
                       if let fileName = dict.chatMsgText, fileName.count != 0
                       {
                           self.SendImageName = fileName
                           let img = WS_ProfileImageURL + fileName
                           let url = URL(string: img)
                           leftImgCell.img_sendUser.kf.indicatorType = .activity
                           leftImgCell.img_sendUser.kf.setImage(with: url, placeholder: UIImage(named: "user_setting"), options: [.transition(.fade(0.2))])
                       }
                       
                       leftImgCell.updateCellTextInfo(positionRight: false)
                       leftImgCell.btn_showImage.tag = indexPath.row
                       leftImgCell.btn_showImage.addTarget(self, action: #selector(self.clickOnImage), for: .touchUpInside)
                       
                       cell = leftImgCell
                   }
               }
               return cell
        }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        let dict = self.oldMessageArr[indexPath.row]
        
        if dict.chatMsgType! == "Text"
        {
            return UITableView.automaticDimension
        }
        else
        {
            return 200
        }
    }
    
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        
        let dict = self.oldMessageArr[indexPath.row]
        
        if dict.chatMsgType! == "Text"
        {
            return UITableView.automaticDimension
        }
        else
        {
            return 200
        }
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let view:UIView = UIView.init(frame:CGRect(x:0,y:0,width:0,height:0))
        return view
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 1
    }
    
    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        let view:UIView = UIView.init(frame:CGRect(x:0,y:0,width:0,height:0))
        return view
    }
    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 1
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
    }
    
    func scrollViewWillBeginDragging(_ scrollView: UIScrollView) {
        self.lastContentOffset = scrollView.contentOffset.y
    }
    
    // while scrolling this delegate is being called so you may now check which direction your scrollView is being scrolled to
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        
        if (self.lastContentOffset < scrollView.contentOffset.y) {
            // moved to top
            print("Scroll Up")
        } else if (self.lastContentOffset > scrollView.contentOffset.y) {
            // moved to bottom
            print("Scroll Down")
        } else {
            // didn't move
        }
    }
    
    
    
    // MARK: - Keyboard methods
    //---------------------------------------------------------------------------------------------------------------------------------------------
    func addKeyboardNotifier() {
        
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardShow(_:)), name: UIResponder.keyboardWillShowNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardShow(_:)), name: UIResponder.keyboardDidShowNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardHide(_:)), name: UIResponder.keyboardWillHideNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardHide(_:)), name: UIResponder.keyboardDidHideNotification, object: nil)
    }
    
    func hideKeyboardWhenTappedAroundTableview() {
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(UIViewController.dismissKeyboard))
        tap.cancelsTouchesInView = false
        self.tableview_chat.addGestureRecognizer(tap)
    }
    
    @objc func keyboardShow(_ notification: Notification?) {
        print(self.view_chatview.frame)
        if let keyboardFrame: NSValue = notification?.userInfo?[UIResponder.keyboardFrameEndUserInfoKey] as? NSValue {
            let keyboardRectangle = keyboardFrame.cgRectValue
            let keyboardHeight = keyboardRectangle.height
            UIView.animate(withDuration: 0.4, delay: 0, options: .allowUserInteraction, animations: {
                print(self.view_chatview.frame)
                print(keyboardHeight)
                if #available(iOS 11.0, *) {
                    self.layoutConstraintChatView_bottom.constant = keyboardHeight - self.view.safeAreaInsets.bottom
                } else {
                    // Fallback on earlier versions
                }
                print(self.view_chatview.frame)
                print(self.view_chatview.frame.origin.y)
                
//                                if self.chatMessageArr.count > 0
//                                {
//                                    self.tableview_chat.scrollToBottom()
//                                }
            })
        }
    }
    
    //---------------------------------------------------------------------------------------------------------------------------------------------
    @objc func keyboardHide(_ notification: Notification?) {
        print(self.view_chatview.frame)
        UIView.animate(withDuration: 0.4, delay: 0, options: .allowUserInteraction, animations: {
            self.layoutConstraintChatView_bottom.constant = 0
        })
    }
    
    
    // MARK:- ImagePicker Delegate
    
    func openCamera()
    {
        let picker:UIImagePickerController = UIImagePickerController()
        if UIImagePickerController.isSourceTypeAvailable(.camera) {
            picker.view.tintColor = UIColor.orange
            picker.delegate = self
            picker.sourceType = UIImagePickerController.SourceType.camera
            self.present(picker, animated: true, completion: nil)
        }
    }
    
    func openGallary()
    {
        let picker:UIImagePickerController = UIImagePickerController()
        if UIImagePickerController.isSourceTypeAvailable(.photoLibrary) {
            picker.view.tintColor = UIColor.orange
            picker.delegate = self
            picker.sourceType = UIImagePickerController.SourceType.photoLibrary
            self.present(picker, animated: true, completion: nil)
        }
    }
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        let image1 = info[UIImagePickerController.InfoKey.originalImage] as! UIImage
        
        if let pngdata = image1.resize(maxWidth: 500, maxHeight: 250).pngData()
        {
            self.uploadPicture(imageData: pngdata)
        }
        picker.dismiss(animated: true, completion: nil);
    }
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController)
    {
        picker.dismiss(animated: true, completion: nil);
    }
    
    
    // MARK:- Button Action
    
    
    @IBAction func click_Back(_ sender: UIButton) {
        if isNavigateToChatListingVC == false
        {
            self.navigationController?.popViewController(animated : true)
        }
        else
        {
            let vc = storyboard?.instantiateViewController(withIdentifier: "MainTabVC") as! MainTabVC
            //vc.selectedIndex = 1
            self.navigationController?.pushViewController(vc, animated: true)
        }
    }
    
    
    @IBAction func click_send(_ sender: UIButton)
    {
        if self.textView_message.text.count != 0
        {
            MessageType = "text"
            self.WS_SendMessageAPI()
            self.btn_send.isUserInteractionEnabled = false
            self.textView_message.text = ""
        }
    }
    
    @objc func clickOnImage(sender: UIButton)
    {
        let index = IndexPath(row: sender.tag, section: 0)
        guard let cell = tableview_chat.cellForRow(at: index) as? ChatTableViewCell else {return}
        
        let imgInfo = GSImageInfo(image: cell.img_sendUser.image!, imageMode: .aspectFit)
        let transitionInfo = GSTransitionInfo(fromView: cell.img_sendUser)
        self.view.bringSubviewToFront(cell.img_sendUser)
        let imgViewer = GSImageViewerController(imageInfo: imgInfo, transitionInfo: transitionInfo)
        
        imgViewer.dismissCompletion = {
            print("dismiss called")
        }
        
        self.present(imgViewer, animated: true)
        
    }
    
    
    @IBAction func click_PhotoGalary(_ sender: UIButton)
    {
        let optionMenu = UIAlertController(title: "Choose Image", message: nil, preferredStyle: UIAlertController.Style.actionSheet)
        
        let option1 = UIAlertAction(title: "Camera", style: .default, handler: {
            (alert: UIAlertAction!) -> Void in
            print("Option 1")
            self.openCamera()
        })
        
        let option2 = UIAlertAction(title: "Gallery", style: .default, handler: {
            
            (alert: UIAlertAction!) -> Void in
            print("Option 2")
            self.openGallary()
        })
        
        let cancelAction = UIAlertAction(title: "Cancel", style: .cancel, handler: {
            (alert: UIAlertAction!) -> Void in
            print("Cancelled")
        })
        
        optionMenu.addAction(option1)
        optionMenu.addAction(option2)
        optionMenu.addAction(cancelAction)
        
        
        if ( UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiom.pad )
        {
            if let currentPopoverpresentioncontroller = optionMenu.popoverPresentationController{
                currentPopoverpresentioncontroller.sourceView = sender
                currentPopoverpresentioncontroller.sourceRect = sender.bounds;
                currentPopoverpresentioncontroller.permittedArrowDirections = UIPopoverArrowDirection.up;
                self.present(optionMenu, animated: true, completion: nil)
            }
        }else{
            self.present(optionMenu, animated: true, completion: nil)
        }
    }
    
    func compressImage(_ image: UIImage?) -> UIImage? {
        var actualHeight = Float(image?.size.height ?? 0.0)
        var actualWidth = Float(image?.size.width ?? 0.0)
        let maxHeight: Float = 400.0
        let maxWidth: Float = 400.0
        var imgRatio: Float = actualWidth / actualHeight
        let maxRatio: Float = maxWidth / maxHeight
        //        let compressionQuality: Float = 1.0
        //50 percent compression
        if actualHeight > maxHeight || actualWidth > maxWidth {
            if imgRatio < maxRatio {
                //adjust width according to maxHeight
                imgRatio = maxHeight / actualHeight
                actualWidth = imgRatio * actualWidth
                actualHeight = maxHeight
            } else if imgRatio > maxRatio {
                //adjust height according to maxWidth
                imgRatio = maxWidth / actualWidth
                actualHeight = imgRatio * actualHeight
                actualWidth = maxWidth
            } else {
                actualHeight = maxHeight
                actualWidth = maxWidth
            }
        }
        let rect = CGRect(x: 0.0, y: 0.0, width: CGFloat(actualWidth), height: CGFloat(actualHeight))
        UIGraphicsBeginImageContext(rect.size)
        image?.draw(in: rect)
        let img: UIImage? = UIGraphicsGetImageFromCurrentImageContext()
        let imageData = img!.pngData()
        UIGraphicsEndImageContext()
        if let aData = imageData {
            return UIImage(data: aData)
        }
        return nil
    }
}

extension ChatVC
{
    func WS_isAccept()
    {
        APIClient<ModelIsAcceptBase>().API_PUT(Url: ws_Put_isAccept+"\(ChatRoomID)", Params: [:], Authentication: true, Progress: true, Alert: true, Offline: false, SuperVC: self, completionSuccess: { (modelResponse) in
            
            print("---------------------")
            print("Login Result:",modelResponse.toJSONString(prettyPrint: true)!)
            print("---------------------")
            
            if(modelResponse.success == true){
                
                self.RequestView.isHidden = true
                ChatVC.self.isAccept = 1
            }
            else {
                 if modelResponse.message == "Authentication API is invalid!" {
                     let vc = self.storyboard?.instantiateViewController(withIdentifier: "LogoutVc") as! LogoutVc
                     self.navigationController?.pushViewController(vc, animated: false)
                 }
                 else {
                     UtilityClass.hidehud()
                     Toast(text: modelResponse.message ?? "").show()
                 }
            }
            
        }) { (failed) in
        }
    }
    
    func WS_SendMessageAPI()
    {
        var url = String()
        
        var offset = Int()
        let dict = oldMessageArr.last
        if let offID = dict?.chatMsgId
        {
            offset = offID
        }
        
        var parameters = [String: Any]()
        
        if self.MessageType == "text"
        {
            parameters = [
                "chatroom_id" : ChatRoomID,
                "receiver_id" : ReceiverID,
                "message" : self.textView_message.text ?? "",
                "chat_msg_type" : self.MessageType,
                "offsetID" : offset
                
            ]
            print("Send Message parameters:::::::::::::",parameters)
        }
        else
        {
            parameters = [
                "chatroom_id" :ChatRoomID,
                "receiver_id" : ReceiverID,
                "message" : self.SendImageName,
                "chat_msg_type" : self.MessageType,
                "offsetID" : offset
                
            ]
            print("Send Message parameters:::::::::::::",parameters)
        }
        
        url = ws_Post_ChatMessage
        
        APIClient<ModelChatMessageBase>().API_POST(Url: url, Params: parameters as [String : AnyObject], Authentication: true, Progress: false, Alert: true, Offline: false, SuperVC: self, completionSuccess: { (modelResponse) in
            
            print("---------------------")
            print("Result:",modelResponse.toJSONString(prettyPrint: true)!)
            print("---------------------")
            
            if(modelResponse.success == true)
            {
                if let result = modelResponse.chatMessages, result.count != 0
                {
                    for item in result
                    {
                        self.oldMessageArr.append(item)
                        self.textView_message.text = ""
                        self.inputPanelUpdate()
                        self.img_send.image = self.img_send.image!.withRenderingMode(.alwaysTemplate)
                        self.img_send.tintColor = UIColor.lightGray
                        self.btn_send.isUserInteractionEnabled = false
                        // self.tableview_chat.scrollToBottom()
                    }
                    
                    self.reloadTableView()
                }
                self.btn_send.isUserInteractionEnabled = true
                ChatVC.isMsgSend = true
            }
            else  {
                // self.showToast(message: modelResponse.message ?? "")
                if modelResponse.message == "Authentication API is invalid!" {
                    let vc = self.storyboard?.instantiateViewController(withIdentifier: "LogoutVc") as! LogoutVc
                    self.navigationController?.pushViewController(vc, animated: false)
                }
                else {
                    Toast(text: modelResponse.message ?? "").show()
                    self.reloadTableView()
                }
            }
           self.reloadTableView()
        }) { (failed) in
            self.reloadTableView()
        }
        
    }
}
extension ChatVC
{

    func WS_GetOldMessagesAPI()
    {
        var url = String()
        var offset = Int()
        var chatRoom = Int()
        let dict = oldMessageArr.first
        if let offID = dict?.chatMsgId
        {
            offset = offID
        }
        else{
            offset = 0
        }
        
        url = ws_Get_OldMessages+"\(offset)"+"&"+"chatroomid=\(ChatRoomID)"
        
        APIClient<ModelChatMessageBase>().API_GET(Url: url, Params: [:], Spinner: 3, Authentication: true, Progress: true, Alert: true, Offline: false, SuperVC: self, completionSuccess: { (modelResponse) in
            
            print("---------------------")
            print("Result:",modelResponse.toJSONString(prettyPrint: true)!)
            print("---------------------")
            
            if(modelResponse.success == true)
            {
                if let result = modelResponse.chatMessages, result.count != 0
                {
                    let neweReverseData = result.reversed()
                    for item in neweReverseData
                    {
                        if let id = item.chatMsgId, self.oldMessageIDsArr.contains(id) == false
                        {
                            self.oldMessageIDsArr.insert(id, at: 0)
                            self.oldMessageArr.insert(item, at: 0)
                        }
                    }
                    self.tableview_chat.tag = 1
                    self.refresher.endRefreshing()
                    self.reloadTableView()
                    self.tableview_chat.scrollToRow(at: IndexPath.init(row: result.count - 1, section: 0), at: .top, animated: false)
                }
                
            }
            self.tableview_chat.tag = 1
            self.refresher.endRefreshing()
            self.reloadTableView()
        }) { (failed) in
            
        }
    }
    
        func uploadPicture(imageData:Data)
        {
            UtilityClass.showhud()
            APIClient1().API_UPLOAD(Url: ws_Image_Upload, Params: ["" : "" as AnyObject], fileName: "profile.png", fileData: imageData, mimeType: "image/png", Authentication: true, mapObject: ModelFileUploadBase.self, SuperVC: self, completionSuccess: { (modelResponse) in
    
                let resultModel = Mapper<ModelFileUploadBase>().map(JSONObject: modelResponse)
                UtilityClass.hidehud()
                if(resultModel?.success == true), let userImg = resultModel?.imageName, userImg.count != 0
                {
                    self.SendImageName = userImg
                    self.MessageType = "File"
                    self.WS_SendMessageAPI()
                }
                else
                {
                     
                }
    
            }) { (failed) in
    
            }
        }
    }

