//
//  PhotosCollectionVC.swift
//  Arrange
//
//  Created by Miral Kamani on 16/03/20.
//  Copyright © 2020 Miral Kamani. All rights reserved.
//

import UIKit

class PhotosCollectionVC: UIViewController {
    
    @IBOutlet var CView: UICollectionView!
    let ArrayImage = ["2-1","3"]
    override func viewDidLoad() {
        super.viewDidLoad()
        self.RegisterXIB()
        // Do any additional setup after loading the view.
    }
    @IBAction func btnNext(_ sender: UIButton)
    {
        let collectionBounds = self.CView.bounds
        let contentOffSet = CGFloat(floor(self.CView.contentOffset.x +
            CView.frame.size.width))
        self.moveCollectionToFrame(contentOffset: contentOffSet)
    }
    @IBAction func btnPrevious(_ sender: UIButton)
    {
        let collectionBounds = self.CView.bounds
        let contentOffSet = CGFloat(floor(self.CView.contentOffset.x -
            CView.frame.size.width))
        self.moveCollectionToFrame(contentOffset: contentOffSet)
    }
    func moveCollectionToFrame(contentOffset : CGFloat) {
        
        let frame: CGRect = CGRect(x : contentOffset ,y : self.CView.contentOffset.y ,width : self.CView.frame.size.width,height : self.CView.frame.size.height)
        self.CView.scrollRectToVisible(frame, animated: true)
    }
    
}

extension PhotosCollectionVC: UICollectionViewDelegate, UICollectionViewDelegateFlowLayout, UICollectionViewDataSource
{
    func RegisterXIB()
    {
        self.CView.register(UINib(nibName: "CardCollectionCell", bundle: nil), forCellWithReuseIdentifier: "CardCollectionCell")
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
       // return ArrayImage.count
        return 3
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "CardCollectionCell", for: indexPath) as! CardCollectionCell
        //let image = ArrayImage[indexPath.row]
      //  cell.imagePhotos.image = UIImage(named: image)
        return cell
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let height = self.CView.frame.size.height
        let width = self.CView.frame.size.width
        return CGSize(width: width, height: height)
    }
    
}
