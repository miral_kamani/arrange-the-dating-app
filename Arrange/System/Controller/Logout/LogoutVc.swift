//7
//  LogoutVc.swift
//  Signer
//
//  Created by Binal on 13/02/20.
//  Copyright © 2020 kashyap. All rights reserved.
//

import UIKit
class LogoutVc: UIViewController {
    @IBOutlet var viewLogout: UIViewX!

    override func viewDidLoad() {
        super.viewDidLoad()
        self.viewLogout.layer.cornerRadius = 10
        self.viewLogout.layer.shadowColor = UIColor.black.cgColor
        self.viewLogout.layer.shadowOpacity = 0.2
        self.viewLogout.layer.shadowOffset = CGSize(width: 1, height: 1)
        self.viewLogout.layer.shadowRadius = 2.0
        self.viewLogout.layoutIfNeeded()
    }
    
   
    @IBAction func btnYes(_ sender: UIButton) {
        
        let f_token = Defaults[.FCMToken]
        Defaults.removeAll()
        Defaults[.FCMToken] = f_token
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) {
            let vc = self.storyboard?.instantiateViewController(withIdentifier: "ViewController") as! ViewController
            self.navigationController?.pushViewController(vc, animated: true)
            
        }
    }
}
