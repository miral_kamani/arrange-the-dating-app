//
//  MatchesVC.swift
//  Arrange
//
//  Created by Miral Kamani on 11/03/20.
//  Copyright © 2020 Miral Kamani. All rights reserved.
//

import UIKit
import Kingfisher
import Toaster

class MatchesVC: UIViewController,UIGestureRecognizerDelegate {
    
    //MARK:- Outlets
    
    @IBOutlet var collectionReload: UICollectionView!
    
    //MARK:- Variable
    
    var arrayMatchData = [ModelMatchResult]()
    static var CreateChatRoomID = Int()
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.interactivePopGestureRecognizer?.delegate = self
        self.navigationController?.interactivePopGestureRecognizer?.isEnabled = true
        RegisterCollectionXIB()
    }
    override func viewWillAppear(_ animated: Bool) {
        self.WS_Get_Matches()
    }
    @IBAction func backTapped(_ sender: UIButton)
    {
        self.navigationController?.popViewController(animated: true)
    }
}
extension MatchesVC: UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout
{
    func RegisterCollectionXIB()
    {
        self.collectionReload.register(UINib(nibName: "MatchCell", bundle: nil), forCellWithReuseIdentifier: "MatchCell")
    }
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int
    {
       return getRowsforList(collectionView, forArrayCount: self.arrayMatchData.count, withPlaceHolder: "You have no matches", withColor: CharcoalGrey, imageName: noMatch, imageSize: imageSize)
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "MatchCell", for: indexPath) as! MatchCell
        if arrayMatchData.count != 0
        {
            let dict = arrayMatchData[indexPath.row]
            if let data = dict.userDetails
            {
                if let name = data.userProfileName, name.count != 0
                {
                    cell.labelName.text = name
                }
                
                if let age = data.userProfileAge, age != 0
                {
                    cell.labelAge.text = "\(age)"
                }
                
                if let image = data.userProfileImage, image.count != 0
                {
                    let url = WS_ProfileImageURL + image
                    print("profile image URL", url)
                    cell.imageProfile.kf.setImage(with: URL(string: url), placeholder: UIImage(named: "default"), options: nil, progressBlock: nil, completionHandler: { (result) in
                        switch result {
                        case .success(let value):
                            print("Task done for: \(value.source.url?.absoluteString ?? "")")
                            
                        case .failure(let error):
                            print("Job failed: \(error.localizedDescription)")
                        }
                    })
                }
                
                if let occupation = data.careerName, occupation.count != 0
                {
                    cell.labelOccupation.text = occupation
                }
                
                if let distance = data.distance, distance != 0
                {
                    cell.labelDistance.text = "\(distance)"
                }
            }
        }
        else
        {
            
        }
        return cell
    }
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let dict = arrayMatchData[indexPath.row]
        if let data = dict.userDetails
        {
            MatchesVC.CreateChatRoomID = data.userID!
            self.WS_Post_CreateChatRoom()
        }
        
    }
    func collectionView(_ collectionView: UICollectionView,layout collectionViewLayout: UICollectionViewLayout,sizeForItemAt indexPath: IndexPath) -> CGSize
    {
        let padding: CGFloat = 10
        let collectionViewSize = collectionReload.frame.size.width - padding
        return CGSize(width: collectionViewSize/2, height: collectionViewSize/2)
    }
    
    
}


extension MatchesVC
{
    func WS_Get_Matches()
    {
        APIClient<ModelGetMatchBase>().API_GET(Url: ws_Get_Matches, Params: [:], Spinner: 1, Authentication: true, Progress: true, Alert: true, Offline: false, SuperVC: self, completionSuccess: { (modelResponse) in
            
            print("---------------------")
            print("Result:",modelResponse.toJSONString(prettyPrint: true)!)
            print("---------------------")
            self.arrayMatchData.removeAll()
            if(modelResponse.success == true)
            {
                if let result = modelResponse.matchResult, result.count != 0
                {
                    for data in result
                    {
                        self.arrayMatchData.append(data)
                    }
                }
            }
            else
            {
                if modelResponse.message == "Authentication API is invalid!" {
                    let vc = self.storyboard?.instantiateViewController(withIdentifier: "LogoutVc") as! LogoutVc
                    self.navigationController?.pushViewController(vc, animated: false)
                }
                else {
                    UtilityClass.hidehud()
                    Toast(text: modelResponse.message ?? "").show()
                }
            }
            self.collectionReload.tag = 1
            self.collectionReload.reloadData()
        }) { (failed) in
            
        }
    }
    
    func WS_Post_CreateChatRoom()
    {
        var parameters = [String: Any]()
        parameters = ["receiver_id" : MatchesVC.CreateChatRoomID]
        print(parameters)
        APIClient<ModelChatRoomBase>().API_POST(Url: ws_Post_ChatRoom, Params: parameters as [String:AnyObject], Authentication: true, Progress: true, Alert: true, Offline: false, SuperVC: self, completionSuccess: { (modelResponse) in
            
            print("---------------------")
            print("Login Result:",modelResponse.toJSONString(prettyPrint: true)!)
            print("---------------------")
            
            if(modelResponse.success == true){
                Toast(text: modelResponse.message ?? "").show()
                let navigate = self.storyboard?.instantiateViewController(withIdentifier: "MainTabVC") as! MainTabVC
                self.navigationController?.pushViewController(navigate, animated: true)
            }
            else {
                 if modelResponse.message == "Authentication API is invalid!" {
                     let vc = self.storyboard?.instantiateViewController(withIdentifier: "LogoutVc") as! LogoutVc
                     self.navigationController?.pushViewController(vc, animated: false)
                 }
                 else {
                     UtilityClass.hidehud()
                     Toast(text: modelResponse.message ?? "").show()
                 }
            }
            
        }) { (failed) in
        }
    }
}
