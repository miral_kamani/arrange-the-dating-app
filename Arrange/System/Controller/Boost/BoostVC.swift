//
//  BoostVC.swift
//  Arrange
//
//  Created by Miral Kamani on 22/02/20.
//  Copyright © 2020 Miral Kamani. All rights reserved.
//

import UIKit

class BoostVC: UIViewController {
    
    //MARK:- Outlets
    @IBOutlet weak var selectedViewOne: UIView!
    @IBOutlet weak var ViewOne: UIViewX!
    
    @IBOutlet weak var btnViewOne: UIButton!
    
    @IBOutlet weak var SelectedViewTwo: UIView!
    @IBOutlet weak var ViewTwo: UIView!
    @IBOutlet weak var btnViewTwo: UIButton!
    
    @IBOutlet weak var SelectedViewThree: UIView!
    @IBOutlet weak var ViewThree: UIView!
    @IBOutlet weak var btnViewThree: UIButton!
    
    @IBOutlet var btnback: UIButton!
    @IBOutlet weak var btnGetBoost: UIButton!
    
    @IBOutlet var btnDull: UIButton!
    @IBOutlet var btnNoThanks: UIButton!
    @IBOutlet var btnBoostAgain: UIButton!
    @IBOutlet var BoostSuccess: UIView!
    
     //MARK:- Variable
    var gradientLayer: CAGradientLayer!
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        
     //   self.SelectedView.isHidden = true
        self.selectedViewOne.isHidden = true
        self.SelectedViewTwo.isHidden = true
        self.SelectedViewThree.isHidden = true
        self.btnDull.isHidden = true
        self.btnViewOne.addTarget(self, action: #selector(OnClickViewOne), for: .touchUpInside)
         self.btnViewTwo.addTarget(self, action: #selector(OnClickViewTwo), for: .touchUpInside)
         self.btnViewThree.addTarget(self, action: #selector(OnClickViewThree), for: .touchUpInside)
//        self.ViewOne.addTapGesture(tapNumber: 1, target: self, action: #selector(OnClickViewOne))
//        self.ViewTwo.addTapGesture(tapNumber: 1, target: self, action: #selector(OnClickViewTwo))
//        self.ViewThree.addTapGesture(tapNumber: 1, target: self, action: #selector(OnClickViewThree))
        self.btnback.addTarget(self, action: #selector(OnClickBack), for: .touchUpInside)
        self.btnGetBoost.addTarget(self, action: #selector(OnClickBoost), for: .touchUpInside)
        self.btnBoostAgain.addTarget(self, action: #selector(OnClickBoostAgain), for: .touchUpInside)
        self.btnNoThanks.addTarget(self, action: #selector(OnClickNoThanks), for: .touchUpInside)
    }
    
    @objc func OnClickViewOne(sender: UIButton)
    {
        self.ViewOne.isHidden = true
        self.ViewTwo.isHidden = false
        self.ViewThree.isHidden = false
        //self.SelectedView.isHidden = false
        self.selectedViewOne.isHidden = false
        self.SelectedViewTwo.isHidden = true
        self.SelectedViewThree.isHidden = true
    }
    @objc func OnClickViewTwo(sender: UIButton)
    {
        self.ViewOne.isHidden = false
        self.ViewTwo.isHidden = true
        self.ViewThree.isHidden = false
        //self.SelectedView.isHidden = false
        self.selectedViewOne.isHidden = true
        self.SelectedViewTwo.isHidden = false
        self.SelectedViewThree.isHidden = true
    }
    @objc func OnClickViewThree(sender: UIButton)
    {
        self.ViewOne.isHidden = false
        self.ViewTwo.isHidden = false
        self.ViewThree.isHidden = true
        //self.SelectedView.isHidden = false
        self.selectedViewOne.isHidden = true
        self.SelectedViewTwo.isHidden = true
        self.SelectedViewThree.isHidden = false
    }
    @objc func OnClickBoost(sender: UIButton)
    {
        BoostSuccess.center = CGPoint(x: view.frame.size.width  / 2,
                                      y: view.frame.size.height / 2)
        btnDull.isHidden = false
        self.view.addSubview(BoostSuccess)
        // self.AddSubViewtoParentView(parentview: view, subview: BoostSuccess)
    }
    @objc func OnClickNoThanks(sender: UIButton)
    {
        self.btnDull.isHidden = true
        self.BoostSuccess.removeFromSuperview()
    }
    @objc func OnClickBoostAgain(sender: UIButton)
    {
        self.btnDull.isHidden = true
        self.BoostSuccess.removeFromSuperview()
    }
    @objc func OnClickBtnDull(sender: UIButton)
    {
        self.btnDull.isHidden = true
        self.BoostSuccess.removeFromSuperview()
    }
    @objc func OnClickBack(sender: UIButton)
    {
        self.navigationController?.popViewController(animated: true)
    }
    
}
