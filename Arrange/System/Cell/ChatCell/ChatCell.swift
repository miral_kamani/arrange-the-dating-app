//
//  ChatCell.swift
//  Arrange
//
//  Created by Miral Kamani on 19/02/20.
//  Copyright © 2020 Miral Kamani. All rights reserved.
//

import UIKit

class ChatCell: UITableViewCell {

    @IBOutlet var imageProfile: RoundImageView!
    @IBOutlet var labelNameAge: UILabel!
    @IBOutlet var labelOccupation: UILabel!
    @IBOutlet var labelMessage: UILabel!
    @IBOutlet var labelDate: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        if UIDevice.current.userInterfaceIdiom == .phone
        {
            imageProfile.layer.cornerRadius = 35.0
        }
        else
        {
            imageProfile.layer.cornerRadius = 50.0
        }
        // Initialization code
    }
    func setLabel()
    {
        let attributedString = NSMutableAttributedString(string: "Diesel, 25", attributes: [
          .font: UIFont(name: "Avenir-Black", size: 22.0)!,
          .foregroundColor: UIColor.white,
          .kern: -0.64
        ])
        attributedString.addAttribute(.font, value: UIFont(name: "Avenir-Book", size: 22.0)!, range: NSRange(location: 8, length: 2))
    }
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
