//
//  PreferencesProfileTableViewCell.swift
//  Nalmout
//
//  Created by Darshit Mendapara on 29/08/18.
//  Copyright © 2018 Socialinfotech. All rights reserved.
//

import UIKit
import AVKit
import AVFoundation
import Photos


class ChatTableViewCell: UITableViewCell {
    
    @IBOutlet var view_back: UIView!
    @IBOutlet var view_chatMessageBackground: UIView!
    @IBOutlet var imageview_chatMessageIcon: UIImageView!
    @IBOutlet var imageview_chatProfile: UIImageView!
    @IBOutlet var lbl_time: UILabel!
    @IBOutlet weak var img_sendUser: UIImageView!
    @IBOutlet var btn_showImage: UIButton!
    
    //TEXT
    @IBOutlet var lbl_chatMessageText: UILabel!
    @IBOutlet var lbl_chatMessageTextConstraintW: NSLayoutConstraint!
    
   
    var MSGType = String()
    private var isPositionRight: Bool = false
    
    let kConstantColorChatMessageLeftObject = UIColor(red: Int(192.0/255.0), green: Int(152.0/255.0), blue: Int(255.0/255.0), a: 1) //0x999999
    let kConstantColorChatMessageRightObject = UIColor(red: Int(140.0/255.0), green: Int(94.0/255.0), blue: Int(255.0/255.0), a: 1) //0xeeeeee
    
    let screenWidth = UIScreen.main.bounds.width
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    @objc override func layoutSubviews() {
        super.layoutSubviews()
       // configSetup()
    }
    
//    private func configSetup()
//    {
//        self.imageview_chatProfile.Default_setCorner(borderColor: .clear, corner: self.imageview_chatProfile.frame.size.width/2, border: 0.0)
//        self.view_chatMessageBackground.Default_setCornerNoBorder(corner: 10.0)
//    }
    
    func updateCellTextInfo(positionRight:Bool)
    {
        if MSGType == "text"
        {
            if positionRight == true
            {
                let size = self.size(positionRight: positionRight)
                self.lbl_chatMessageText.sizeToFit()
                self.updateConstraints()
                self.lbl_chatMessageTextConstraintW.constant = CGFloat.maximum(10, size.width)
            }
            else
            {
                let size = self.size(positionRight: positionRight)
                self.lbl_chatMessageText.sizeToFit()
                self.updateConstraints()
                self.lbl_chatMessageTextConstraintW.constant = CGFloat.maximum(10, size.width)
            }
        }
    }
   
    func size(positionRight:Bool) -> CGRect
    {
        if positionRight == true
        {
            let rect = self.lbl_chatMessageText.text?.trimmed.boundingRect(with: CGSize(width: screenWidth-(71+64)-(20+20), height: CGFloat.greatestFiniteMagnitude), options: .usesLineFragmentOrigin, attributes: [NSAttributedString.Key.font: self.lbl_chatMessageText.font!], context: nil)
            return rect!
        }
        else
        {
            let rect = self.lbl_chatMessageText.text?.trimmed.boundingRect(with: CGSize(width: screenWidth-(71+64)-(20+20), height: CGFloat.greatestFiniteMagnitude), options: .usesLineFragmentOrigin, attributes: [NSAttributedString.Key.font: self.lbl_chatMessageText.font!], context: nil)
            return rect!
        }
    }
}
