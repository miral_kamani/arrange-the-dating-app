//
//  LocationCell.swift
//  Arrange
//
//  Created by Miral Kamani on 25/02/20.
//  Copyright © 2020 Miral Kamani. All rights reserved.
//

import UIKit

class LocationCell: UITableViewCell {
    @IBOutlet var labelOption: UILabel!
    @IBOutlet var imageTick: UIImageView!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
