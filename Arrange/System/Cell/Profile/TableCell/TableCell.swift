//
//  TableCell.swift
//  Arrange
//
//  Created by Miral Kamani on 14/02/20.
//  Copyright © 2020 Miral Kamani. All rights reserved.
//

import UIKit

class TableCell: UITableViewCell {

    @IBOutlet var ImageIcon: UIImageView!
    
    @IBOutlet var labelTitleNAme: UILabel!
    
    @IBOutlet var imagePush: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
