//
//  CardCollectionCell.swift
//  Arrange
//
//  Created by Miral Kamani on 16/03/20.
//  Copyright © 2020 Miral Kamani. All rights reserved.
//

import UIKit

class CardCollectionCell: UICollectionViewCell {

    @IBOutlet var Pager: UIPageControl!
    @IBOutlet var imagePhotos: UIImageView!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

}
