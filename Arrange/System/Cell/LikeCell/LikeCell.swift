//
//  LikeCell.swift
//  Arrange
//
//  Created by Miral Kamani on 18/02/20.
//  Copyright © 2020 Miral Kamani. All rights reserved.
//

import UIKit

class LikeCell: UICollectionViewCell {
    
    @IBOutlet var labelDistance: UILabel!
    @IBOutlet var labelAge: UILabel!
    @IBOutlet var labelName: UILabel!
    @IBOutlet var labelOccupation: UILabel!
    
    @IBOutlet var imageProfile: UIImageView!
    @IBOutlet var LikeView: UIView!
    override func awakeFromNib() {
        super.awakeFromNib()
        
    }
    
}
