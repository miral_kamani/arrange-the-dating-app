//
//  InterestCell.swift
//  Arrange
//
//  Created by Miral Kamani on 03/03/20.
//  Copyright © 2020 Miral Kamani. All rights reserved.
//

import UIKit

class InterestCell: UICollectionViewCell {

    @IBOutlet var RoundView: UIView!
    @IBOutlet var imageInterest: UIImageView!
    @IBOutlet var btnClick: UIButton!
    @IBOutlet var labelName: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        self.RoundView.layer.cornerRadius = self.RoundView.frame.height / 2
        // Initialization code
    }

}
