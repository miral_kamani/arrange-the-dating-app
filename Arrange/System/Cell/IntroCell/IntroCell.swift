//
//  IntroCell.swift
//  Arrange
//
//  Created by Miral Kamani on 20/02/20.
//  Copyright © 2020 Miral Kamani. All rights reserved.
//

import UIKit

class IntroCell: UICollectionViewCell {

    @IBOutlet var ViewOne: UIView!
    @IBOutlet var ViewTwo: UIView!
    @IBOutlet var ViewThree: UIView!
    @IBOutlet var ViewFour: UIView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

}
