//
//  MatchSettingCell.swift
//  Arrange
//
//  Created by Miral Kamani on 12/02/20.
//  Copyright © 2020 Miral Kamani. All rights reserved.
//

import UIKit
import TTSegmentedControl
import RangeSeekSlider
class MatchSettingCell: UITableViewCell {

    @IBOutlet var Segment: TTSegmentedControl!
   
    @IBOutlet var ageRangeSlider: RangeSeekSlider!
    @IBOutlet var heightRangeSlider: RangeSeekSlider!
    @IBOutlet var txtReligion: UITextField!
    @IBOutlet var txtCommunity: UITextField!
    @IBOutlet var txtCareer: UITextField!
    @IBOutlet var txtEducation: UITextField!
    @IBOutlet var txtLocation: UITextField!
    @IBOutlet weak var btnDate: UIButton!
    @IBOutlet weak var btnMonth: UIButton!
    
    @IBOutlet weak var btnYear: UIButton!
    override func awakeFromNib() {
        
        super.awakeFromNib()
        self.setSegment()
        
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func setSegment()
    {
               Segment.allowChangeThumbWidth = false
               Segment.itemTitles = ["ALL","NEW","NEAR ME"]
               Segment.didSelectItemWith = { (index, title) -> () in
                   print("Selected item \(index)")
               }
               Segment.defaultTextColor = UIColor(red: 67, green: 66, blue: 72)
               Segment.selectedTextFont = UIFont(name: "Avenir-Black", size: 12)!
               Segment.defaultTextFont = UIFont(name: "Avenir-Black", size: 12)!
               Segment.selectedTextColor = UIColor.white
               Segment.thumbGradientColors = [UIColor(red: 192, green: 152, blue: 255), UIColor(red: 140, green: 94, blue: 255)]
               Segment.useShadow = true
               Segment.layoutSubviews()
              
    }
    
    
    
}
