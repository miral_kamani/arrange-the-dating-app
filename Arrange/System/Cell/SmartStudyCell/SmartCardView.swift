//
//  CardView.swift
//  SpanishFlashcards
//
//  Created by Grewon on 11/07/19.
//  Copyright © 2019 Grewon. All rights reserved.
//

import UIKit
import ImageSlideshow

class SmartCardView: UIView
{

    @IBOutlet weak var cardFullBackGroundView: UIView!
    @IBOutlet weak var cardFullBgViewTConstraint: NSLayoutConstraint!

    @IBOutlet var imageSlideShow: ImageSlideshow!
    @IBOutlet var ImageFullProfile: RoundableImageView!
    @IBOutlet var LabelUniversity: UILabel!
    @IBOutlet var UnlikeView: UIViewX!
    @IBOutlet var LikeView: UIViewX!
    @IBOutlet var btnViewMore: UIButton!
    
    @IBOutlet var ChatView: UIViewX!
    @IBOutlet var btnBoost: UIButton!
    
    @IBOutlet var BoostView: UIViewX!
    @IBOutlet var btnChat: UIButton!
    @IBOutlet var imageShape: UIImageView!
    @IBOutlet var btnSelectedViewMore: UIButton!
    @IBOutlet weak var LabelTitle: UILabel!
    
    @IBOutlet var btnViewPhotos: UIButton!
    var superVC:UIViewController!
    var contentview: SmartCardView!
    
    override func awakeFromNib()
    {
        self.imageSlideShow.slideshowInterval = 0.0
        self.imageSlideShow.isUserInteractionEnabled = true
        self.imageSlideShow.contentScaleMode = UIViewContentMode.scaleAspectFill
        self.imageSlideShow.layer.cornerRadius = 20.0
   
        self.ImageFullProfile.isHidden = true

        if UIDevice.current.userInterfaceIdiom == .phone
        {
            BoostView.cornerRadius = 30.0
            ChatView.cornerRadius = 24.0
        }
        else
        {
            BoostView.cornerRadius = 32.5
            ChatView.cornerRadius = 26.0
        }
    }
    
    func setLabel()
    {
        let attributedString = NSMutableAttributedString(string: "Aleksandra, 25", attributes: [
          .font: UIFont(name: "Avenir-Black", size: 22.0)!,
          .foregroundColor: UIColor.white,
          .kern: -0.64
        ])
        attributedString.addAttribute(.font, value: UIFont(name: "Avenir-Book", size: 22.0)!, range: NSRange(location: 12, length: 2))
    }
    override init(frame: CGRect)
    {
        super.init(frame: frame)
        setup()
    }
    
    required init?(coder aDecoder: NSCoder)
    {
        super.init(coder: aDecoder)
        setup()
    
    }
    
    func setup()
    {
        // Shadow
        layer.shadowColor = UIColor.darkGray.cgColor
        layer.shadowOpacity = 0.25
        layer.shadowOffset = CGSize(width: 0, height: 1.5)
        layer.shadowRadius = 1.0
        layer.shouldRasterize = true
        layer.rasterizationScale = UIScreen.main.scale
        
        // Corner Radius
        layer.cornerRadius = 10.0
    }
}

