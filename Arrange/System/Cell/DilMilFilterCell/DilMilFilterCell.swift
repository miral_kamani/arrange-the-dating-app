//
//  DilMilFilterCell.swift
//  Arrange
//
//  Created by Miral Kamani on 13/02/20.
//  Copyright © 2020 Miral Kamani. All rights reserved.
//

import UIKit

class DilMilFilterCell: UITableViewCell {
    
    @IBOutlet var ImageIcon: UIImageView!
    @IBOutlet var labelTitle: UILabel!
    @IBOutlet var labelSelectionData: UILabel!
    @IBOutlet var shadowView: UIView!
    @IBOutlet var btnClick: DCustomButton!
    override func awakeFromNib() {
        super.awakeFromNib()
        if UIDevice.current.userInterfaceIdiom == .phone
        {
            shadowView.layer.applySketchShadow()
            shadowView.layer.cornerRadius = shadowView.frame.size.height / 2
        }
        else
        {
            shadowView.layer.applySketchShadow()
            shadowView.layer.cornerRadius = 32.5
        }
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

    }
    
}
