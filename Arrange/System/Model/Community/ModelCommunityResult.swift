//
//  ModelCommunityResult.swift
//
//  Created by Miral Kamani on 04/03/20
//  Copyright (c) . All rights reserved.
//

import Foundation
import ObjectMapper
public class ModelCommunityResult: Mappable {

  // MARK: Declaration for string constants to be used to decode and also serialize.
  private struct SerializationKeys {
    static let communityDatetime = "community_datetime"
    static let communityName = "community_name"
    static let communityId = "community_id"
  }

  // MARK: Properties
  public var communityDatetime: String?
  public var communityName: String?
  public var communityId: Int?

  // MARK: ObjectMapper Initializers
  /// Map a JSON object to this class using ObjectMapper.
  ///
  /// - parameter map: A mapping from ObjectMapper.
  public required init?(map: Map){

  }

  /// Map a JSON object to this class using ObjectMapper.
  ///
  /// - parameter map: A mapping from ObjectMapper.
  public func mapping(map: Map) {
    communityDatetime <- map[SerializationKeys.communityDatetime]
    communityName <- map[SerializationKeys.communityName]
    communityId <- map[SerializationKeys.communityId]
  }

  /// Generates description of the object in the form of a NSDictionary.
  ///
  /// - returns: A Key value pair containing all valid values in the object.
  public func dictionaryRepresentation() -> [String: Any] {
    var dictionary: [String: Any] = [:]
    if let value = communityDatetime { dictionary[SerializationKeys.communityDatetime] = value }
    if let value = communityName { dictionary[SerializationKeys.communityName] = value }
    if let value = communityId { dictionary[SerializationKeys.communityId] = value }
    return dictionary
  }

}
