//
//  ModelUserPhotos.swift
//
//  Created by Miral Kamani on 18/03/20
//  Copyright (c) . All rights reserved.
//

import Foundation
import ObjectMapper

public class ModelUserPhotos: Mappable {

  // MARK: Declaration for string constants to be used to decode and also serialize.
  private struct SerializationKeys {
    static let userImages = "user_images"
  }

  // MARK: Properties
  public var userImages: String?

  // MARK: ObjectMapper Initializers
  /// Map a JSON object to this class using ObjectMapper.
  ///
  /// - parameter map: A mapping from ObjectMapper.
  public required init?(map: Map){

  }

  /// Map a JSON object to this class using ObjectMapper.
  ///
  /// - parameter map: A mapping from ObjectMapper.
  public func mapping(map: Map) {
    userImages <- map[SerializationKeys.userImages]
  }

  /// Generates description of the object in the form of a NSDictionary.
  ///
  /// - returns: A Key value pair containing all valid values in the object.
  public func dictionaryRepresentation() -> [String: Any] {
    var dictionary: [String: Any] = [:]
    if let value = userImages { dictionary[SerializationKeys.userImages] = value }
    return dictionary
  }

}
