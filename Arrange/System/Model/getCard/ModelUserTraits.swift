//
//  ModelUserTraits.swift
//
//  Created by Miral Kamani on 06/03/20
//  Copyright (c) . All rights reserved.
//

import Foundation
import ObjectMapper

public final class ModelUserTraits: Mappable {

  // MARK: Declaration for string constants to be used to decode and also serialize.
  private struct SerializationKeys {
    static let userTraitsId = "user_traits_id"
    static let refUserId = "ref_user_id"
    static let refTraitsId = "ref_traits_id"
    static let userTraitsDatetime = "user_traits_datetime"
    static let TraitName = "traits_name"
  }

  // MARK: Properties
  public var userTraitsId: Int?
  public var refUserId: Int?
  public var refTraitsId: Int?
  public var userTraitsDatetime: String?
    public var TraitName: String?
  // MARK: ObjectMapper Initializers
  /// Map a JSON object to this class using ObjectMapper.
  ///
  /// - parameter map: A mapping from ObjectMapper.
  public required init?(map: Map){

  }

  /// Map a JSON object to this class using ObjectMapper.
  ///
  /// - parameter map: A mapping from ObjectMapper.
  public func mapping(map: Map) {
    userTraitsId <- map[SerializationKeys.userTraitsId]
    refUserId <- map[SerializationKeys.refUserId]
    refTraitsId <- map[SerializationKeys.refTraitsId]
    userTraitsDatetime <- map[SerializationKeys.userTraitsDatetime]
    TraitName <- map[SerializationKeys.TraitName]
  }

  /// Generates description of the object in the form of a NSDictionary.
  ///
  /// - returns: A Key value pair containing all valid values in the object.
  public func dictionaryRepresentation() -> [String: Any] {
    var dictionary: [String: Any] = [:]
    if let value = userTraitsId { dictionary[SerializationKeys.userTraitsId] = value }
    if let value = refUserId { dictionary[SerializationKeys.refUserId] = value }
    if let value = refTraitsId { dictionary[SerializationKeys.refTraitsId] = value }
    if let value = userTraitsDatetime { dictionary[SerializationKeys.userTraitsDatetime] = value }
    if let value = TraitName { dictionary[SerializationKeys.TraitName] = value }
    return dictionary
  }

}
