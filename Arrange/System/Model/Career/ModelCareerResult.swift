//
//  ModelCareerResult.swift
//
//  Created by Miral Kamani on 04/03/20
//  Copyright (c) . All rights reserved.
//

import Foundation
import ObjectMapper
public class ModelCareerResult: Mappable {

  // MARK: Declaration for string constants to be used to decode and also serialize.
  private struct SerializationKeys {
    static let careerName = "career_name"
    static let careerId = "career_id"
    static let careerDatetime = "career_datetime"
  }

  // MARK: Properties
  public var careerName: String?
  public var careerId: Int?
  public var careerDatetime: String?

  // MARK: ObjectMapper Initializers
  /// Map a JSON object to this class using ObjectMapper.
  ///
  /// - parameter map: A mapping from ObjectMapper.
  public required init?(map: Map){

  }

  /// Map a JSON object to this class using ObjectMapper.
  ///
  /// - parameter map: A mapping from ObjectMapper.
  public func mapping(map: Map) {
    careerName <- map[SerializationKeys.careerName]
    careerId <- map[SerializationKeys.careerId]
    careerDatetime <- map[SerializationKeys.careerDatetime]
  }

  /// Generates description of the object in the form of a NSDictionary.
  ///
  /// - returns: A Key value pair containing all valid values in the object.
  public func dictionaryRepresentation() -> [String: Any] {
    var dictionary: [String: Any] = [:]
    if let value = careerName { dictionary[SerializationKeys.careerName] = value }
    if let value = careerId { dictionary[SerializationKeys.careerId] = value }
    if let value = careerDatetime { dictionary[SerializationKeys.careerDatetime] = value }
    return dictionary
  }

}
