//
//  ModelStateResult.swift
//
//  Created by Miral Kamani on 12/03/20
//  Copyright (c) . All rights reserved.
//

import Foundation
import ObjectMapper
public class ModelStateResult: Mappable {

  // MARK: Declaration for string constants to be used to decode and also serialize.
  private struct SerializationKeys {
    static let statesId = "states_id"
    static let name = "name"
    static let countryId = "country_id"
  }

  // MARK: Properties
  public var statesId: Int?
  public var name: String?
  public var countryId: Int?

  // MARK: ObjectMapper Initializers
  /// Map a JSON object to this class using ObjectMapper.
  ///
  /// - parameter map: A mapping from ObjectMapper.
  public required init?(map: Map){

  }

  /// Map a JSON object to this class using ObjectMapper.
  ///
  /// - parameter map: A mapping from ObjectMapper.
  public func mapping(map: Map) {
    statesId <- map[SerializationKeys.statesId]
    name <- map[SerializationKeys.name]
    countryId <- map[SerializationKeys.countryId]
  }

  /// Generates description of the object in the form of a NSDictionary.
  ///
  /// - returns: A Key value pair containing all valid values in the object.
  public func dictionaryRepresentation() -> [String: Any] {
    var dictionary: [String: Any] = [:]
    if let value = statesId { dictionary[SerializationKeys.statesId] = value }
    if let value = name { dictionary[SerializationKeys.name] = value }
    if let value = countryId { dictionary[SerializationKeys.countryId] = value }
    return dictionary
  }

}
