//
//  ModelReligionResult.swift
//
//  Created by Miral Kamani on 04/03/20
//  Copyright (c) . All rights reserved.
//

import Foundation
import ObjectMapper
public class ModelReligionResult: Mappable {

  // MARK: Declaration for string constants to be used to decode and also serialize.
  private struct SerializationKeys {
    static let religionName = "religion_name"
    static let religionDatetime = "religion_datetime"
    static let religionId = "religion_id"
  }

  // MARK: Properties
  public var religionName: String?
  public var religionDatetime: String?
  public var religionId: Int?

  // MARK: ObjectMapper Initializers
  /// Map a JSON object to this class using ObjectMapper.
  ///
  /// - parameter map: A mapping from ObjectMapper.
  public required init?(map: Map){

  }

  /// Map a JSON object to this class using ObjectMapper.
  ///
  /// - parameter map: A mapping from ObjectMapper.
  public func mapping(map: Map) {
    religionName <- map[SerializationKeys.religionName]
    religionDatetime <- map[SerializationKeys.religionDatetime]
    religionId <- map[SerializationKeys.religionId]
  }

  /// Generates description of the object in the form of a NSDictionary.
  ///
  /// - returns: A Key value pair containing all valid values in the object.
  public func dictionaryRepresentation() -> [String: Any] {
    var dictionary: [String: Any] = [:]
    if let value = religionName { dictionary[SerializationKeys.religionName] = value }
    if let value = religionDatetime { dictionary[SerializationKeys.religionDatetime] = value }
    if let value = religionId { dictionary[SerializationKeys.religionId] = value }
    return dictionary
  }

}
