//
//  ModelBaseGetAddressData.swift
//
//  Created by SocialInfotech 13 on 3/10/18
//  Copyright (c) Socialinfotech 13. All rights reserved.
//

import Foundation
import ObjectMapper

class ModelBaseGetAddressData: Mappable {

    // MARK: Declaration for string constants to be used to decode and also serialize.
	internal let kModelBaseGetAddressDataStatusKey: String = "status"
	internal let kModelBaseGetAddressDataPredictionsKey: String = "predictions"


    // MARK: Properties
	public var status: String?
	public var predictions: [ModelPredictions]?



    // MARK: ObjectMapper Initalizers
    /**
        Map a JSON object to this class using ObjectMapper
        - parameter map: A mapping from ObjectMapper
    */
    required convenience init?(map: Map) {
        self.init()
    }


    /**
        Map a JSON object to this class using ObjectMapper
        - parameter map: A mapping from ObjectMapper
    */
    func mapping(map: Map) {
		status <- map[kModelBaseGetAddressDataStatusKey]
		predictions <- map[kModelBaseGetAddressDataPredictionsKey]

    }

    /**
    Generates description of the object in the form of a NSDictionary.
    - returns: A Key value pair containing all valid values in the object.
    */
    public func dictionaryRepresentation() -> [String : AnyObject ] {

        var dictionary: [String : AnyObject ] = [ : ]
		if status != nil {
			dictionary.updateValue(status! as AnyObject, forKey: kModelBaseGetAddressDataStatusKey)
		}
		if predictions?.count != 0 {
			var temp: [AnyObject] = []
			for item in predictions! {
                temp.append(item.dictionaryRepresentation() as AnyObject)
			}
            dictionary.updateValue(temp as AnyObject, forKey: kModelBaseGetAddressDataPredictionsKey)
		}

        return dictionary
    }

}
