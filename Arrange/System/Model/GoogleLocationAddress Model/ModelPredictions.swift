//
//  ModelPredictions.swift
//
//  Created by SocialInfotech 13 on 3/10/18
//  Copyright (c) Socialinfotech 13. All rights reserved.
//

import Foundation
import ObjectMapper

class ModelPredictions: Mappable {

    // MARK: Declaration for string constants to be used to decode and also serialize.
	internal let kModelPredictionsMatchedSubstringsKey: String = "matched_substrings"
	internal let kModelPredictionsInternalIdentifierKey: String = "id"
	internal let kModelPredictionsPlaceIdKey: String = "place_id"
	internal let kModelPredictionsTermsKey: String = "terms"
	internal let kModelPredictionsReferenceKey: String = "reference"
	internal let kModelPredictionsDescriptionValueKey: String = "description"
	internal let kModelPredictionsTypesKey: String = "types"
	internal let kModelPredictionsStructuredFormattingKey: String = "structured_formatting"


    // MARK: Properties
	public var matchedSubstrings: [ModelMatchedSubstrings]?
	public var internalIdentifier: String?
	public var placeId: String?
	public var terms: [ModelTerms]?
	public var reference: String?
	public var descriptionValue: String?
	public var types: [ModelTypes]?
	public var structuredFormatting: ModelStructuredFormatting?



    // MARK: ObjectMapper Initalizers
    /**
        Map a JSON object to this class using ObjectMapper
        - parameter map: A mapping from ObjectMapper
    */
    required convenience init?(map: Map) {
        self.init()
    }


    /**
        Map a JSON object to this class using ObjectMapper
        - parameter map: A mapping from ObjectMapper
    */
    func mapping(map: Map) {
		matchedSubstrings <- map[kModelPredictionsMatchedSubstringsKey]
		internalIdentifier <- map[kModelPredictionsInternalIdentifierKey]
		placeId <- map[kModelPredictionsPlaceIdKey]
		terms <- map[kModelPredictionsTermsKey]
		reference <- map[kModelPredictionsReferenceKey]
		descriptionValue <- map[kModelPredictionsDescriptionValueKey]
		types <- map[kModelPredictionsTypesKey]
		structuredFormatting <- map[kModelPredictionsStructuredFormattingKey]

    }

    /**
    Generates description of the object in the form of a NSDictionary.
    - returns: A Key value pair containing all valid values in the object.
    */
    public func dictionaryRepresentation() -> [String : AnyObject ] {

        var dictionary: [String : AnyObject ] = [ : ]
		if matchedSubstrings?.count != 0 {
			var temp: [AnyObject] = []
			for item in matchedSubstrings! {
                temp.append(item.dictionaryRepresentation() as AnyObject)
			}
            dictionary.updateValue(temp as AnyObject, forKey: kModelPredictionsMatchedSubstringsKey)
		}
		if internalIdentifier != nil {
			dictionary.updateValue(internalIdentifier! as AnyObject, forKey: kModelPredictionsInternalIdentifierKey)
		}
		if placeId != nil {
			dictionary.updateValue(placeId! as AnyObject, forKey: kModelPredictionsPlaceIdKey)
		}
		if terms?.count != 0 {
			var temp: [AnyObject] = []
			for item in terms! {
                temp.append(item.dictionaryRepresentation() as AnyObject)
			}
            dictionary.updateValue(temp as AnyObject, forKey: kModelPredictionsTermsKey)
		}
		if reference != nil {
			dictionary.updateValue(reference! as AnyObject, forKey: kModelPredictionsReferenceKey)
		}
		if descriptionValue != nil {
			dictionary.updateValue(descriptionValue! as AnyObject, forKey: kModelPredictionsDescriptionValueKey)
		}
		if types?.count != 0 {
			var temp: [AnyObject] = []
			for item in types! {
                temp.append(item.dictionaryRepresentation() as AnyObject)
			}
            dictionary.updateValue(temp as AnyObject, forKey: kModelPredictionsTypesKey)
		}
		if structuredFormatting != nil {
            dictionary.updateValue(structuredFormatting!.dictionaryRepresentation() as AnyObject, forKey: kModelPredictionsStructuredFormattingKey)
		}

        return dictionary
    }

}
