//
//  ModelMainTextMatchedSubstrings.swift
//
//  Created by SocialInfotech 13 on 3/10/18
//  Copyright (c) Socialinfotech 13. All rights reserved.
//

import Foundation
import ObjectMapper

class ModelMainTextMatchedSubstrings: Mappable {

    // MARK: Declaration for string constants to be used to decode and also serialize.
	internal let kModelMainTextMatchedSubstringsLengthKey: String = "length"
	internal let kModelMainTextMatchedSubstringsOffsetKey: String = "offset"


    // MARK: Properties
	public var length: Int?
	public var offset: Int?



    // MARK: ObjectMapper Initalizers
    /**
        Map a JSON object to this class using ObjectMapper
        - parameter map: A mapping from ObjectMapper
    */
    required convenience init?(map: Map) {
        self.init()
    }


    /**
        Map a JSON object to this class using ObjectMapper
        - parameter map: A mapping from ObjectMapper
    */
    func mapping(map: Map) {
		length <- map[kModelMainTextMatchedSubstringsLengthKey]
		offset <- map[kModelMainTextMatchedSubstringsOffsetKey]

    }

    /**
    Generates description of the object in the form of a NSDictionary.
    - returns: A Key value pair containing all valid values in the object.
    */
    public func dictionaryRepresentation() -> [String : AnyObject ] {

        var dictionary: [String : AnyObject ] = [ : ]
		if length != nil {
			dictionary.updateValue(length! as AnyObject, forKey: kModelMainTextMatchedSubstringsLengthKey)
		}
		if offset != nil {
			dictionary.updateValue(offset! as AnyObject, forKey: kModelMainTextMatchedSubstringsOffsetKey)
		}

        return dictionary
    }

}
