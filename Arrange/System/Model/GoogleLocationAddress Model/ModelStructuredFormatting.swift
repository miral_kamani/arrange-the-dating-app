//
//  ModelStructuredFormatting.swift
//
//  Created by SocialInfotech 13 on 3/10/18
//  Copyright (c) Socialinfotech 13. All rights reserved.
//

import Foundation
import ObjectMapper

class ModelStructuredFormatting: Mappable {

    // MARK: Declaration for string constants to be used to decode and also serialize.
	internal let kModelStructuredFormattingMainTextKey: String = "main_text"
	internal let kModelStructuredFormattingMainTextMatchedSubstringsKey: String = "main_text_matched_substrings"
	internal let kModelStructuredFormattingSecondaryTextKey: String = "secondary_text"


    // MARK: Properties
	public var mainText: String?
	public var mainTextMatchedSubstrings: [ModelMainTextMatchedSubstrings]?
	public var secondaryText: String?



    // MARK: ObjectMapper Initalizers
    /**
        Map a JSON object to this class using ObjectMapper
        - parameter map: A mapping from ObjectMapper
    */
    required convenience init?(map: Map) {
        self.init()
    }


    /**
        Map a JSON object to this class using ObjectMapper
        - parameter map: A mapping from ObjectMapper
    */
    func mapping(map: Map) {
		mainText <- map[kModelStructuredFormattingMainTextKey]
		mainTextMatchedSubstrings <- map[kModelStructuredFormattingMainTextMatchedSubstringsKey]
		secondaryText <- map[kModelStructuredFormattingSecondaryTextKey]

    }

    /**
    Generates description of the object in the form of a NSDictionary.
    - returns: A Key value pair containing all valid values in the object.
    */
    public func dictionaryRepresentation() -> [String : AnyObject ] {

        var dictionary: [String : AnyObject ] = [ : ]
		if mainText != nil {
			dictionary.updateValue(mainText! as AnyObject, forKey: kModelStructuredFormattingMainTextKey)
		}
		if mainTextMatchedSubstrings?.count != 0 {
			var temp: [AnyObject] = []
			for item in mainTextMatchedSubstrings! {
                temp.append(item.dictionaryRepresentation() as AnyObject)
			}
            dictionary.updateValue(temp as AnyObject, forKey: kModelStructuredFormattingMainTextMatchedSubstringsKey)
		}
		if secondaryText != nil {
			dictionary.updateValue(secondaryText! as AnyObject, forKey: kModelStructuredFormattingSecondaryTextKey)
		}

        return dictionary
    }

}
