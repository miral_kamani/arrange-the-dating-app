//
//  ModelTerms.swift
//
//  Created by SocialInfotech 13 on 3/10/18
//  Copyright (c) Socialinfotech 13. All rights reserved.
//

import Foundation
import ObjectMapper

class ModelTerms: Mappable {

    // MARK: Declaration for string constants to be used to decode and also serialize.
	internal let kModelTermsOffsetKey: String = "offset"
	internal let kModelTermsValueKey: String = "value"


    // MARK: Properties
	public var offset: Int?
	public var value: String?



    // MARK: ObjectMapper Initalizers
    /**
        Map a JSON object to this class using ObjectMapper
        - parameter map: A mapping from ObjectMapper
    */
    required convenience init?(map: Map) {
        self.init()
    }


    /**
        Map a JSON object to this class using ObjectMapper
        - parameter map: A mapping from ObjectMapper
    */
    func mapping(map: Map) {
		offset <- map[kModelTermsOffsetKey]
		value <- map[kModelTermsValueKey]

    }

    /**
    Generates description of the object in the form of a NSDictionary.
    - returns: A Key value pair containing all valid values in the object.
    */
    public func dictionaryRepresentation() -> [String : AnyObject ] {

        var dictionary: [String : AnyObject ] = [ : ]
		if offset != nil {
			dictionary.updateValue(offset! as AnyObject, forKey: kModelTermsOffsetKey)
		}
		if value != nil {
			dictionary.updateValue(value! as AnyObject, forKey: kModelTermsValueKey)
		}

        return dictionary
    }

}
