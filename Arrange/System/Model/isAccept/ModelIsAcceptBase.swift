//
//  ModelIsAcceptBase.swift
//
//  Created by Miral Kamani on 16/03/20
//  Copyright (c) . All rights reserved.
//

import Foundation
import ObjectMapper
public class ModelIsAcceptBase: Mappable {

  // MARK: Declaration for string constants to be used to decode and also serialize.
  private struct SerializationKeys {
    static let error = "error"
    static let isAcceptResult = "Result"
    static let message = "message"
    static let success = "success"
  }

  // MARK: Properties
  public var error: Bool? = false
  public var isAcceptResult: [ModelIsAcceptResult]?
  public var message: String?
  public var success: Bool? = false

  // MARK: ObjectMapper Initializers
  /// Map a JSON object to this class using ObjectMapper.
  ///
  /// - parameter map: A mapping from ObjectMapper.
  public required init?(map: Map){

  }

  /// Map a JSON object to this class using ObjectMapper.
  ///
  /// - parameter map: A mapping from ObjectMapper.
  public func mapping(map: Map) {
    error <- map[SerializationKeys.error]
    isAcceptResult <- map[SerializationKeys.isAcceptResult]
    message <- map[SerializationKeys.message]
    success <- map[SerializationKeys.success]
  }

  /// Generates description of the object in the form of a NSDictionary.
  ///
  /// - returns: A Key value pair containing all valid values in the object.
  public func dictionaryRepresentation() -> [String: Any] {
    var dictionary: [String: Any] = [:]
    dictionary[SerializationKeys.error] = error
    if let value = isAcceptResult { dictionary[SerializationKeys.isAcceptResult] = value.map { $0.dictionaryRepresentation() } }
    if let value = message { dictionary[SerializationKeys.message] = value }
    dictionary[SerializationKeys.success] = success
    return dictionary
  }

}
