//
//  ModelChatMassageDetails.swift
//
//  Created by Miral Kamani on 14/03/20
//  Copyright (c) . All rights reserved.
//

import Foundation
import ObjectMapper
public class ModelChatMassageDetails: Mappable {

  // MARK: Declaration for string constants to be used to decode and also serialize.
  private struct SerializationKeys {
    static let chatMsgUpdate = "chat_msg_update"
    static let chatMsgType = "chat_msg_type"
    static let chatMsgText = "chat_msg_text"
  }

  // MARK: Properties
  public var chatMsgUpdate: String?
  public var chatMsgType: String?
  public var chatMsgText: String?

  // MARK: ObjectMapper Initializers
  /// Map a JSON object to this class using ObjectMapper.
  ///
  /// - parameter map: A mapping from ObjectMapper.
  public required init?(map: Map){

  }

  /// Map a JSON object to this class using ObjectMapper.
  ///
  /// - parameter map: A mapping from ObjectMapper.
  public func mapping(map: Map) {
    chatMsgUpdate <- map[SerializationKeys.chatMsgUpdate]
    chatMsgType <- map[SerializationKeys.chatMsgType]
    chatMsgText <- map[SerializationKeys.chatMsgText]
  }

  /// Generates description of the object in the form of a NSDictionary.
  ///
  /// - returns: A Key value pair containing all valid values in the object.
  public func dictionaryRepresentation() -> [String: Any] {
    var dictionary: [String: Any] = [:]
    if let value = chatMsgUpdate { dictionary[SerializationKeys.chatMsgUpdate] = value }
    if let value = chatMsgType { dictionary[SerializationKeys.chatMsgType] = value }
    if let value = chatMsgText { dictionary[SerializationKeys.chatMsgText] = value }
    return dictionary
  }

}
