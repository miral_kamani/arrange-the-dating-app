//
//  ModelChatResult.swift
//
//  Created by Miral Kamani on 14/03/20
//  Copyright (c) . All rights reserved.
//

import Foundation
import ObjectMapper
public class ModelChatResult: Mappable {

  // MARK: Declaration for string constants to be used to decode and also serialize.
  private struct SerializationKeys {
    static let chatRoomId = "chat_room_id"
    static let chatRoomDatetime = "chat_room_datetime"
    static let senderRefId = "sender_ref_id"
    static let userDetails = "user_details"
    static let receiverRefId = "receiver_ref_id"
    static let isAccepted = "is_accepted"
  }

  // MARK: Properties
  public var chatRoomId: Int?
  public var chatRoomDatetime: String?
  public var senderRefId: Int?
  public var userDetails: ModelChatUserDetails?
  public var receiverRefId: Int?
  public var isAccepted: Int?

  // MARK: ObjectMapper Initializers
  /// Map a JSON object to this class using ObjectMapper.
  ///
  /// - parameter map: A mapping from ObjectMapper.
  public required init?(map: Map){

  }

  /// Map a JSON object to this class using ObjectMapper.
  ///
  /// - parameter map: A mapping from ObjectMapper.
  public func mapping(map: Map) {
    chatRoomId <- map[SerializationKeys.chatRoomId]
    chatRoomDatetime <- map[SerializationKeys.chatRoomDatetime]
    senderRefId <- map[SerializationKeys.senderRefId]
    userDetails <- map[SerializationKeys.userDetails]
    receiverRefId <- map[SerializationKeys.receiverRefId]
    isAccepted <- map[SerializationKeys.isAccepted]
  }

  /// Generates description of the object in the form of a NSDictionary.
  ///
  /// - returns: A Key value pair containing all valid values in the object.
  public func dictionaryRepresentation() -> [String: Any] {
    var dictionary: [String: Any] = [:]
    if let value = chatRoomId { dictionary[SerializationKeys.chatRoomId] = value }
    if let value = chatRoomDatetime { dictionary[SerializationKeys.chatRoomDatetime] = value }
    if let value = senderRefId { dictionary[SerializationKeys.senderRefId] = value }
    if let value = userDetails { dictionary[SerializationKeys.userDetails] = value.dictionaryRepresentation() }
    if let value = receiverRefId { dictionary[SerializationKeys.receiverRefId] = value }
    if let value = isAccepted { dictionary[SerializationKeys.isAccepted] = value }
    return dictionary
  }

}
