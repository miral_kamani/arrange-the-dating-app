//
//  ModelUserDetails.swift
//
//  Created by Miral Kamani on 14/03/20
//  Copyright (c) . All rights reserved.
//

import Foundation
import ObjectMapper
public class ModelChatUserDetails: Mappable {

  // MARK: Declaration for string constants to be used to decode and also serialize.
  private struct SerializationKeys {
    static let careerName = "career_name"
    static let userProfileId = "user_profile_id"
    static let userProfileImage = "user_profile_image"
    static let chatMassageDetails = "Chat_massage_details"
    static let userProfileAge = "user_profile_age"
    static let refUserId = "ref_user_id"
    static let userProfileName = "user_profile_name"
  }

  // MARK: Properties
  public var careerName: String?
  public var userProfileId: Int?
  public var userProfileImage: String?
  public var chatMassageDetails: ModelChatMassageDetails?
  public var userProfileAge: Int?
  public var refUserId: Int?
  public var userProfileName: String?

  // MARK: ObjectMapper Initializers
  /// Map a JSON object to this class using ObjectMapper.
  ///
  /// - parameter map: A mapping from ObjectMapper.
  public required init?(map: Map){

  }

  /// Map a JSON object to this class using ObjectMapper.
  ///
  /// - parameter map: A mapping from ObjectMapper.
  public func mapping(map: Map) {
    careerName <- map[SerializationKeys.careerName]
    userProfileId <- map[SerializationKeys.userProfileId]
    userProfileImage <- map[SerializationKeys.userProfileImage]
    chatMassageDetails <- map[SerializationKeys.chatMassageDetails]
    userProfileAge <- map[SerializationKeys.userProfileAge]
    refUserId <- map[SerializationKeys.refUserId]
    userProfileName <- map[SerializationKeys.userProfileName]
  }

  /// Generates description of the object in the form of a NSDictionary.
  ///
  /// - returns: A Key value pair containing all valid values in the object.
  public func dictionaryRepresentation() -> [String: Any] {
    var dictionary: [String: Any] = [:]
    if let value = careerName { dictionary[SerializationKeys.careerName] = value }
    if let value = userProfileId { dictionary[SerializationKeys.userProfileId] = value }
    if let value = userProfileImage { dictionary[SerializationKeys.userProfileImage] = value }
    if let value = chatMassageDetails { dictionary[SerializationKeys.chatMassageDetails] = value.dictionaryRepresentation() }
    if let value = userProfileAge { dictionary[SerializationKeys.userProfileAge] = value }
    if let value = refUserId { dictionary[SerializationKeys.refUserId] = value }
    if let value = userProfileName { dictionary[SerializationKeys.userProfileName] = value }
    return dictionary
  }

}
