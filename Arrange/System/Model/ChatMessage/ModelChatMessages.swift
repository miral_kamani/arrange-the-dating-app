//
//  ModelChatMessages.swift
//
//  Created by Miral Kamani on 16/03/20
//  Copyright (c) . All rights reserved.
//

import Foundation
import ObjectMapper
public class ModelChatMessages: Mappable {

  // MARK: Declaration for string constants to be used to decode and also serialize.
  private struct SerializationKeys {
    static let chatMsgText = "chat_msg_text"
    static let chatMsgId = "chat_msg_id"
    static let isNew = "is_new"
    static let chatMsgDatetime = "chat_msg_datetime"
    static let isRead = "is_read"
    static let userProfileImage = "user_profile_image"
    static let chatMsgUpdate = "chat_msg_update"
    static let chatMsgType = "chat_msg_type"
    static let msgSenderId = "msg_sender_id"
    static let refChatroomId = "ref_chatroom_id"
    static let msgReceiverId = "msg_receiver_id"
    static let userProfileName = "user_profile_name"
  }

  // MARK: Properties
  public var chatMsgText: String?
  public var chatMsgId: Int?
  public var isNew: String?
  public var chatMsgDatetime: String?
  public var isRead: String?
  public var userProfileImage: String?
  public var chatMsgUpdate: String?
  public var chatMsgType: String?
  public var msgSenderId: Int?
  public var refChatroomId: Int?
  public var msgReceiverId: Int?
  public var userProfileName: String?

  // MARK: ObjectMapper Initializers
  /// Map a JSON object to this class using ObjectMapper.
  ///
  /// - parameter map: A mapping from ObjectMapper.
  public required init?(map: Map){

  }

  /// Map a JSON object to this class using ObjectMapper.
  ///
  /// - parameter map: A mapping from ObjectMapper.
  public func mapping(map: Map) {
    chatMsgText <- map[SerializationKeys.chatMsgText]
    chatMsgId <- map[SerializationKeys.chatMsgId]
    isNew <- map[SerializationKeys.isNew]
    chatMsgDatetime <- map[SerializationKeys.chatMsgDatetime]
    isRead <- map[SerializationKeys.isRead]
    userProfileImage <- map[SerializationKeys.userProfileImage]
    chatMsgUpdate <- map[SerializationKeys.chatMsgUpdate]
    chatMsgType <- map[SerializationKeys.chatMsgType]
    msgSenderId <- map[SerializationKeys.msgSenderId]
    refChatroomId <- map[SerializationKeys.refChatroomId]
    msgReceiverId <- map[SerializationKeys.msgReceiverId]
    userProfileName <- map[SerializationKeys.userProfileName]
  }

  /// Generates description of the object in the form of a NSDictionary.
  ///
  /// - returns: A Key value pair containing all valid values in the object.
  public func dictionaryRepresentation() -> [String: Any] {
    var dictionary: [String: Any] = [:]
    if let value = chatMsgText { dictionary[SerializationKeys.chatMsgText] = value }
    if let value = chatMsgId { dictionary[SerializationKeys.chatMsgId] = value }
    if let value = isNew { dictionary[SerializationKeys.isNew] = value }
    if let value = chatMsgDatetime { dictionary[SerializationKeys.chatMsgDatetime] = value }
    if let value = isRead { dictionary[SerializationKeys.isRead] = value }
    if let value = userProfileImage { dictionary[SerializationKeys.userProfileImage] = value }
    if let value = chatMsgUpdate { dictionary[SerializationKeys.chatMsgUpdate] = value }
    if let value = chatMsgType { dictionary[SerializationKeys.chatMsgType] = value }
    if let value = msgSenderId { dictionary[SerializationKeys.msgSenderId] = value }
    if let value = refChatroomId { dictionary[SerializationKeys.refChatroomId] = value }
    if let value = msgReceiverId { dictionary[SerializationKeys.msgReceiverId] = value }
    if let value = userProfileName { dictionary[SerializationKeys.userProfileName] = value }
    return dictionary
  }

}
