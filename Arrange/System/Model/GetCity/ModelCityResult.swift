//
//  ModelCityResult.swift
//
//  Created by Miral Kamani on 12/03/20
//  Copyright (c) . All rights reserved.
//

import Foundation
import ObjectMapper
public class ModelCityResult: Mappable {

  // MARK: Declaration for string constants to be used to decode and also serialize.
  private struct SerializationKeys {
    static let citiesId = "cities_id"
    static let name = "name"
    static let stateId = "state_id"
  }

  // MARK: Properties
  public var citiesId: Int?
  public var name: String?
  public var stateId: Int?

  // MARK: ObjectMapper Initializers
  /// Map a JSON object to this class using ObjectMapper.
  ///
  /// - parameter map: A mapping from ObjectMapper.
  public required init?(map: Map){

  }

  /// Map a JSON object to this class using ObjectMapper.
  ///
  /// - parameter map: A mapping from ObjectMapper.
  public func mapping(map: Map) {
    citiesId <- map[SerializationKeys.citiesId]
    name <- map[SerializationKeys.name]
    stateId <- map[SerializationKeys.stateId]
  }

  /// Generates description of the object in the form of a NSDictionary.
  ///
  /// - returns: A Key value pair containing all valid values in the object.
  public func dictionaryRepresentation() -> [String: Any] {
    var dictionary: [String: Any] = [:]
    if let value = citiesId { dictionary[SerializationKeys.citiesId] = value }
    if let value = name { dictionary[SerializationKeys.name] = value }
    if let value = stateId { dictionary[SerializationKeys.stateId] = value }
    return dictionary
  }

}
