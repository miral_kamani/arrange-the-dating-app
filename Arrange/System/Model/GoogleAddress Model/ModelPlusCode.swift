//
//  ModelPlusCode.swift
//
//  Created by SocialInfotech 13 on 11/16/18
//  Copyright (c) . All rights reserved.
//

import Foundation
import ObjectMapper

class ModelPlusCode: Mappable {

    // MARK: Declaration for string constants to be used to decode and also serialize.
	internal let kModelPlusCodeGlobalCodeKey: String = "global_code"
	internal let kModelPlusCodeCompoundCodeKey: String = "compound_code"


    // MARK: Properties
	public var globalCode: String?
	public var compoundCode: String?



    // MARK: ObjectMapper Initalizers
    /**
        Map a JSON object to this class using ObjectMapper
        - parameter map: A mapping from ObjectMapper
    */
    required convenience init?(map: Map) {
        self.init()
    }


    /**
        Map a JSON object to this class using ObjectMapper
        - parameter map: A mapping from ObjectMapper
    */
    func mapping(map: Map) {
		globalCode <- map[kModelPlusCodeGlobalCodeKey]
		compoundCode <- map[kModelPlusCodeCompoundCodeKey]

    }

    /**
    Generates description of the object in the form of a NSDictionary.
    - returns: A Key value pair containing all valid values in the object.
    */
    public func dictionaryRepresentation() -> [String : AnyObject ] {

        var dictionary: [String : AnyObject ] = [ : ]
		if globalCode != nil {
			dictionary.updateValue(globalCode! as AnyObject, forKey: kModelPlusCodeGlobalCodeKey)
		}
		if compoundCode != nil {
			dictionary.updateValue(compoundCode! as AnyObject, forKey: kModelPlusCodeCompoundCodeKey)
		}

        return dictionary
    }

}
