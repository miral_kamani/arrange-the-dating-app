//
//  ModelBounds.swift
//
//  Created by SocialInfotech 13 on 11/16/18
//  Copyright (c) . All rights reserved.
//

import Foundation
import ObjectMapper

class ModelBounds: Mappable {

    // MARK: Declaration for string constants to be used to decode and also serialize.
	internal let kModelBoundsNortheastKey: String = "northeast"
	internal let kModelBoundsSouthwestKey: String = "southwest"


    // MARK: Properties
	public var northeast: ModelNortheast?
	public var southwest: ModelSouthwest?



    // MARK: ObjectMapper Initalizers
    /**
        Map a JSON object to this class using ObjectMapper
        - parameter map: A mapping from ObjectMapper
    */
    required convenience init?(map: Map) {
        self.init()
    }


    /**
        Map a JSON object to this class using ObjectMapper
        - parameter map: A mapping from ObjectMapper
    */
    func mapping(map: Map) {
		northeast <- map[kModelBoundsNortheastKey]
		southwest <- map[kModelBoundsSouthwestKey]

    }

    /**
    Generates description of the object in the form of a NSDictionary.
    - returns: A Key value pair containing all valid values in the object.
    */
    public func dictionaryRepresentation() -> [String : AnyObject ] {

        var dictionary: [String : AnyObject ] = [ : ]
		if northeast != nil {
            dictionary.updateValue(northeast!.dictionaryRepresentation() as AnyObject, forKey: kModelBoundsNortheastKey)
		}
		if southwest != nil {
            dictionary.updateValue(southwest!.dictionaryRepresentation() as AnyObject, forKey: kModelBoundsSouthwestKey)
		}

        return dictionary
    }

}
