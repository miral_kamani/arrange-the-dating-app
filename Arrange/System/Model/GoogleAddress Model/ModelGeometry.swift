//
//  ModelGeometry.swift
//
//  Created by SocialInfotech 13 on 11/16/18
//  Copyright (c) . All rights reserved.
//

import Foundation
import ObjectMapper

class ModelGeometry: Mappable {

    // MARK: Declaration for string constants to be used to decode and also serialize.
	internal let kModelGeometryLocationKey: String = "location"
	internal let kModelGeometryBoundsKey: String = "bounds"
	internal let kModelGeometryViewportKey: String = "viewport"
	internal let kModelGeometryLocationTypeKey: String = "location_type"


    // MARK: Properties
	public var location: ModelLocation?
	public var bounds: ModelBounds?
	public var viewport: ModelViewport?
	public var locationType: String?



    // MARK: ObjectMapper Initalizers
    /**
        Map a JSON object to this class using ObjectMapper
        - parameter map: A mapping from ObjectMapper
    */
    required convenience init?(map: Map) {
        self.init()
    }


    /**
        Map a JSON object to this class using ObjectMapper
        - parameter map: A mapping from ObjectMapper
    */
    func mapping(map: Map) {
		location <- map[kModelGeometryLocationKey]
		bounds <- map[kModelGeometryBoundsKey]
		viewport <- map[kModelGeometryViewportKey]
		locationType <- map[kModelGeometryLocationTypeKey]
    }

    /**
    Generates description of the object in the form of a NSDictionary.
    - returns: A Key value pair containing all valid values in the object.
    */
    public func dictionaryRepresentation() -> [String : AnyObject ] {

        var dictionary: [String : AnyObject ] = [ : ]
		if location != nil {
            dictionary.updateValue(location!.dictionaryRepresentation() as AnyObject, forKey: kModelGeometryLocationKey)
		}
		if bounds != nil {
            dictionary.updateValue(bounds!.dictionaryRepresentation() as AnyObject, forKey: kModelGeometryBoundsKey)
		}
		if viewport != nil {
            dictionary.updateValue(viewport!.dictionaryRepresentation() as AnyObject, forKey: kModelGeometryViewportKey)
		}
		if locationType != nil {
			dictionary.updateValue(locationType! as AnyObject, forKey: kModelGeometryLocationTypeKey)
		}

        return dictionary
    }

}
