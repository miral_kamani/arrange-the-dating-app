//
//  ModelBaseGoogleAddress.swift
//
//  Created by SocialInfotech 13 on 11/16/18
//  Copyright (c) . All rights reserved.
//

import Foundation
import ObjectMapper

class ModelBaseGoogleAddress: Mappable {

    // MARK: Declaration for string constants to be used to decode and also serialize.
	internal let kModelBaseGoogleAddressStatusKey: String = "status"
	internal let kModelBaseGoogleAddressPlusCodeKey: String = "plus_code"
	internal let kModelBaseGoogleAddressGoogleAddressresultsKey: String = "results"


    // MARK: Properties
	public var status: String?
	public var plusCode: ModelPlusCode?
	public var googleAddressresults: [ModelGoogleAddressresults]?



    // MARK: ObjectMapper Initalizers
    /**
        Map a JSON object to this class using ObjectMapper
        - parameter map: A mapping from ObjectMapper
    */
    required convenience init?(map: Map) {
        self.init()
    }


    /**
        Map a JSON object to this class using ObjectMapper
        - parameter map: A mapping from ObjectMapper
    */
    func mapping(map: Map) {
		status <- map[kModelBaseGoogleAddressStatusKey]
		plusCode <- map[kModelBaseGoogleAddressPlusCodeKey]
		googleAddressresults <- map[kModelBaseGoogleAddressGoogleAddressresultsKey]

    }

    /**
    Generates description of the object in the form of a NSDictionary.
    - returns: A Key value pair containing all valid values in the object.
    */
    public func dictionaryRepresentation() -> [String : AnyObject ] {

        var dictionary: [String : AnyObject ] = [ : ]
		if status != nil {
			dictionary.updateValue(status! as AnyObject, forKey: kModelBaseGoogleAddressStatusKey)
		}
		if plusCode != nil {
            dictionary.updateValue(plusCode!.dictionaryRepresentation() as AnyObject, forKey: kModelBaseGoogleAddressPlusCodeKey)
		}
		if googleAddressresults?.count != 0 {
			var temp: [AnyObject] = []
			for item in googleAddressresults! {
                temp.append(item.dictionaryRepresentation() as AnyObject)
			}
            dictionary.updateValue(temp as AnyObject, forKey: kModelBaseGoogleAddressGoogleAddressresultsKey)
		}

        return dictionary
    }

}
