//
//  ModelGoogleAddressresults.swift
//
//  Created by SocialInfotech 13 on 11/16/18
//  Copyright (c) . All rights reserved.
//

import Foundation
import ObjectMapper

class ModelGoogleAddressresults: Mappable {

    // MARK: Declaration for string constants to be used to decode and also serialize.
	internal let kModelGoogleAddressresultsPlusCodeKey: String = "plus_code"
	internal let kModelGoogleAddressresultsTypesKey: String = "types"
	internal let kModelGoogleAddressresultsAddressComponentsKey: String = "address_components"
	internal let kModelGoogleAddressresultsPlaceIdKey: String = "place_id"
	internal let kModelGoogleAddressresultsGeometryKey: String = "geometry"
	internal let kModelGoogleAddressresultsFormattedAddressKey: String = "formatted_address"


    // MARK: Properties
	public var plusCode: ModelPlusCode?
	public var types: [ModelTypes]?
	public var addressComponents: [ModelAddressComponents]?
	public var placeId: String?
	public var geometry: ModelGeometry?
	public var formattedAddress: String?



    // MARK: ObjectMapper Initalizers
    /**
        Map a JSON object to this class using ObjectMapper
        - parameter map: A mapping from ObjectMapper
    */
    required convenience init?(map: Map) {
        self.init()
    }


    /**
        Map a JSON object to this class using ObjectMapper
        - parameter map: A mapping from ObjectMapper
    */
    func mapping(map: Map) {
		plusCode <- map[kModelGoogleAddressresultsPlusCodeKey]
		types <- map[kModelGoogleAddressresultsTypesKey]
		addressComponents <- map[kModelGoogleAddressresultsAddressComponentsKey]
		placeId <- map[kModelGoogleAddressresultsPlaceIdKey]
		geometry <- map[kModelGoogleAddressresultsGeometryKey]
		formattedAddress <- map[kModelGoogleAddressresultsFormattedAddressKey]

    }

    /**
    Generates description of the object in the form of a NSDictionary.
    - returns: A Key value pair containing all valid values in the object.
    */
    public func dictionaryRepresentation() -> [String : AnyObject ] {

        var dictionary: [String : AnyObject ] = [ : ]
		if plusCode != nil {
            dictionary.updateValue(plusCode!.dictionaryRepresentation() as AnyObject, forKey: kModelGoogleAddressresultsPlusCodeKey)
		}
		if types?.count != 0 {
			var temp: [AnyObject] = []
			for item in types! {
                temp.append(item.dictionaryRepresentation() as AnyObject)
			}
            dictionary.updateValue(temp as AnyObject, forKey: kModelGoogleAddressresultsTypesKey)
		}
		if addressComponents?.count != 0 {
			var temp: [AnyObject] = []
			for item in addressComponents! {
                temp.append(item.dictionaryRepresentation() as AnyObject)
			}
            dictionary.updateValue(temp as AnyObject, forKey: kModelGoogleAddressresultsAddressComponentsKey)
		}
		if placeId != nil {
			dictionary.updateValue(placeId! as AnyObject, forKey: kModelGoogleAddressresultsPlaceIdKey)
		}
		if geometry != nil {
            dictionary.updateValue(geometry!.dictionaryRepresentation() as AnyObject, forKey: kModelGoogleAddressresultsGeometryKey)
		}
		if formattedAddress != nil {
			dictionary.updateValue(formattedAddress! as AnyObject, forKey: kModelGoogleAddressresultsFormattedAddressKey)
		}

        return dictionary
    }

}
