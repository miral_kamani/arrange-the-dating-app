//
//  ModelAddressComponents.swift
//
//  Created by SocialInfotech 13 on 11/16/18
//  Copyright (c) . All rights reserved.
//

import Foundation
import ObjectMapper

class ModelAddressComponents: Mappable {

    // MARK: Declaration for string constants to be used to decode and also serialize.
	internal let kModelAddressComponentsShortNameKey: String = "short_name"
	internal let kModelAddressComponentsTypesKey: String = "types"
	internal let kModelAddressComponentsLongNameKey: String = "long_name"


    // MARK: Properties
	public var shortName: String?
	public var types: [String]?
	public var longName: String?



    // MARK: ObjectMapper Initalizers
    /**
        Map a JSON object to this class using ObjectMapper
        - parameter map: A mapping from ObjectMapper
    */
    required convenience init?(map: Map) {
        self.init()
    }


    /**
        Map a JSON object to this class using ObjectMapper
        - parameter map: A mapping from ObjectMapper
    */
    func mapping(map: Map) {
		shortName <- map[kModelAddressComponentsShortNameKey]
		types <- map[kModelAddressComponentsTypesKey]
		longName <- map[kModelAddressComponentsLongNameKey]

    }

    /**
    Generates description of the object in the form of a NSDictionary.
    - returns: A Key value pair containing all valid values in the object.
    */
    public func dictionaryRepresentation() -> [String : AnyObject ] {

        var dictionary: [String : AnyObject ] = [ : ]
		if shortName != nil {
			dictionary.updateValue(shortName! as AnyObject, forKey: kModelAddressComponentsShortNameKey)
		}
		if types?.count != 0 {
			var temp: [AnyObject] = []
			for item in types! {
                temp.append(item as AnyObject)
			}
            dictionary.updateValue(temp as AnyObject, forKey: kModelAddressComponentsTypesKey)
		}
		if longName != nil {
			dictionary.updateValue(longName! as AnyObject, forKey: kModelAddressComponentsLongNameKey)
		}

        return dictionary
    }

}
