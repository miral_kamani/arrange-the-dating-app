//
//  ModelSouthwest.swift
//
//  Created by SocialInfotech 13 on 11/16/18
//  Copyright (c) . All rights reserved.
//

import Foundation
import ObjectMapper

class ModelSouthwest: Mappable {

    // MARK: Declaration for string constants to be used to decode and also serialize.
	internal let kModelSouthwestLatKey: String = "lat"
	internal let kModelSouthwestLngKey: String = "lng"


    // MARK: Properties
	public var lat: Float?
	public var lng: Float?



    // MARK: ObjectMapper Initalizers
    /**
        Map a JSON object to this class using ObjectMapper
        - parameter map: A mapping from ObjectMapper
    */
    required convenience init?(map: Map) {
        self.init()
    }


    /**
        Map a JSON object to this class using ObjectMapper
        - parameter map: A mapping from ObjectMapper
    */
    func mapping(map: Map) {
		lat <- map[kModelSouthwestLatKey]
		lng <- map[kModelSouthwestLngKey]

    }

    /**
    Generates description of the object in the form of a NSDictionary.
    - returns: A Key value pair containing all valid values in the object.
    */
    public func dictionaryRepresentation() -> [String : AnyObject ] {

        var dictionary: [String : AnyObject ] = [ : ]
		if lat != nil {
			dictionary.updateValue(lat! as AnyObject, forKey: kModelSouthwestLatKey)
		}
		if lng != nil {
			dictionary.updateValue(lng! as AnyObject, forKey: kModelSouthwestLngKey)
		}

        return dictionary
    }

}
