//
//  ModelUserImageResult.swift
//
//  Created by Miral Kamani on 05/03/20
//  Copyright (c) . All rights reserved.
//

import Foundation
import ObjectMapper
public class ModelUserImageResult: Mappable {

  // MARK: Declaration for string constants to be used to decode and also serialize.
  private struct SerializationKeys {
    static let userImagesDatetime = "user_images_datetime"
    static let userImagesId = "user_images_id"
    static let userImages = "user_images"
    static let refUserId = "ref_user_id"
  }

  // MARK: Properties
  public var userImagesDatetime: String?
  public var userImagesId: Int?
  public var userImages: String?
  public var refUserId: Int?

  // MARK: ObjectMapper Initializers
  /// Map a JSON object to this class using ObjectMapper.
  ///
  /// - parameter map: A mapping from ObjectMapper.
  public required init?(map: Map){

  }

  /// Map a JSON object to this class using ObjectMapper.
  ///
  /// - parameter map: A mapping from ObjectMapper.
  public func mapping(map: Map) {
    userImagesDatetime <- map[SerializationKeys.userImagesDatetime]
    userImagesId <- map[SerializationKeys.userImagesId]
    userImages <- map[SerializationKeys.userImages]
    refUserId <- map[SerializationKeys.refUserId]
  }

  /// Generates description of the object in the form of a NSDictionary.
  ///
  /// - returns: A Key value pair containing all valid values in the object.
  public func dictionaryRepresentation() -> [String: Any] {
    var dictionary: [String: Any] = [:]
    if let value = userImagesDatetime { dictionary[SerializationKeys.userImagesDatetime] = value }
    if let value = userImagesId { dictionary[SerializationKeys.userImagesId] = value }
    if let value = userImages { dictionary[SerializationKeys.userImages] = value }
    if let value = refUserId { dictionary[SerializationKeys.refUserId] = value }
    return dictionary
  }

}
