//
//  ModelCountryBase.swift
//
//  Created by Miral Kamani on 11/03/20
//  Copyright (c) . All rights reserved.
//

import Foundation
import ObjectMapper
public class ModelCountryBase: Mappable {

  // MARK: Declaration for string constants to be used to decode and also serialize.
  private struct SerializationKeys {
    static let success = "success"
    static let error = "error"
    static let message = "message"
    static let countryResult = "Result"
  }

  // MARK: Properties
  public var success: Bool? = false
  public var error: Bool? = false
  public var message: String?
  public var countryResult: [ModelCountryResult]?

  // MARK: ObjectMapper Initializers
  /// Map a JSON object to this class using ObjectMapper.
  ///
  /// - parameter map: A mapping from ObjectMapper.
  public required init?(map: Map){

  }

  /// Map a JSON object to this class using ObjectMapper.
  ///
  /// - parameter map: A mapping from ObjectMapper.
  public func mapping(map: Map) {
    success <- map[SerializationKeys.success]
    error <- map[SerializationKeys.error]
    message <- map[SerializationKeys.message]
    countryResult <- map[SerializationKeys.countryResult]
  }

  /// Generates description of the object in the form of a NSDictionary.
  ///
  /// - returns: A Key value pair containing all valid values in the object.
  public func dictionaryRepresentation() -> [String: Any] {
    var dictionary: [String: Any] = [:]
    dictionary[SerializationKeys.success] = success
    dictionary[SerializationKeys.error] = error
    if let value = message { dictionary[SerializationKeys.message] = value }
    if let value = countryResult { dictionary[SerializationKeys.countryResult] = value.map { $0.dictionaryRepresentation() } }
    return dictionary
  }

}
