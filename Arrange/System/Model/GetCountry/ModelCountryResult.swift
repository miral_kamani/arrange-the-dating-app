//
//  ModelCountryResult.swift
//
//  Created by Miral Kamani on 11/03/20
//  Copyright (c) . All rights reserved.
//

import Foundation
import ObjectMapper
public class ModelCountryResult: Mappable {

  // MARK: Declaration for string constants to be used to decode and also serialize.
  private struct SerializationKeys {
    static let phonecode = "phonecode"
    static let name = "name"
    static let countriesId = "countries_id"
    static let sortname = "sortname"
  }

  // MARK: Properties
  public var phonecode: Int?
  public var name: String?
  public var countriesId: Int?
  public var sortname: String?

  // MARK: ObjectMapper Initializers
  /// Map a JSON object to this class using ObjectMapper.
  ///
  /// - parameter map: A mapping from ObjectMapper.
  public required init?(map: Map){

  }

  /// Map a JSON object to this class using ObjectMapper.
  ///
  /// - parameter map: A mapping from ObjectMapper.
  public func mapping(map: Map) {
    phonecode <- map[SerializationKeys.phonecode]
    name <- map[SerializationKeys.name]
    countriesId <- map[SerializationKeys.countriesId]
    sortname <- map[SerializationKeys.sortname]
  }

  /// Generates description of the object in the form of a NSDictionary.
  ///
  /// - returns: A Key value pair containing all valid values in the object.
  public func dictionaryRepresentation() -> [String: Any] {
    var dictionary: [String: Any] = [:]
    if let value = phonecode { dictionary[SerializationKeys.phonecode] = value }
    if let value = name { dictionary[SerializationKeys.name] = value }
    if let value = countriesId { dictionary[SerializationKeys.countriesId] = value }
    if let value = sortname { dictionary[SerializationKeys.sortname] = value }
    return dictionary
  }

}
