//
//  ModelMatchResult.swift
//
//  Created by Miral Kamani on 12/03/20
//  Copyright (c) . All rights reserved.
//

import Foundation
import ObjectMapper
public class ModelMatchResult: Mappable {

  // MARK: Declaration for string constants to be used to decode and also serialize.
  private struct SerializationKeys {
    static let likesDatetime = "likes_datetime"
    static let likesId = "likes_id"
    static let isLiked = "is_liked"
    static let refUserId = "ref_user_id"
    static let refToUserId = "ref_to_user_id"
    static let userDetails = "user_details"
  }

  // MARK: Properties
  public var likesDatetime: String?
  public var likesId: Int?
  public var isLiked: Int?
  public var refUserId: Int?
  public var refToUserId: Int?
  public var userDetails: ModelUserDetails?

  // MARK: ObjectMapper Initializers
  /// Map a JSON object to this class using ObjectMapper.
  ///
  /// - parameter map: A mapping from ObjectMapper.
  public required init?(map: Map){

  }

  /// Map a JSON object to this class using ObjectMapper.
  ///
  /// - parameter map: A mapping from ObjectMapper.
  public func mapping(map: Map) {
    likesDatetime <- map[SerializationKeys.likesDatetime]
    likesId <- map[SerializationKeys.likesId]
    isLiked <- map[SerializationKeys.isLiked]
    refUserId <- map[SerializationKeys.refUserId]
    refToUserId <- map[SerializationKeys.refToUserId]
    userDetails <- map[SerializationKeys.userDetails]
  }

  /// Generates description of the object in the form of a NSDictionary.
  ///
  /// - returns: A Key value pair containing all valid values in the object.
  public func dictionaryRepresentation() -> [String: Any] {
    var dictionary: [String: Any] = [:]
    if let value = likesDatetime { dictionary[SerializationKeys.likesDatetime] = value }
    if let value = likesId { dictionary[SerializationKeys.likesId] = value }
    if let value = isLiked { dictionary[SerializationKeys.isLiked] = value }
    if let value = refUserId { dictionary[SerializationKeys.refUserId] = value }
    if let value = refToUserId { dictionary[SerializationKeys.refToUserId] = value }
    if let value = userDetails { dictionary[SerializationKeys.userDetails] = value.dictionaryRepresentation() }
    return dictionary
  }

}
