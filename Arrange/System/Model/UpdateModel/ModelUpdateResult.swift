//
//  ModelUpdateResult.swift
//
//  Created by Miral Kamani on 07/03/20
//  Copyright (c) . All rights reserved.
//

import Foundation
import ObjectMapper
public class ModelUpdateResult: Mappable {

  // MARK: Declaration for string constants to be used to decode and also serialize.
  private struct SerializationKeys {
    static let userDatetime = "user_datetime"
    static let userEmail = "user_email"
    static let userDevice = "user_device"
    static let refUserId = "ref_user_id"
    static let userSocialId = "user_social_id"
    static let refCareerId = "ref_career_id"
    static let userLoginType = "user_login_type"
    static let userProfileEducation = "user_profile_education"
    static let userProfileId = "user_profile_id"
    static let religionName = "religion_name"
    static let communityName = "community_name"
    static let userProfileName = "user_profile_name"
    static let userProfileGender = "user_profile_gender"
    static let userApi = "user_api"
    static let refReligionId = "ref_religion_id"
    static let userProfileAge = "user_profile_age"
    static let userProfileAbout = "user_profile_about"
    static let userProfileDob = "user_profile_dob"
    static let careerName = "career_name"
    static let refRaisedInId = "ref_raised_in_id"
    static let userProfileHeight = "user_profile_height"
    static let userProfileImage = "user_profile_image"
    static let refCommunityId = "ref_community_id"
    static let userProfileDatetime = "user_profile_datetime"
    static let userToken = "user_token"
    static let userId = "user_id"
    static let raisedInName = "raised_in_name"
    static let userProfileHeadline = "user_profile_headline"
  }

  // MARK: Properties
  public var userDatetime: String?
  public var userEmail: String?
  public var userDevice: String?
  public var refUserId: Int?
  public var userSocialId: String?
  public var refCareerId: Int?
  public var userLoginType: String?
  public var userProfileEducation: String?
  public var userProfileId: Int?
  public var religionName: String?
  public var communityName: String?
  public var userProfileName: String?
  public var userProfileGender: String?
  public var userApi: String?
  public var refReligionId: Int?
  public var userProfileAge: Int?
  public var userProfileAbout: String?
  public var userProfileDob: String?
  public var careerName: String?
  public var refRaisedInId: Int?
  public var userProfileHeight: String?
  public var userProfileImage: String?
  public var refCommunityId: Int?
  public var userProfileDatetime: String?
  public var userToken: String?
  public var userId: Int?
  public var raisedInName: String?
  public var userProfileHeadline: String?

  // MARK: ObjectMapper Initializers
  /// Map a JSON object to this class using ObjectMapper.
  ///
  /// - parameter map: A mapping from ObjectMapper.
  public required init?(map: Map){

  }

  /// Map a JSON object to this class using ObjectMapper.
  ///
  /// - parameter map: A mapping from ObjectMapper.
  public func mapping(map: Map) {
    userDatetime <- map[SerializationKeys.userDatetime]
    userEmail <- map[SerializationKeys.userEmail]
    userDevice <- map[SerializationKeys.userDevice]
    refUserId <- map[SerializationKeys.refUserId]
    userSocialId <- map[SerializationKeys.userSocialId]
    refCareerId <- map[SerializationKeys.refCareerId]
    userLoginType <- map[SerializationKeys.userLoginType]
    userProfileEducation <- map[SerializationKeys.userProfileEducation]
    userProfileId <- map[SerializationKeys.userProfileId]
    religionName <- map[SerializationKeys.religionName]
    communityName <- map[SerializationKeys.communityName]
    userProfileName <- map[SerializationKeys.userProfileName]
    userProfileGender <- map[SerializationKeys.userProfileGender]
    userApi <- map[SerializationKeys.userApi]
    refReligionId <- map[SerializationKeys.refReligionId]
    userProfileAge <- map[SerializationKeys.userProfileAge]
    userProfileAbout <- map[SerializationKeys.userProfileAbout]
    userProfileDob <- map[SerializationKeys.userProfileDob]
    careerName <- map[SerializationKeys.careerName]
    refRaisedInId <- map[SerializationKeys.refRaisedInId]
    userProfileHeight <- map[SerializationKeys.userProfileHeight]
    userProfileImage <- map[SerializationKeys.userProfileImage]
    refCommunityId <- map[SerializationKeys.refCommunityId]
    userProfileDatetime <- map[SerializationKeys.userProfileDatetime]
    userToken <- map[SerializationKeys.userToken]
    userId <- map[SerializationKeys.userId]
    raisedInName <- map[SerializationKeys.raisedInName]
    userProfileHeadline <- map[SerializationKeys.userProfileHeadline]
  }

  /// Generates description of the object in the form of a NSDictionary.
  ///
  /// - returns: A Key value pair containing all valid values in the object.
  public func dictionaryRepresentation() -> [String: Any] {
    var dictionary: [String: Any] = [:]
    if let value = userDatetime { dictionary[SerializationKeys.userDatetime] = value }
    if let value = userEmail { dictionary[SerializationKeys.userEmail] = value }
    if let value = userDevice { dictionary[SerializationKeys.userDevice] = value }
    if let value = refUserId { dictionary[SerializationKeys.refUserId] = value }
    if let value = userSocialId { dictionary[SerializationKeys.userSocialId] = value }
    if let value = refCareerId { dictionary[SerializationKeys.refCareerId] = value }
    if let value = userLoginType { dictionary[SerializationKeys.userLoginType] = value }
    if let value = userProfileEducation { dictionary[SerializationKeys.userProfileEducation] = value }
    if let value = userProfileId { dictionary[SerializationKeys.userProfileId] = value }
    if let value = religionName { dictionary[SerializationKeys.religionName] = value }
    if let value = communityName { dictionary[SerializationKeys.communityName] = value }
    if let value = userProfileName { dictionary[SerializationKeys.userProfileName] = value }
    if let value = userProfileGender { dictionary[SerializationKeys.userProfileGender] = value }
    if let value = userApi { dictionary[SerializationKeys.userApi] = value }
    if let value = refReligionId { dictionary[SerializationKeys.refReligionId] = value }
    if let value = userProfileAge { dictionary[SerializationKeys.userProfileAge] = value }
    if let value = userProfileAbout { dictionary[SerializationKeys.userProfileAbout] = value }
    if let value = userProfileDob { dictionary[SerializationKeys.userProfileDob] = value }
    if let value = careerName { dictionary[SerializationKeys.careerName] = value }
    if let value = refRaisedInId { dictionary[SerializationKeys.refRaisedInId] = value }
    if let value = userProfileHeight { dictionary[SerializationKeys.userProfileHeight] = value }
    if let value = userProfileImage { dictionary[SerializationKeys.userProfileImage] = value }
    if let value = refCommunityId { dictionary[SerializationKeys.refCommunityId] = value }
    if let value = userProfileDatetime { dictionary[SerializationKeys.userProfileDatetime] = value }
    if let value = userToken { dictionary[SerializationKeys.userToken] = value }
    if let value = userId { dictionary[SerializationKeys.userId] = value }
    if let value = raisedInName { dictionary[SerializationKeys.raisedInName] = value }
    if let value = userProfileHeadline { dictionary[SerializationKeys.userProfileHeadline] = value }
    return dictionary
  }

}
