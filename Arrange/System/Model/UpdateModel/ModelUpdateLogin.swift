//
//  ModelUpdateLogin.swift
//
//  Created by Miral Kamani on 07/03/20
//  Copyright (c) . All rights reserved.
//

import Foundation
import ObjectMapper
public class ModelUpdateLogin: Mappable {

  // MARK: Declaration for string constants to be used to decode and also serialize.
  private struct SerializationKeys {
    static let error = "error"
    static let message = "message"
    static let updateResult = "UpdateResult"
    static let success = "success"
  }

  // MARK: Properties
  public var error: Bool? = false
  public var message: String?
  public var updateResult: ModelUpdateResult?
  public var success: Bool? = false

  // MARK: ObjectMapper Initializers
  /// Map a JSON object to this class using ObjectMapper.
  ///
  /// - parameter map: A mapping from ObjectMapper.
  public required init?(map: Map){

  }

  /// Map a JSON object to this class using ObjectMapper.
  ///
  /// - parameter map: A mapping from ObjectMapper.
  public func mapping(map: Map) {
    error <- map[SerializationKeys.error]
    message <- map[SerializationKeys.message]
    updateResult <- map[SerializationKeys.updateResult]
    success <- map[SerializationKeys.success]
  }

  /// Generates description of the object in the form of a NSDictionary.
  ///
  /// - returns: A Key value pair containing all valid values in the object.
  public func dictionaryRepresentation() -> [String: Any] {
    var dictionary: [String: Any] = [:]
    dictionary[SerializationKeys.error] = error
    if let value = message { dictionary[SerializationKeys.message] = value }
    if let value = updateResult { dictionary[SerializationKeys.updateResult] = value.dictionaryRepresentation() }
    dictionary[SerializationKeys.success] = success
    return dictionary
  }

}
