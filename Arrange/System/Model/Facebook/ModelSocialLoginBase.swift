//
//  ModelSocialLoginBase.swift
//
//  Created by Miral Kamani on 18/03/20
//  Copyright (c) . All rights reserved.
//

import Foundation
import ObjectMapper
public class ModelSocialLoginBase: Mappable {

  // MARK: Declaration for string constants to be used to decode and also serialize.
  private struct SerializationKeys {
    static let success = "success"
    static let message = "message"
    static let facebookResult = "Result"
    static let error = "error"
  }

  // MARK: Properties
  public var success: Bool? = false
  public var message: String?
  public var facebookResult: ModelFacebookResult?
  public var error: Bool? = false

  // MARK: ObjectMapper Initializers
  /// Map a JSON object to this class using ObjectMapper.
  ///
  /// - parameter map: A mapping from ObjectMapper.
  public required init?(map: Map){

  }

  /// Map a JSON object to this class using ObjectMapper.
  ///
  /// - parameter map: A mapping from ObjectMapper.
  public func mapping(map: Map) {
    success <- map[SerializationKeys.success]
    message <- map[SerializationKeys.message]
    facebookResult <- map[SerializationKeys.facebookResult]
    error <- map[SerializationKeys.error]
  }

  /// Generates description of the object in the form of a NSDictionary.
  ///
  /// - returns: A Key value pair containing all valid values in the object.
  public func dictionaryRepresentation() -> [String: Any] {
    var dictionary: [String: Any] = [:]
    dictionary[SerializationKeys.success] = success
    if let value = message { dictionary[SerializationKeys.message] = value }
    if let value = facebookResult { dictionary[SerializationKeys.facebookResult] = value.dictionaryRepresentation() }
    dictionary[SerializationKeys.error] = error
    return dictionary
  }

}
