//
//  ModelFacebookResult.swift
//
//  Created by Miral Kamani on 18/03/20
//  Copyright (c) . All rights reserved.
//

import Foundation
import ObjectMapper
public class ModelFacebookResult: Mappable {

  // MARK: Declaration for string constants to be used to decode and also serialize.
  private struct SerializationKeys {
    static let userDatetime = "user_datetime"
    static let userEmail = "user_email"
    static let userProfileCountry = "user_profile_country"
    static let userProfileState = "user_profile_state"
    static let userDevice = "user_device"
    static let userSocialId = "user_social_id"
    static let refUserId = "ref_user_id"
    static let refCareerId = "ref_career_id"
    static let userProfileEducation = "user_profile_education"
    static let userLoginType = "user_login_type"
    static let userProfileId = "user_profile_id"
    static let religionName = "religion_name"
    static let communityName = "community_name"
    static let userProfileName = "user_profile_name"
    static let userProfileLatitude = "user_profile_latitude"
    static let userProfileGender = "user_profile_gender"
    static let userApi = "user_api"
    static let isUpdated = "is_updated"
    static let refReligionId = "ref_religion_id"
    static let userProfileAge = "user_profile_age"
    static let userProfileAbout = "user_profile_about"
    static let userProfileDob = "user_profile_dob"
    static let careerName = "career_name"
    static let refRaisedInId = "ref_raised_in_id"
    static let userProfileAddress = "user_profile_address"
    static let userProfileHeight = "user_profile_height"
    static let userProfileImage = "user_profile_image"
    static let userProfileCity = "user_profile_city"
    static let refCommunityId = "ref_community_id"
    static let userToken = "user_token"
    static let userProfileLongitude = "user_profile_longitude"
    static let userId = "user_id"
    static let userProfileDatetime = "user_profile_datetime"
    static let raisedInName = "raised_in_name"
    static let userProfileHeadline = "user_profile_headline"
  }

  // MARK: Properties
  public var userDatetime: String?
  public var userEmail: String?
  public var userProfileCountry: String?
  public var userProfileState: String?
  public var userDevice: String?
  public var userSocialId: String?
  public var refUserId: Int?
  public var refCareerId: Int?
  public var userProfileEducation: String?
  public var userLoginType: String?
  public var userProfileId: Int?
  public var religionName: String?
  public var communityName: String?
  public var userProfileName: String?
  public var userProfileLatitude: Int?
  public var userProfileGender: String?
  public var userApi: String?
  public var isUpdated: Int?
  public var refReligionId: Int?
  public var userProfileAge: Int?
  public var userProfileAbout: String?
  public var userProfileDob: String?
  public var careerName: String?
  public var refRaisedInId: Int?
  public var userProfileAddress: String?
  public var userProfileHeight: String?
  public var userProfileImage: String?
  public var userProfileCity: String?
  public var refCommunityId: Int?
  public var userToken: String?
  public var userProfileLongitude: Int?
  public var userId: Int?
  public var userProfileDatetime: String?
  public var raisedInName: String?
  public var userProfileHeadline: String?

  // MARK: ObjectMapper Initializers
  /// Map a JSON object to this class using ObjectMapper.
  ///
  /// - parameter map: A mapping from ObjectMapper.
  public required init?(map: Map){

  }

  /// Map a JSON object to this class using ObjectMapper.
  ///
  /// - parameter map: A mapping from ObjectMapper.
  public func mapping(map: Map) {
    userDatetime <- map[SerializationKeys.userDatetime]
    userEmail <- map[SerializationKeys.userEmail]
    userProfileCountry <- map[SerializationKeys.userProfileCountry]
    userProfileState <- map[SerializationKeys.userProfileState]
    userDevice <- map[SerializationKeys.userDevice]
    userSocialId <- map[SerializationKeys.userSocialId]
    refUserId <- map[SerializationKeys.refUserId]
    refCareerId <- map[SerializationKeys.refCareerId]
    userProfileEducation <- map[SerializationKeys.userProfileEducation]
    userLoginType <- map[SerializationKeys.userLoginType]
    userProfileId <- map[SerializationKeys.userProfileId]
    religionName <- map[SerializationKeys.religionName]
    communityName <- map[SerializationKeys.communityName]
    userProfileName <- map[SerializationKeys.userProfileName]
    userProfileLatitude <- map[SerializationKeys.userProfileLatitude]
    userProfileGender <- map[SerializationKeys.userProfileGender]
    userApi <- map[SerializationKeys.userApi]
    isUpdated <- map[SerializationKeys.isUpdated]
    refReligionId <- map[SerializationKeys.refReligionId]
    userProfileAge <- map[SerializationKeys.userProfileAge]
    userProfileAbout <- map[SerializationKeys.userProfileAbout]
    userProfileDob <- map[SerializationKeys.userProfileDob]
    careerName <- map[SerializationKeys.careerName]
    refRaisedInId <- map[SerializationKeys.refRaisedInId]
    userProfileAddress <- map[SerializationKeys.userProfileAddress]
    userProfileHeight <- map[SerializationKeys.userProfileHeight]
    userProfileImage <- map[SerializationKeys.userProfileImage]
    userProfileCity <- map[SerializationKeys.userProfileCity]
    refCommunityId <- map[SerializationKeys.refCommunityId]
    userToken <- map[SerializationKeys.userToken]
    userProfileLongitude <- map[SerializationKeys.userProfileLongitude]
    userId <- map[SerializationKeys.userId]
    userProfileDatetime <- map[SerializationKeys.userProfileDatetime]
    raisedInName <- map[SerializationKeys.raisedInName]
    userProfileHeadline <- map[SerializationKeys.userProfileHeadline]
  }

  /// Generates description of the object in the form of a NSDictionary.
  ///
  /// - returns: A Key value pair containing all valid values in the object.
  public func dictionaryRepresentation() -> [String: Any] {
    var dictionary: [String: Any] = [:]
    if let value = userDatetime { dictionary[SerializationKeys.userDatetime] = value }
    if let value = userEmail { dictionary[SerializationKeys.userEmail] = value }
    if let value = userProfileCountry { dictionary[SerializationKeys.userProfileCountry] = value }
    if let value = userProfileState { dictionary[SerializationKeys.userProfileState] = value }
    if let value = userDevice { dictionary[SerializationKeys.userDevice] = value }
    if let value = userSocialId { dictionary[SerializationKeys.userSocialId] = value }
    if let value = refUserId { dictionary[SerializationKeys.refUserId] = value }
    if let value = refCareerId { dictionary[SerializationKeys.refCareerId] = value }
    if let value = userProfileEducation { dictionary[SerializationKeys.userProfileEducation] = value }
    if let value = userLoginType { dictionary[SerializationKeys.userLoginType] = value }
    if let value = userProfileId { dictionary[SerializationKeys.userProfileId] = value }
    if let value = religionName { dictionary[SerializationKeys.religionName] = value }
    if let value = communityName { dictionary[SerializationKeys.communityName] = value }
    if let value = userProfileName { dictionary[SerializationKeys.userProfileName] = value }
    if let value = userProfileLatitude { dictionary[SerializationKeys.userProfileLatitude] = value }
    if let value = userProfileGender { dictionary[SerializationKeys.userProfileGender] = value }
    if let value = userApi { dictionary[SerializationKeys.userApi] = value }
    if let value = isUpdated { dictionary[SerializationKeys.isUpdated] = value }
    if let value = refReligionId { dictionary[SerializationKeys.refReligionId] = value }
    if let value = userProfileAge { dictionary[SerializationKeys.userProfileAge] = value }
    if let value = userProfileAbout { dictionary[SerializationKeys.userProfileAbout] = value }
    if let value = userProfileDob { dictionary[SerializationKeys.userProfileDob] = value }
    if let value = careerName { dictionary[SerializationKeys.careerName] = value }
    if let value = refRaisedInId { dictionary[SerializationKeys.refRaisedInId] = value }
    if let value = userProfileAddress { dictionary[SerializationKeys.userProfileAddress] = value }
    if let value = userProfileHeight { dictionary[SerializationKeys.userProfileHeight] = value }
    if let value = userProfileImage { dictionary[SerializationKeys.userProfileImage] = value }
    if let value = userProfileCity { dictionary[SerializationKeys.userProfileCity] = value }
    if let value = refCommunityId { dictionary[SerializationKeys.refCommunityId] = value }
    if let value = userToken { dictionary[SerializationKeys.userToken] = value }
    if let value = userProfileLongitude { dictionary[SerializationKeys.userProfileLongitude] = value }
    if let value = userId { dictionary[SerializationKeys.userId] = value }
    if let value = userProfileDatetime { dictionary[SerializationKeys.userProfileDatetime] = value }
    if let value = raisedInName { dictionary[SerializationKeys.raisedInName] = value }
    if let value = userProfileHeadline { dictionary[SerializationKeys.userProfileHeadline] = value }
    return dictionary
  }

}
