//
//  ModelRaisedInResult.swift
//
//  Created by Miral Kamani on 04/03/20
//  Copyright (c) . All rights reserved.
//

import Foundation
import ObjectMapper
public class ModelRaisedInResult: Mappable {

  // MARK: Declaration for string constants to be used to decode and also serialize.
  private struct SerializationKeys {
    static let raisedInDatetime = "raised_in_datetime"
    static let raisedInId = "raised_in_id"
    static let raisedInName = "raised_in_name"
  }

  // MARK: Properties
  public var raisedInDatetime: String?
  public var raisedInId: Int?
  public var raisedInName: String?

  // MARK: ObjectMapper Initializers
  /// Map a JSON object to this class using ObjectMapper.
  ///
  /// - parameter map: A mapping from ObjectMapper.
  public required init?(map: Map){

  }

  /// Map a JSON object to this class using ObjectMapper.
  ///
  /// - parameter map: A mapping from ObjectMapper.
  public func mapping(map: Map) {
    raisedInDatetime <- map[SerializationKeys.raisedInDatetime]
    raisedInId <- map[SerializationKeys.raisedInId]
    raisedInName <- map[SerializationKeys.raisedInName]
  }

  /// Generates description of the object in the form of a NSDictionary.
  ///
  /// - returns: A Key value pair containing all valid values in the object.
  public func dictionaryRepresentation() -> [String: Any] {
    var dictionary: [String: Any] = [:]
    if let value = raisedInDatetime { dictionary[SerializationKeys.raisedInDatetime] = value }
    if let value = raisedInId { dictionary[SerializationKeys.raisedInId] = value }
    if let value = raisedInName { dictionary[SerializationKeys.raisedInName] = value }
    return dictionary
  }

}
