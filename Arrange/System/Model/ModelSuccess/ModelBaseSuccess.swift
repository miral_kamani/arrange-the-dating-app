//
//  ModelBaseSuccess.swift
//
//  Created by Darshit on 07/06/19
//  Copyright (c) Grewon. All rights reserved.
//

import Foundation
import ObjectMapper

class ModelBaseSuccess: Mappable {

    // MARK: Declaration for string constants to be used to decode and also serialize.
	internal let kModelBaseSuccessMessageKey: String = "message"
	internal let kModelBaseSuccessErrorKey: String = "error"
	internal let kModelBaseSuccessSuccessKey: String = "success"


    // MARK: Properties
	public var message: String?
	public var error: Bool = false
	public var success: Bool = false



    // MARK: ObjectMapper Initalizers
    /**
        Map a JSON object to this class using ObjectMapper
        - parameter map: A mapping from ObjectMapper
    */
    required convenience init?(map: Map) {
        self.init()
    }


    /**
        Map a JSON object to this class using ObjectMapper
        - parameter map: A mapping from ObjectMapper
    */
    func mapping(map: Map) {
		message <- map[kModelBaseSuccessMessageKey]
		error <- map[kModelBaseSuccessErrorKey]
		success <- map[kModelBaseSuccessSuccessKey]

    }

    /**
    Generates description of the object in the form of a NSDictionary.
    - returns: A Key value pair containing all valid values in the object.
    */
    public func dictionaryRepresentation() -> [String : AnyObject ] {
        var dictionary: [String : AnyObject ] = [ : ]
		if message != nil {
			dictionary.updateValue(message! as AnyObject, forKey: kModelBaseSuccessMessageKey)
		}
		dictionary.updateValue(error as AnyObject, forKey: kModelBaseSuccessErrorKey)
		dictionary.updateValue(success as AnyObject, forKey: kModelBaseSuccessSuccessKey)
        return dictionary
    }
}
