//
//  ModelFileUploadBase.swift
//
//  Created by Darshit on 07/06/19
//  Copyright (c) . All rights reserved.
//

import Foundation
import ObjectMapper

class ModelFileUploadBase: Mappable {

    // MARK: Declaration for string constants to be used to decode and also serialize.
	internal let kModelFileUploadBaseFileNameKey: String = "file_name"
    internal let kModelFileUploadBaseImageNameKey: String = "image_name"
	internal let kModelFileUploadBaseErrorKey: String = "error"
	internal let kModelFileUploadBaseMessageKey: String = "message"
	internal let kModelFileUploadBaseSuccessKey: String = "success"


    // MARK: Properties
	public var fileName: String?
    public var imageName: String?
	public var error: Bool = false
	public var message: String?
	public var success: Bool = false



    // MARK: ObjectMapper Initalizers
    /**
        Map a JSON object to this class using ObjectMapper
        - parameter map: A mapping from ObjectMapper
    */
    required convenience init?(map: Map) {
        self.init()
    }


    /**
        Map a JSON object to this class using ObjectMapper
        - parameter map: A mapping from ObjectMapper
    */
    func mapping(map: Map) {
		fileName <- map[kModelFileUploadBaseFileNameKey]
        imageName <- map[kModelFileUploadBaseImageNameKey]
		error <- map[kModelFileUploadBaseErrorKey]
		message <- map[kModelFileUploadBaseMessageKey]
		success <- map[kModelFileUploadBaseSuccessKey]

    }

    /**
    Generates description of the object in the form of a NSDictionary.
    - returns: A Key value pair containing all valid values in the object.
    */
    public func dictionaryRepresentation() -> [String : AnyObject ] {

        var dictionary: [String : AnyObject ] = [ : ]
		if fileName != nil {
			dictionary.updateValue(fileName! as AnyObject, forKey: kModelFileUploadBaseFileNameKey)
		}
        if imageName != nil {
            dictionary.updateValue(imageName! as AnyObject, forKey: kModelFileUploadBaseImageNameKey)
        }
		dictionary.updateValue(error as AnyObject, forKey: kModelFileUploadBaseErrorKey)
		if message != nil {
			dictionary.updateValue(message! as AnyObject, forKey: kModelFileUploadBaseMessageKey)
		}
		dictionary.updateValue(success as AnyObject, forKey: kModelFileUploadBaseSuccessKey)

        return dictionary
    }

}
